package cn.accjiyun.entity.json;

import java.util.List;

/**
 * Created by jiyun on 2017/5/10.
 */
public class Output {


    /**
     * legend : {"data":["种植","畜牧","水产","林业"]}
     * data : [{"value":5335,"name":"种植"},{"value":4310,"name":"畜牧"},{"value":2310,"name":"水产"},{"value":3320,"name":"林业"}]
     */

    private LegendBean legend;
    private List<DataBean> data;

    public LegendBean getLegend() {
        return legend;
    }

    public void setLegend(LegendBean legend) {
        this.legend = legend;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class LegendBean {
        private List<String> data;

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }
    }

    public static class DataBean {
        /**
         * value : 5335
         * name : 种植
         */
        private String name;
        private double value;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

    }
}

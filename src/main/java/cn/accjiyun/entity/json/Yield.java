package cn.accjiyun.entity.json;

import java.util.List;

/**
 * Created by jiyun on 2017/5/10.
 */
public class Yield {
    /**
     * legend : {"data":["土豆","玉米","水稻","花生","棉花"]}
     * xAxis : {"data":["2012","2013","2014","2015","2016","2017"]}
     * series : [{"name":"土豆","type":"line","data":[200,234,290,262,272,291]},{"name":"玉米","type":"line","data":[248,0,228,279,185,212]},{"name":"水稻","type":"line","data":[298,62,327,276,303,334]},{"name":"花生","type":"line","data":[197,256,320,237,239,365]},{"name":"棉花","type":"line","data":[177,103,173,116,222,0]}]
     */

    private LegendBean legend;
    private XAxisBean xAxis;
    private List<SeriesBean> series;

    public LegendBean getLegend() {
        return legend;
    }

    public void setLegend(LegendBean legend) {
        this.legend = legend;
    }

    public XAxisBean getXAxis() {
        return xAxis;
    }

    public void setXAxis(XAxisBean xAxis) {
        this.xAxis = xAxis;
    }

    public List<SeriesBean> getSeries() {
        return series;
    }

    public void setSeries(List<SeriesBean> series) {
        this.series = series;
    }

    public static class LegendBean {
        private List<String> data;

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }
    }

    public static class XAxisBean {
        private List<String> data;

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }
    }

    public static class SeriesBean {
        /**
         * name : 土豆
         * type : line
         * data : [200,234,290,262,272,291]
         */

        private String name;
        private String type = "line";
        private List<Long> data;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<Long> getData() {
            return data;
        }

        public void setData(List<Long> data) {
            this.data = data;
        }
    }
}

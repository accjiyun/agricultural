package cn.accjiyun.entity.json;

import java.util.List;

/**
 * Created by jiyun on 2017/5/10.
 */
public class Price {
    /**
     * legend : {"data":["土豆","玉米","水稻","花生","棉花"]}
     * xAxis : {"data":["2012","2013","2014","2015","2016","2017"]}
     * series : [{"name":"土豆","type":"line","data":[1.3,1.3,1.9,1.6,1.7,2.2]},{"name":"玉米","type":"line","data":[2.4,2.1,2.2,2.7,1.8,2.1]},{"name":"水稻","type":"line","data":[2.9,4.2,3.2,2.7,3,3.3]},{"name":"花生","type":"line","data":[1.7,2.6,3,3.7,2.9,3.5]},{"name":"棉花","type":"line","data":[4.7,5,3.7,2.1,3.2,5]}]
     */

    private LegendBean legend;
    private XAxisBean xAxis;
    private List<SeriesBean> series;

    public LegendBean getLegend() {
        return legend;
    }

    public void setLegend(LegendBean legend) {
        this.legend = legend;
    }

    public XAxisBean getXAxis() {
        return xAxis;
    }

    public void setXAxis(XAxisBean xAxis) {
        this.xAxis = xAxis;
    }

    public List<SeriesBean> getSeries() {
        return series;
    }

    public void setSeries(List<SeriesBean> series) {
        this.series = series;
    }

    public static class LegendBean {
        private List<String> data;

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }
    }

    public static class XAxisBean {
        private List<String> data;

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }
    }

    public static class SeriesBean {
        /**
         * name : 土豆
         * type : line
         * data : [1.3,1.3,1.9,1.6,1.7,2.2]
         */

        private String name;
        private String type;
        private List<Double> data;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<Double> getData() {
            return data;
        }

        public void setData(List<Double> data) {
            this.data = data;
        }
    }
}

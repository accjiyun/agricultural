package cn.accjiyun.entity.society;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
public class Relative implements Serializable {
    private int relativeId;
    private String relativeName;
    private String sex;
    private String relationship;
    private String nation;
    private String mobile;
    private String email;
    private String address;
    private String comments;
    private SocietyUser societyUserByUserId;

    @Id
    @Column(name = "relative_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRelativeId() {
        return relativeId;
    }

    public void setRelativeId(int relativeId) {
        this.relativeId = relativeId;
    }

    @Basic
    @Column(name = "relative_name", nullable = true, length = 20)
    public String getRelativeName() {
        return relativeName;
    }

    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }

    @Basic
    @Column(name = "sex", nullable = true, length = 2)
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "relationship", nullable = true, length = 15)
    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    @Basic
    @Column(name = "nation", nullable = true, length = 20)
    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    @Basic
    @Column(name = "mobile", nullable = true, length = 15)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 30)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 50)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "comments", nullable = true, length = 255)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Relative relative = (Relative) o;

        if (relativeId != relative.relativeId) return false;
        if (relativeName != null ? !relativeName.equals(relative.relativeName) : relative.relativeName != null)
            return false;
        if (sex != null ? !sex.equals(relative.sex) : relative.sex != null) return false;
        if (relationship != null ? !relationship.equals(relative.relationship) : relative.relationship != null)
            return false;
        if (nation != null ? !nation.equals(relative.nation) : relative.nation != null) return false;
        if (mobile != null ? !mobile.equals(relative.mobile) : relative.mobile != null) return false;
        if (email != null ? !email.equals(relative.email) : relative.email != null) return false;
        if (address != null ? !address.equals(relative.address) : relative.address != null) return false;
        if (comments != null ? !comments.equals(relative.comments) : relative.comments != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = relativeId;
        result = 31 * result + (relativeName != null ? relativeName.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (relationship != null ? relationship.hashCode() : 0);
        result = 31 * result + (nation != null ? nation.hashCode() : 0);
        result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public SocietyUser getSocietyUserByUserId() {
        return societyUserByUserId;
    }

    public void setSocietyUserByUserId(SocietyUser societyUserByUserId) {
        this.societyUserByUserId = societyUserByUserId;
    }
}

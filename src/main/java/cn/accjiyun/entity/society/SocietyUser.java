package cn.accjiyun.entity.society;

import cn.accjiyun.entity.system.SystemUser;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
@Table(name = "society_user", schema = "agricultural_db", catalog = "")
public class SocietyUser implements Serializable {
    private int userId;
    private String username;
    private String identityCard;
    private String sex;
    private String nation;
    private String mobile;
    private String email;
    private String address;
    private Timestamp joinDate;
    private Timestamp insureEndDate;
    private String comments;
    private Collection<Relative> relativesByUserId;
    private SocietyRole societyRoleByRoleTypeId;
    private SystemUser systemUserByUserId;

    @Id
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 20)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "identity_card", nullable = false, length = 20)
    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    @Basic
    @Column(name = "sex", nullable = true, length = 2)
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "nation", nullable = true, length = 10)
    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    @Basic
    @Column(name = "mobile", nullable = true, length = 15)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 30)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 50)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "join_date", nullable = false)
    public Timestamp getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Timestamp joinDate) {
        this.joinDate = joinDate;
    }

    @Basic
    @Column(name = "insure_end_date", nullable = true)
    public Timestamp getInsureEndDate() {
        return insureEndDate;
    }

    public void setInsureEndDate(Timestamp insureEndDate) {
        this.insureEndDate = insureEndDate;
    }

    @Basic
    @Column(name = "comments", nullable = true, length = 255)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SocietyUser that = (SocietyUser) o;

        if (userId != that.userId) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (identityCard != null ? !identityCard.equals(that.identityCard) : that.identityCard != null) return false;
        if (sex != null ? !sex.equals(that.sex) : that.sex != null) return false;
        if (nation != null ? !nation.equals(that.nation) : that.nation != null) return false;
        if (mobile != null ? !mobile.equals(that.mobile) : that.mobile != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (joinDate != null ? !joinDate.equals(that.joinDate) : that.joinDate != null) return false;
        if (insureEndDate != null ? !insureEndDate.equals(that.insureEndDate) : that.insureEndDate != null)
            return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (identityCard != null ? identityCard.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (nation != null ? nation.hashCode() : 0);
        result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (joinDate != null ? joinDate.hashCode() : 0);
        result = 31 * result + (insureEndDate != null ? insureEndDate.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "societyUserByUserId")
    public Collection<Relative> getRelativesByUserId() {
        return relativesByUserId;
    }

    public void setRelativesByUserId(Collection<Relative> relativesByUserId) {
        this.relativesByUserId = relativesByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "role_type_id", referencedColumnName = "role_type_id")
    public SocietyRole getSocietyRoleByRoleTypeId() {
        return societyRoleByRoleTypeId;
    }

    public void setSocietyRoleByRoleTypeId(SocietyRole societyRoleByRoleTypeId) {
        this.societyRoleByRoleTypeId = societyRoleByRoleTypeId;
    }

    @OneToOne(mappedBy = "societyUserByUserId")
    public SystemUser getSystemUserByUserId() {
        return systemUserByUserId;
    }

    public void setSystemUserByUserId(SystemUser systemUserByUserId) {
        this.systemUserByUserId = systemUserByUserId;
    }
}

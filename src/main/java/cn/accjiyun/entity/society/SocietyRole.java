package cn.accjiyun.entity.society;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
@Table(name = "society_role", schema = "agricultural_db", catalog = "")
public class SocietyRole implements Serializable {
    private byte roleTypeId;
    private String roleName;

    @Id
    @Column(name = "role_type_id", nullable = false)
    public byte getRoleTypeId() {
        return roleTypeId;
    }

    public void setRoleTypeId(byte roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    @Basic
    @Column(name = "role_name", nullable = true, length = 20)
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SocietyRole that = (SocietyRole) o;

        if (roleTypeId != that.roleTypeId) return false;
        if (roleName != null ? !roleName.equals(that.roleName) : that.roleName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) roleTypeId;
        result = 31 * result + (roleName != null ? roleName.hashCode() : 0);
        return result;
    }
}

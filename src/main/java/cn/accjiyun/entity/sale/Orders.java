package cn.accjiyun.entity.sale;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
public class Orders implements Serializable {
    private int ordersId;
    private String variety;
    private Integer count;
    private Integer realPrice;
    private Timestamp createTime;
    private Customer customerByCustomerId;

    @Id
    @Column(name = "orders_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(int ordersId) {
        this.ordersId = ordersId;
    }

    @Basic
    @Column(name = "variety", nullable = true, length = 20)
    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    @Basic
    @Column(name = "count", nullable = true)
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Basic
    @Column(name = "real_price", nullable = true)
    public Integer getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Integer realPrice) {
        this.realPrice = realPrice;
    }

    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orders orders = (Orders) o;

        if (ordersId != orders.ordersId) return false;
        if (variety != null ? !variety.equals(orders.variety) : orders.variety != null) return false;
        if (count != null ? !count.equals(orders.count) : orders.count != null) return false;
        if (realPrice != null ? !realPrice.equals(orders.realPrice) : orders.realPrice != null) return false;
        if (createTime != null ? !createTime.equals(orders.createTime) : orders.createTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ordersId;
        result = 31 * result + (variety != null ? variety.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (realPrice != null ? realPrice.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    public Customer getCustomerByCustomerId() {
        return customerByCustomerId;
    }

    public void setCustomerByCustomerId(Customer customerByCustomerId) {
        this.customerByCustomerId = customerByCustomerId;
    }
}

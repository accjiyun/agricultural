package cn.accjiyun.entity.article;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
@Table(name = "article_type", schema = "agricultural_db", catalog = "")
public class ArticleType implements Serializable {
    private byte articleTypeId;
    private String typeName;

    @Id
    @Column(name = "article_type_id", nullable = false)
    public byte getArticleTypeId() {
        return articleTypeId;
    }

    public void setArticleTypeId(byte articleTypeId) {
        this.articleTypeId = articleTypeId;
    }

    @Basic
    @Column(name = "type_name", nullable = true, length = 20)
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleType that = (ArticleType) o;

        if (articleTypeId != that.articleTypeId) return false;
        if (typeName != null ? !typeName.equals(that.typeName) : that.typeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) articleTypeId;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        return result;
    }
}

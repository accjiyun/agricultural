package cn.accjiyun.entity.article;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
public class Article implements Serializable {
    private int articleId;
    private String title;
    private String keyWord;
    private Timestamp createTime;
    private Timestamp publicTime;
    private Byte status;
    private String imageUrl;
    private Integer clickNum;
    private Integer sort;
    private ArticleType articleTypeByArticleTypeId;
    private ArticleContent articleContentByArticleId;
    private String summary;

    @Id
    @Column(name = "article_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    @Basic
    @Column(name = "title", nullable = true, length = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "key_word", nullable = true, length = 50)
    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "public_time", nullable = true)
    public Timestamp getPublicTime() {
        return publicTime;
    }

    public void setPublicTime(Timestamp publicTime) {
        this.publicTime = publicTime;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @Basic
    @Column(name = "image_url", nullable = true, length = 255)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "click_num", nullable = true)
    public Integer getClickNum() {
        return clickNum;
    }

    public void setClickNum(Integer clickNum) {
        this.clickNum = clickNum;
    }

    @Basic
    @Column(name = "sort", nullable = true)
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (articleId != article.articleId) return false;
        if (title != null ? !title.equals(article.title) : article.title != null) return false;
        if (keyWord != null ? !keyWord.equals(article.keyWord) : article.keyWord != null) return false;
        if (createTime != null ? !createTime.equals(article.createTime) : article.createTime != null) return false;
        if (publicTime != null ? !publicTime.equals(article.publicTime) : article.publicTime != null) return false;
        if (status != null ? !status.equals(article.status) : article.status != null) return false;
        if (imageUrl != null ? !imageUrl.equals(article.imageUrl) : article.imageUrl != null) return false;
        if (clickNum != null ? !clickNum.equals(article.clickNum) : article.clickNum != null) return false;
        if (sort != null ? !sort.equals(article.sort) : article.sort != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = articleId;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (keyWord != null ? keyWord.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (publicTime != null ? publicTime.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (clickNum != null ? clickNum.hashCode() : 0);
        result = 31 * result + (sort != null ? sort.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "article_type_id", referencedColumnName = "article_type_id", nullable = false)
    public ArticleType getArticleTypeByArticleTypeId() {
        return articleTypeByArticleTypeId;
    }

    public void setArticleTypeByArticleTypeId(ArticleType articleTypeByArticleTypeId) {
        this.articleTypeByArticleTypeId = articleTypeByArticleTypeId;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    public ArticleContent getArticleContentByArticleId() {
        return articleContentByArticleId;
    }

    public void setArticleContentByArticleId(ArticleContent articleContentByArticleId) {
        this.articleContentByArticleId = articleContentByArticleId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}

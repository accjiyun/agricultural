package cn.accjiyun.entity.article;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
@Table(name = "article_content", schema = "agricultural_db", catalog = "")
public class ArticleContent implements Serializable {
    private int articleId;
    private String content;
    private Article articleByArticleId;

    @Id
    @Column(name = "article_id", nullable = false)
    @GenericGenerator(name = "foreignKey", strategy = "foreign",
            parameters = {@org.hibernate.annotations.Parameter(name = "property", value = "articleByArticleId")})
    @GeneratedValue(generator = "foreignKey")
    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    @Basic
    @Column(name = "content", nullable = true, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleContent that = (ArticleContent) o;

        if (articleId != that.articleId) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = articleId;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    public Article getArticleByArticleId() {
        return articleByArticleId;
    }

    public void setArticleByArticleId(Article articleByArticleId) {
        this.articleByArticleId = articleByArticleId;
    }
}

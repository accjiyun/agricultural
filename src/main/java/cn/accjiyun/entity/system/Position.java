package cn.accjiyun.entity.system;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
public class Position implements Serializable {
    private byte positionId;
    private String positionName;
    private String menuJson;
    private String comments;

    @Id
    @Column(name = "position_id", nullable = false)
    public byte getPositionId() {
        return positionId;
    }

    public void setPositionId(byte positionId) {
        this.positionId = positionId;
    }

    @Basic
    @Column(name = "position_name", nullable = true, length = 30)
    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    @Basic
    @Column(name = "menu_json", nullable = true, length = -1)
    public String getMenuJson() {
        return menuJson;
    }

    public void setMenuJson(String menuJson) {
        this.menuJson = menuJson;
    }

    @Basic
    @Column(name = "comments", nullable = true, length = 100)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (positionId != position.positionId) return false;
        if (positionName != null ? !positionName.equals(position.positionName) : position.positionName != null)
            return false;
        if (menuJson != null ? !menuJson.equals(position.menuJson) : position.menuJson != null) return false;
        if (comments != null ? !comments.equals(position.comments) : position.comments != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) positionId;
        result = 31 * result + (positionName != null ? positionName.hashCode() : 0);
        result = 31 * result + (menuJson != null ? menuJson.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }
}

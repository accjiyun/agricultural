package cn.accjiyun.entity.production;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
@Table(name = "contract_type", schema = "agricultural_db", catalog = "")
public class ContractType implements Serializable {
    private byte contractTypeId;
    private String contractTypeName;
    private String comments;

    @Id
    @Column(name = "contract_type_id", nullable = false)
    public byte getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(byte contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    @Basic
    @Column(name = "contract_type_name", nullable = true, length = 20)
    public String getContractTypeName() {
        return contractTypeName;
    }

    public void setContractTypeName(String contractTypeName) {
        this.contractTypeName = contractTypeName;
    }

    @Basic
    @Column(name = "comments", nullable = true, length = 100)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContractType that = (ContractType) o;

        if (contractTypeId != that.contractTypeId) return false;
        if (contractTypeName != null ? !contractTypeName.equals(that.contractTypeName) : that.contractTypeName != null)
            return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) contractTypeId;
        result = 31 * result + (contractTypeName != null ? contractTypeName.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }
}

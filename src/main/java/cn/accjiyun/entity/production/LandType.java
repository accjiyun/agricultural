package cn.accjiyun.entity.production;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
@Table(name = "land_type", schema = "agricultural_db", catalog = "")
public class LandType implements Serializable {
    private byte landTypeId;
    private String landTypeName;
    private String comments;

    @Id
    @Column(name = "land_type_id", nullable = false)
    public byte getLandTypeId() {
        return landTypeId;
    }

    public void setLandTypeId(byte landTypeId) {
        this.landTypeId = landTypeId;
    }

    @Basic
    @Column(name = "land_type_name", nullable = true, length = 30)
    public String getLandTypeName() {
        return landTypeName;
    }

    public void setLandTypeName(String landTypeName) {
        this.landTypeName = landTypeName;
    }

    @Basic
    @Column(name = "comments", nullable = true, length = 100)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LandType landType = (LandType) o;

        if (landTypeId != landType.landTypeId) return false;
        if (landTypeName != null ? !landTypeName.equals(landType.landTypeName) : landType.landTypeName != null)
            return false;
        if (comments != null ? !comments.equals(landType.comments) : landType.comments != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) landTypeId;
        result = 31 * result + (landTypeName != null ? landTypeName.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }
}

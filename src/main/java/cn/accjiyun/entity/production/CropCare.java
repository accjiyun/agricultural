package cn.accjiyun.entity.production;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
@Table(name = "crop_care", schema = "agricultural_db", catalog = "")
public class CropCare implements Serializable {
    private int cropCareId;
    private Integer batch;
    private String principalName;
    private String germchit;
    private Integer count;
    private String fertilizer;
    private String drug;
    private Timestamp cropTime;
    private Land landByLandId;

    @Id
    @Column(name = "crop_care_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getCropCareId() {
        return cropCareId;
    }

    public void setCropCareId(int cropCareId) {
        this.cropCareId = cropCareId;
    }

    @Basic
    @Column(name = "batch", nullable = true)
    public Integer getBatch() {
        return batch;
    }

    public void setBatch(Integer batch) {
        this.batch = batch;
    }

    @Basic
    @Column(name = "principal_name", nullable = true, length = 30)
    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    @Basic
    @Column(name = "germchit", nullable = true, length = 30)
    public String getGermchit() {
        return germchit;
    }

    public void setGermchit(String germchit) {
        this.germchit = germchit;
    }

    @Basic
    @Column(name = "count", nullable = true)
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Basic
    @Column(name = "fertilizer", nullable = true, length = 255)
    public String getFertilizer() {
        return fertilizer;
    }

    public void setFertilizer(String fertilizer) {
        this.fertilizer = fertilizer;
    }

    @Basic
    @Column(name = "drug", nullable = true, length = 255)
    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    @Basic
    @Column(name = "crop_time", nullable = true)
    public Timestamp getCropTime() {
        return cropTime;
    }

    public void setCropTime(Timestamp cropTime) {
        this.cropTime = cropTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CropCare cropCare = (CropCare) o;

        if (cropCareId != cropCare.cropCareId) return false;
        if (batch != null ? !batch.equals(cropCare.batch) : cropCare.batch != null) return false;
        if (principalName != null ? !principalName.equals(cropCare.principalName) : cropCare.principalName != null)
            return false;
        if (germchit != null ? !germchit.equals(cropCare.germchit) : cropCare.germchit != null) return false;
        if (count != null ? !count.equals(cropCare.count) : cropCare.count != null) return false;
        if (fertilizer != null ? !fertilizer.equals(cropCare.fertilizer) : cropCare.fertilizer != null) return false;
        if (drug != null ? !drug.equals(cropCare.drug) : cropCare.drug != null) return false;
        if (cropTime != null ? !cropTime.equals(cropCare.cropTime) : cropCare.cropTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cropCareId;
        result = 31 * result + (batch != null ? batch.hashCode() : 0);
        result = 31 * result + (principalName != null ? principalName.hashCode() : 0);
        result = 31 * result + (germchit != null ? germchit.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (fertilizer != null ? fertilizer.hashCode() : 0);
        result = 31 * result + (drug != null ? drug.hashCode() : 0);
        result = 31 * result + (cropTime != null ? cropTime.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "land_id", referencedColumnName = "land_id")
    public Land getLandByLandId() {
        return landByLandId;
    }

    public void setLandByLandId(Land landByLandId) {
        this.landByLandId = landByLandId;
    }
}

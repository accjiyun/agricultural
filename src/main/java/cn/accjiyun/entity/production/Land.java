package cn.accjiyun.entity.production;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by jiyun on 2017/4/28.
 */
@Entity
public class Land implements Serializable {
    private int landId;
    private String landName;
    private double area;
    private Double totalPrice;
    private Integer contractId;
    private Timestamp insureStartDate;
    private Double deadline;
    private Double realTotalPrice;
    private String transferor;
    private String assignee;
    private String mobile;
    private String comments;
    private Collection<CropCare> cropCaresByLandId;
    private LandType landTypeByLandTypeId;
    private ContractType contractTypeByContractTypeId;

    @Id
    @Column(name = "land_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getLandId() {
        return landId;
    }

    public void setLandId(int landId) {
        this.landId = landId;
    }

    @Basic
    @Column(name = "land_name", nullable = true, length = 40)
    public String getLandName() {
        return landName;
    }

    public void setLandName(String landName) {
        this.landName = landName;
    }

    @Basic
    @Column(name = "area", nullable = false, precision = 0)
    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    @Basic
    @Column(name = "total_price", nullable = true, precision = 0)
    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "contract_id", nullable = true)
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "insure_start_date", nullable = true)
    public Timestamp getInsureStartDate() {
        return insureStartDate;
    }

    public void setInsureStartDate(Timestamp insureStartDate) {
        this.insureStartDate = insureStartDate;
    }

    @Basic
    @Column(name = "deadline", nullable = true, precision = 0)
    public Double getDeadline() {
        return deadline;
    }

    public void setDeadline(Double deadline) {
        this.deadline = deadline;
    }

    @Basic
    @Column(name = "real_total_price", nullable = true, precision = 0)
    public Double getRealTotalPrice() {
        return realTotalPrice;
    }

    public void setRealTotalPrice(Double realTotalPrice) {
        this.realTotalPrice = realTotalPrice;
    }

    @Basic
    @Column(name = "transferor", nullable = true, length = 20)
    public String getTransferor() {
        return transferor;
    }

    public void setTransferor(String transferor) {
        this.transferor = transferor;
    }

    @Basic
    @Column(name = "assignee", nullable = true, length = 20)
    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    @Basic
    @Column(name = "mobile", nullable = true, length = 15)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "comments", nullable = true, length = 255)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Land land = (Land) o;

        if (landId != land.landId) return false;
        if (Double.compare(land.area, area) != 0) return false;
        if (landName != null ? !landName.equals(land.landName) : land.landName != null) return false;
        if (totalPrice != null ? !totalPrice.equals(land.totalPrice) : land.totalPrice != null) return false;
        if (contractId != null ? !contractId.equals(land.contractId) : land.contractId != null) return false;
        if (insureStartDate != null ? !insureStartDate.equals(land.insureStartDate) : land.insureStartDate != null)
            return false;
        if (deadline != null ? !deadline.equals(land.deadline) : land.deadline != null) return false;
        if (realTotalPrice != null ? !realTotalPrice.equals(land.realTotalPrice) : land.realTotalPrice != null)
            return false;
        if (transferor != null ? !transferor.equals(land.transferor) : land.transferor != null) return false;
        if (assignee != null ? !assignee.equals(land.assignee) : land.assignee != null) return false;
        if (mobile != null ? !mobile.equals(land.mobile) : land.mobile != null) return false;
        if (comments != null ? !comments.equals(land.comments) : land.comments != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = landId;
        result = 31 * result + (landName != null ? landName.hashCode() : 0);
        temp = Double.doubleToLongBits(area);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (totalPrice != null ? totalPrice.hashCode() : 0);
        result = 31 * result + (contractId != null ? contractId.hashCode() : 0);
        result = 31 * result + (insureStartDate != null ? insureStartDate.hashCode() : 0);
        result = 31 * result + (deadline != null ? deadline.hashCode() : 0);
        result = 31 * result + (realTotalPrice != null ? realTotalPrice.hashCode() : 0);
        result = 31 * result + (transferor != null ? transferor.hashCode() : 0);
        result = 31 * result + (assignee != null ? assignee.hashCode() : 0);
        result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "landByLandId")
    public Collection<CropCare> getCropCaresByLandId() {
        return cropCaresByLandId;
    }

    public void setCropCaresByLandId(Collection<CropCare> cropCaresByLandId) {
        this.cropCaresByLandId = cropCaresByLandId;
    }

    @ManyToOne
    @JoinColumn(name = "land_type_id", referencedColumnName = "land_type_id")
    public LandType getLandTypeByLandTypeId() {
        return landTypeByLandTypeId;
    }

    public void setLandTypeByLandTypeId(LandType landTypeByLandTypeId) {
        this.landTypeByLandTypeId = landTypeByLandTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "contract_type_id", referencedColumnName = "contract_type_id")
    public ContractType getContractTypeByContractTypeId() {
        return contractTypeByContractTypeId;
    }

    public void setContractTypeByContractTypeId(ContractType contractTypeByContractTypeId) {
        this.contractTypeByContractTypeId = contractTypeByContractTypeId;
    }
}

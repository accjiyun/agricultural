package cn.accjiyun.entity.production;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2017/4/30.
 */
@Entity
public class Producer implements Serializable {
    private int producerId;
    private String producerName;
    private Double area;
    private String producerType;
    private String varietyName;
    private Integer batchDuration;
    private String principalName;
    private Integer batchId;
    private Integer plannedYield;
    private Double plannedPrice;
    private Double plannedTotalPrice;
    private Integer realYield;
    private Double realPrice;
    private Double realTotalPrice;
    private Byte isProduced;
    private Timestamp startTime;
    private Timestamp endTime;
    private String comments;

    @Id
    @Column(name = "producer_id", nullable = false)
    public int getProducerId() {
        return producerId;
    }

    public void setProducerId(int producerId) {
        this.producerId = producerId;
    }

    @Basic
    @Column(name = "producer_name", nullable = true, length = 50)
    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    @Basic
    @Column(name = "area", nullable = true, precision = 0)
    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    @Basic
    @Column(name = "producer_type", nullable = true, length = 20)
    public String getProducerType() {
        return producerType;
    }

    public void setProducerType(String producerType) {
        this.producerType = producerType;
    }

    @Basic
    @Column(name = "variety_name", nullable = true, length = 20)
    public String getVarietyName() {
        return varietyName;
    }

    public void setVarietyName(String varietyName) {
        this.varietyName = varietyName;
    }

    @Basic
    @Column(name = "batch_duration", nullable = true)
    public Integer getBatchDuration() {
        return batchDuration;
    }

    public void setBatchDuration(Integer batchDuration) {
        this.batchDuration = batchDuration;
    }

    @Basic
    @Column(name = "principal_name", nullable = false, length = 20)
    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    @Basic
    @Column(name = "batch_id", nullable = true)
    public Integer getBatchId() {
        return batchId;
    }

    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }

    @Basic
    @Column(name = "planned_yield", nullable = true)
    public Integer getPlannedYield() {
        return plannedYield;
    }

    public void setPlannedYield(Integer plannedYield) {
        this.plannedYield = plannedYield;
    }

    @Basic
    @Column(name = "planned_price", nullable = true, precision = 0)
    public Double getPlannedPrice() {
        return plannedPrice;
    }

    public void setPlannedPrice(Double plannedPrice) {
        this.plannedPrice = plannedPrice;
    }

    @Basic
    @Column(name = "planned_total_price", nullable = true, precision = 0)
    public Double getPlannedTotalPrice() {
        return plannedTotalPrice;
    }

    public void setPlannedTotalPrice(Double plannedTotalPrice) {
        this.plannedTotalPrice = plannedTotalPrice;
    }

    @Basic
    @Column(name = "real_yield", nullable = true)
    public Integer getRealYield() {
        return realYield;
    }

    public void setRealYield(Integer realYield) {
        this.realYield = realYield;
    }

    @Basic
    @Column(name = "real_price", nullable = true, precision = 0)
    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    @Basic
    @Column(name = "real_total_price", nullable = true, precision = 0)
    public Double getRealTotalPrice() {
        return realTotalPrice;
    }

    public void setRealTotalPrice(Double realTotalPrice) {
        this.realTotalPrice = realTotalPrice;
    }

    @Basic
    @Column(name = "is_produced", nullable = true)
    public Byte getIsProduced() {
        return isProduced;
    }

    public void setIsProduced(Byte isProduced) {
        this.isProduced = isProduced;
    }

    @Basic
    @Column(name = "start_time", nullable = true)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = true)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "comments", nullable = true, length = 255)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Producer producer = (Producer) o;

        if (producerId != producer.producerId) return false;
        if (producerName != null ? !producerName.equals(producer.producerName) : producer.producerName != null)
            return false;
        if (area != null ? !area.equals(producer.area) : producer.area != null) return false;
        if (producerType != null ? !producerType.equals(producer.producerType) : producer.producerType != null)
            return false;
        if (varietyName != null ? !varietyName.equals(producer.varietyName) : producer.varietyName != null)
            return false;
        if (batchDuration != null ? !batchDuration.equals(producer.batchDuration) : producer.batchDuration != null)
            return false;
        if (principalName != null ? !principalName.equals(producer.principalName) : producer.principalName != null)
            return false;
        if (batchId != null ? !batchId.equals(producer.batchId) : producer.batchId != null) return false;
        if (plannedYield != null ? !plannedYield.equals(producer.plannedYield) : producer.plannedYield != null)
            return false;
        if (plannedPrice != null ? !plannedPrice.equals(producer.plannedPrice) : producer.plannedPrice != null)
            return false;
        if (plannedTotalPrice != null ? !plannedTotalPrice.equals(producer.plannedTotalPrice) : producer.plannedTotalPrice != null)
            return false;
        if (realYield != null ? !realYield.equals(producer.realYield) : producer.realYield != null) return false;
        if (realPrice != null ? !realPrice.equals(producer.realPrice) : producer.realPrice != null) return false;
        if (realTotalPrice != null ? !realTotalPrice.equals(producer.realTotalPrice) : producer.realTotalPrice != null)
            return false;
        if (isProduced != null ? !isProduced.equals(producer.isProduced) : producer.isProduced != null) return false;
        if (startTime != null ? !startTime.equals(producer.startTime) : producer.startTime != null) return false;
        if (endTime != null ? !endTime.equals(producer.endTime) : producer.endTime != null) return false;
        if (comments != null ? !comments.equals(producer.comments) : producer.comments != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = producerId;
        result = 31 * result + (producerName != null ? producerName.hashCode() : 0);
        result = 31 * result + (area != null ? area.hashCode() : 0);
        result = 31 * result + (producerType != null ? producerType.hashCode() : 0);
        result = 31 * result + (varietyName != null ? varietyName.hashCode() : 0);
        result = 31 * result + (batchDuration != null ? batchDuration.hashCode() : 0);
        result = 31 * result + (principalName != null ? principalName.hashCode() : 0);
        result = 31 * result + (batchId != null ? batchId.hashCode() : 0);
        result = 31 * result + (plannedYield != null ? plannedYield.hashCode() : 0);
        result = 31 * result + (plannedPrice != null ? plannedPrice.hashCode() : 0);
        result = 31 * result + (plannedTotalPrice != null ? plannedTotalPrice.hashCode() : 0);
        result = 31 * result + (realYield != null ? realYield.hashCode() : 0);
        result = 31 * result + (realPrice != null ? realPrice.hashCode() : 0);
        result = 31 * result + (realTotalPrice != null ? realTotalPrice.hashCode() : 0);
        result = 31 * result + (isProduced != null ? isProduced.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }
}

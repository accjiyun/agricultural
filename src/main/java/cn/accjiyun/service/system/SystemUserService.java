package cn.accjiyun.service.system;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.system.Department;
import cn.accjiyun.entity.system.Position;
import cn.accjiyun.entity.system.SystemUser;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface SystemUserService {
    /**
     * 创建系统用户
     * @param user 系统用户实体
     * @return 系统用户ID
     */
    public int createSystemUser(SystemUser user);

    /**
     * 通过系统用户ID数组删除系统用户
     * @param userIds 系统用户ID数组
     */
    public void deleteSystemUser(int[] userIds);

    /**
     * 通过系统用户ID查询系统用户信息
     * @param userId 庭成员ID
     * @return 系统用户实体
     */
    public SystemUser querySystemUserById(int userId);

    /**
     * 分页查询系统用户信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 系统用户实体列表
     */
    public List<SystemUser> querySystemUserPage(QueryEntity queryEntity, PageModel<SystemUser> model);

    /**
     * 获取系统用户总数
     * @return 系统用户总数
     */
    public long queryAllSystemUserCount();

    /**
     * 登录查询系统用户实体
     * @param user 系统用户实例
     * @return 如果存在用户，返回完整实体，否则为空
     */
    public SystemUser queryLoginUser(SystemUser user);

    /**
     * Validate the loginName if exist.
     * @param loginName
     * @return true if the loginName exist.
     */
    public boolean validateLoginName(String loginName);

    /**
     * 利用hql语句查找系统用户信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 系统用户实体列表
     */
    public List<SystemUser> createQuery(final String hql, final Object[] queryParams);

    /**
     * 通过名称查询部门信息
     * @param departmentId 部门ID
     * @return 对应部门实体
     */
    public Department queryDepartmentById(byte departmentId);

    /**
     * 通过名称查询部门信息
     * @param departmentName 部门名称
     * @return 对应部门实体
     */
    public Department queryDepartmentIdByName(String departmentName);

    /**
     * 查询所有的部门名称
     * @return 部门名列表
     */
    public List<String> queryAllDepartmentName();

    /**
     * 通过名称查询职位信息
     * @param positionId 职位ID
     * @return 对应职位实体
     */
    public Position queryPositionById(byte positionId);

    /**
     * 通过名称查询职位信息
     * @param positionName 职位名称
     * @return 对应职位实体
     */
    public Position queryPositionIdByName(String positionName);

    /**
     * 查询所有的职位名称
     * @return 职位名列表
     */
    public List<String> queryAllPositionName();

    /**
     * 更新系统用户信息
     * @param user 系统用户实体
     */
    public void updateSystemUser(SystemUser user);

    /**
     * 更新用户启用状态
     * @param userId 用户ID
     * @param status 状态0:正常,1:冻结
     */
    public void updateDisableOrStartUser(int userId, int status);
}

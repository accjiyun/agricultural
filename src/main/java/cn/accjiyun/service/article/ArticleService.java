package cn.accjiyun.service.article;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.article.Article;
import cn.accjiyun.entity.article.ArticleContent;
import cn.accjiyun.entity.article.ArticleType;

import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2016/11/16.
 */
public interface ArticleService {
    /**
     * 创建文章
     *
     * @param article 文章实体
     * @return 返回文章ID
     */
    public int createArticle(Article article);

    /**
     * 添加文章内容
     *
     * @param content 文章内容实体
     */
    public void addArticleContent(ArticleContent content);

    /**
     * 删除文章
     *
     * @param articleIds 文章ID数组
     */
    public void deleteArticleByIds(int[] articleIds);

    /**
     * 通过文章ID查询文章信息
     *
     * @param articleId 文章ID
     * @return Article文章实体信息
     */
    public Article queryArticleById(int articleId);

    /**
     * 通过文章ID查询文章内容
     *
     * @param articleId 文章内容
     * @return String类型文章内容
     */
    public String queryArticleContentByArticleId(int articleId);

    /**
     * 通过名称查询文章类型信息
     * @param articleTypeId 文章类型ID
     * @return 对应文章类型实体
     */
    public ArticleType queryArticleTypeById(byte articleTypeId);

    /**
     * 通过名称查询文章类型信息
     * @param typeName 文章类型名称
     * @return 对应文章类型实体
     */
    public ArticleType queryArticleTypeIdByName(String typeName);

    /**
     * 查询所有的文章类型名称
     * @return 文章类型名列表
     */
    public List<String> queryAllArticleTypeName();

    /**
     * 分页查询文章列表
     */
    public List<Article> queryArticlePage(QueryEntity query, PageModel<Article> pageModel);

    /**
     * 获取所有文章总记录数
     *
     * @return 总记录数
     */
    public int queryAllArticleCount();

    /**
     * 创建HQL查询文章文章
     * @param hql
     * @param queryParams
     * @return
     */
    public List<Article> createArticleQuery(final String hql, final Object[] queryParams);

    /**
     * 修改文章信息
     *
     * @param article 文章实体
     */
    public void updateArticle(Article article);

    /**
     * 修改文章内容
     *
     * @param content
     */
    public void updateArticleContent(ArticleContent content);

    /**
     * 修改累加文章点击量
     */
    public void updateArticleNum(Map<String, String> map);

}

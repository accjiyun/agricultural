package cn.accjiyun.service.impl.system;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.system.DepartmentDao;
import cn.accjiyun.dao.system.PositionDao;
import cn.accjiyun.dao.system.SystemUserDao;
import cn.accjiyun.entity.system.Department;
import cn.accjiyun.entity.system.Position;
import cn.accjiyun.entity.system.SystemUser;
import cn.accjiyun.service.system.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Service("systemUserService")
public class SystemUserServiceImpl implements SystemUserService{

    @Autowired
    private SystemUserDao systemUserDao;
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private PositionDao positionDao;

    /**
     * 创建系统用户
     *
     * @param user 系统用户实体
     * @return 系统用户ID
     */
    @Override
    public int createSystemUser(SystemUser user) {
        return systemUserDao.createSystemUser(user);
    }

    /**
     * 通过系统用户ID数组删除系统用户
     *
     * @param userIds 系统用户ID数组
     */
    @Override
    public void deleteSystemUser(int[] userIds) {
        systemUserDao.deleteSystemUser(userIds);
    }

    /**
     * 通过系统用户ID查询系统用户信息
     *
     * @param userId 庭成员ID
     * @return 系统用户实体
     */
    @Override
    public SystemUser querySystemUserById(int userId) {
        return systemUserDao.querySystemUserById(userId);
    }

    /**
     * 分页查询系统用户信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 系统用户实体列表
     */
    @Override
    public List<SystemUser> querySystemUserPage(QueryEntity queryEntity, PageModel<SystemUser> model) {
        return systemUserDao.querySystemUserPage(queryEntity, model);
    }

    /**
     * 获取系统用户总数
     *
     * @return 系统用户总数
     */
    @Override
    public long queryAllSystemUserCount() {
        return systemUserDao.queryAllSystemUserCount();
    }

    /**
     * 登录查询系统用户实体
     *
     * @param user 系统用户实例
     * @return 如果存在用户，返回完整实体，否则为空
     */
    @Override
    public SystemUser queryLoginUser(SystemUser user) {
        return systemUserDao.queryLoginUser(user);
    }

    /**
     * Validate the loginName if exist.
     *
     * @param loginName
     * @return true if the loginName exist.
     */
    @Override
    public boolean validateLoginName(String loginName) {
        return systemUserDao.validateLoginName(loginName);
    }

    /**
     * 利用hql语句查找系统用户信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 系统用户实体列表
     */
    @Override
    public List<SystemUser> createQuery(String hql, Object[] queryParams) {
        return systemUserDao.createQuery(hql, queryParams);
    }

    /**
     * 通过名称查询部门
     *
     * @param departmentId 部门ID
     * @return 对应部门实体
     */
    @Override
    public Department queryDepartmentById(byte departmentId) {
        return departmentDao.queryDepartmentById(departmentId);
    }

    /**
     * 通过ID查询部门名
     *
     * @param departmentName 部门名称
     * @return 对应部门实体
     */
    @Override
    public Department queryDepartmentIdByName(String departmentName) {
        return departmentDao.queryDepartmentIdByName(departmentName );
    }

    /**
     * 查询所有的部门名称
     *
     * @return 部门名列表
     */
    @Override
    public List<String> queryAllDepartmentName() {
        List<String> departmentNameList = new ArrayList<>();
        QueryEntity queryEntity = new QueryEntity();
        PageModel<Department> pageModel = new PageModel<>();
        pageModel.setPageSize((int)departmentDao.getCount());
        pageModel.setPageNo(1);
        List<Department> departmentList = departmentDao.queryDepartmentPage(queryEntity, pageModel);
        for (Department department : departmentList) {
            departmentNameList.add(department.getDepartmentName());
        }
        return departmentNameList;
    }

    /**
     * 通过名称查询职位
     *
     * @param positionId 职位ID
     * @return 对应职位实体
     */
    @Override
    public Position queryPositionById(byte positionId) {
        return positionDao.queryPositionById(positionId);
    }

    /**
     * 通过ID查询职位名
     *
     * @param positionName 职位名称
     * @return 对应职位实体
     */
    @Override
    public Position queryPositionIdByName(String positionName) {
        return positionDao.queryPositionIdByName(positionName );
    }

    /**
     * 查询所有的职位名称
     *
     * @return 职位名列表
     */
    @Override
    public List<String> queryAllPositionName() {
        List<String> positionNameList = new ArrayList<>();
        QueryEntity queryEntity = new QueryEntity();
        PageModel<Position> pageModel = new PageModel<>();
        pageModel.setPageSize((int)positionDao.getCount());
        pageModel.setPageNo(1);
        List<Position> positionList = positionDao.queryPositionPage(queryEntity, pageModel);
        for (Position position : positionList) {
            positionNameList.add(position.getPositionName());
        }
        return positionNameList;
    }

    /**
     * 更新系统用户信息
     *
     * @param user 系统用户实体
     */
    @Override
    public void updateSystemUser(SystemUser user) {
        systemUserDao.updateSystemUser(user);
    }

    /**
     * 更新用户启用状态
     *
     * @param userId 用户ID
     * @param status 状态0:正常,1:冻结
     */
    @Override
    public void updateDisableOrStartUser(int userId, int status) {
        systemUserDao.updateDisableOrStartUser(userId, status);
    }
}

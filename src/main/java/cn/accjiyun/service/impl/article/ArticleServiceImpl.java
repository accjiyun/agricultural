package cn.accjiyun.service.impl.article;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.article.ArticleContentDao;
import cn.accjiyun.dao.article.ArticleDao;
import cn.accjiyun.dao.article.ArticleTypeDao;
import cn.accjiyun.entity.article.Article;
import cn.accjiyun.entity.article.ArticleContent;
import cn.accjiyun.entity.article.ArticleType;
import cn.accjiyun.service.article.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2016/11/16.
 */
@Service("articleService")
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleDao articleDao;
    @Autowired
    private ArticleContentDao articleContentDao;
    @Autowired
    private ArticleTypeDao articleTypeDao;

    public int createArticle(Article article) {
        article.getArticleContentByArticleId().setArticleByArticleId(article);
        articleContentDao.addArticleContent(article.getArticleContentByArticleId());
        return articleDao.createArticle(article);
    }

    public void addArticleContent(ArticleContent content) {
        articleContentDao.addArticleContent(content);
    }

    /**
     * 删除文章
     *
     * @param articleIds 文章ID数组
     */
    @Override
    public void deleteArticleByIds(int[] articleIds) {
        articleDao.deleteArticleByIds(articleIds);
    }

    public Article queryArticleById(int articleId) {
        return articleDao.queryArticleById(articleId);
    }

    public String queryArticleContentByArticleId(int articleId) {
        return articleContentDao.queryArticleContentByArticleId(articleId);
    }

    /**
     * 通过名称查询文章类型
     *
     * @param articleTypeId 文章类型ID
     * @return 对应文章类型实体
     */
    @Override
    public ArticleType queryArticleTypeById(byte articleTypeId) {
        return articleTypeDao.queryArticleTypeById(articleTypeId);
    }

    /**
     * 通过ID查询文章类型名
     *
     * @param typeName 文章类型名称
     * @return 对应文章类型实体
     */
    @Override
    public ArticleType queryArticleTypeIdByName(String typeName) {
        return articleTypeDao.queryArticleTypeIdByName(typeName );
    }

    /**
     * 查询所有的文章类型名称
     *
     * @return 文章类型名列表
     */
    @Override
    public List<String> queryAllArticleTypeName() {
        List<String> typeNameList = new ArrayList<>();
        QueryEntity queryEntity = new QueryEntity();
        PageModel<ArticleType> pageModel = new PageModel<>();
        pageModel.setPageSize((int)articleTypeDao.getCount());
        pageModel.setPageNo(1);
        List<ArticleType> articleTypeList = articleTypeDao.queryArticleTypePage(queryEntity, pageModel);
        for (ArticleType articleType : articleTypeList) {
            typeNameList.add(articleType.getTypeName());
        }
        return typeNameList;
    }

    /**
     * 分页查询文章列表
     */
    public List<Article> queryArticlePage(QueryEntity query, PageModel<Article> pageModel) {
        return articleDao.queryArticlePage(query, pageModel);
    }

    /**
     * 获取所有文章总记录数
     *
     * @return 总记录数
     */
    public int queryAllArticleCount() {
        return articleDao.queryAllArticleCount();
    }

    /**
     * 创建HQL查询文章文章
     *
     * @param hql
     * @param queryParams
     * @return
     */
    @Override
    public List<Article> createArticleQuery(final String hql, final Object[] queryParams) {
        return articleDao.createQuery(hql, queryParams);
    }

    /**
     * 修改累加文章点击量
     *
     * @param map
     */
    @Override
    public void updateArticleNum(Map<String, String> map) {
        articleDao.updateArticleNum(map);
    }

    public void updateArticle(Article article) {
        articleDao.updateArticle(article);
    }

    public void updateArticleContent(ArticleContent content) {
        articleContentDao.updateArticleContent(content);
    }
}

package cn.accjiyun.service.impl.society;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.society.RelativeDao;
import cn.accjiyun.entity.society.Relative;
import cn.accjiyun.service.society.RelativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Service("relativeService")
public class RelativeServiceImpl implements RelativeService{

    @Autowired
    private RelativeDao relativeDao;

    /**
     * 创建家庭成员
     *
     * @param relative 家庭成员实体
     * @return 家庭成员ID
     */
    @Override
    public int createRelative(Relative relative) {
        return relativeDao.createRelative(relative);
    }

    /**
     * 通过家庭成员ID数组删除家庭成员
     *
     * @param relativeIds 家庭成员ID数组
     */
    @Override
    public void deleteRelative(int[] relativeIds) {
        relativeDao.deleteRelative(relativeIds);
    }

    /**
     * 通过家庭成员ID查询家庭成员信息
     *
     * @param relativeId 庭成员ID
     * @return 家庭成员实体
     */
    @Override
    public Relative queryRelativeById(int relativeId) {
        return relativeDao.queryRelativeById(relativeId);
    }

    /**
     * 分页查询家庭成员信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 家庭成员实体列表
     */
    @Override
    public List<Relative> queryRelativePage(QueryEntity queryEntity, PageModel<Relative> model) {
        return relativeDao.queryRelativePage(queryEntity, model);
    }

    /**
     * 获取家庭成员总数
     *
     * @return 家庭成员总数
     */
    @Override
    public long queryAllRelativeCount() {
        return relativeDao.getCount();
    }

    /**
     * 利用hql语句查找家庭成员信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 家庭成员实体列表
     */
    @Override
    public List<Relative> createQuery(String hql, Object[] queryParams) {
        return relativeDao.createQuery(hql, queryParams);
    }

    /**
     * 更新家庭成员信息
     *
     * @param relative 家庭成员实体
     */
    @Override
    public void updateRelative(Relative relative) {
        relativeDao.updateRelative(relative);
    }
}

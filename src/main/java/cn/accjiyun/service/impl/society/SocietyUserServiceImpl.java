package cn.accjiyun.service.impl.society;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.society.SocietyRoleDao;
import cn.accjiyun.dao.society.SocietyUserDao;
import cn.accjiyun.entity.society.SocietyRole;
import cn.accjiyun.entity.society.SocietyUser;
import cn.accjiyun.service.society.SocietyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Service("societyUserService")
public class SocietyUserServiceImpl implements SocietyUserService {

    @Autowired
    private SocietyUserDao societyUserDao;
    @Autowired
    private SocietyRoleDao societyRoleDao;
    /**
     * 创建社员
     *
     * @param user 社员实体
     * @return 社员ID
     */
    @Override
    public int createSocietyUser(SocietyUser user) {
        return societyUserDao.createSocietyUser(user);
    }

    /**
     * 通过社员ID数组删除社员
     *
     * @param userIds 社员ID数组
     */
    @Override
    public void deleteSocietyUser(int[] userIds) {
        societyUserDao.deleteSocietyUser(userIds);
    }

    /**
     * 通过社员ID查询社员信息
     *
     * @param userId 庭成员ID
     * @return 社员实体
     */
    @Override
    public SocietyUser querySocietyUserById(int userId) {
        return societyUserDao.querySocietyUserById(userId);
    }

    /**
     * 分页查询社员信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 社员实体列表
     */
    @Override
    public List<SocietyUser> querySocietyUserPage(QueryEntity queryEntity, PageModel<SocietyUser> model) {
        return societyUserDao.querySocietyUserPage(queryEntity, model);
    }

    /**
     * 获取社员总数
     *
     * @return 社员总数
     */
    @Override
    public long queryAllSocietyUserCount() {
        return societyUserDao.getCount();
    }

    /**
     * 通过名称查询社员角色
     *
     * @param societyRoleId 社员角色ID
     * @return 对应社员角色实体
     */
    @Override
    public SocietyRole querySocietyRoleById(byte societyRoleId) {
        return societyRoleDao.querySocietyRoleById(societyRoleId);
    }

    /**
     * 通过ID查询社员角色名
     *
     * @param roleName 社员角色名称
     * @return 对应社员角色实体
     */
    @Override
    public SocietyRole querySocietyRoleIdByName(String roleName) {
        return societyRoleDao.querySocietyRoleIdByName(roleName );
    }

    /**
     * 查询所有的社员角色名称
     *
     * @return 社员角色名列表
     */
    @Override
    public List<String> queryAllSocietyRoleName() {
        List<String> roleNameList = new ArrayList<>();
        QueryEntity queryEntity = new QueryEntity();
        PageModel<SocietyRole> pageModel = new PageModel<>();
        pageModel.setPageSize((int)societyRoleDao.getCount());
        pageModel.setPageNo(1);
        List<SocietyRole> societyRoleList = societyRoleDao.querySocietyRolePage(queryEntity, pageModel);
        for (SocietyRole societyRole : societyRoleList) {
            roleNameList.add(societyRole.getRoleName());
        }
        return roleNameList;
    }

    /**
     * 利用hql语句查找社员信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 社员实体列表
     */
    @Override
    public List<SocietyUser> createQuery(String hql, Object[] queryParams) {
        return societyUserDao.createQuery(hql, queryParams);
    }

    /**
     * 验证社员身份证是否有效
     *
     * @param identityCard
     * @return 社员身份证不存在则有效
     */
    @Override
    public boolean validateIDCard(String identityCard) {
        return societyUserDao.validateIDCard(identityCard);
    }

    /**
     * 更新社员信息
     *
     * @param user 社员实体
     */
    @Override
    public void updateSocietyUser(SocietyUser user) {
        societyUserDao.updateSocietyUser(user);
    }
}

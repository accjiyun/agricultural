package cn.accjiyun.service.impl.sale;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.sale.OrdersDao;
import cn.accjiyun.entity.sale.Orders;
import cn.accjiyun.service.sale.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Service("ordersService")
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersDao ordersDao;
    /**
     * 创建订单
     *
     * @param orders 订单实体
     * @return 订单ID
     */
    @Override
    public int createOrders(Orders orders) {
        return ordersDao.createOrders(orders);
    }

    /**
     * 通过订单ID数组删除订单
     *
     * @param ordersIds 订单ID数组
     */
    @Override
    public void deleteOrders(int[] ordersIds) {
        ordersDao.deleteOrders(ordersIds);
    }

    /**
     * 通过订单ID查询订单信息
     *
     * @param ordersId 庭成员ID
     * @return 订单实体
     */
    @Override
    public Orders queryOrdersById(int ordersId) {
        return ordersDao.queryOrdersById(ordersId);
    }

    /**
     * 分页查询订单信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 订单实体列表
     */
    @Override
    public List<Orders> queryOrdersPage(QueryEntity queryEntity, PageModel<Orders> model) {
        return ordersDao.queryOrdersPage(queryEntity, model);
    }

    /**
     * 更新订单信息
     *
     * @param orders 订单实体
     */
    @Override
    public void updateOrders(Orders orders) {
        ordersDao.updateOrders(orders);
    }

    /**
     * 获取订单总数
     *
     * @return 订单总数
     */
    @Override
    public long queryAllOrdersCount() {
        return ordersDao.queryAllOrdersCount();
    }

    /**
     * 利用hql语句查找订单信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 订单实体列表
     */
    @Override
    public List<Orders> createQuery(String hql, Object[] queryParams) {
        return ordersDao.createQuery(hql, queryParams);
    }
}

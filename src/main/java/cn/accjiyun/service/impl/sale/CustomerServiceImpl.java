package cn.accjiyun.service.impl.sale;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.sale.CustomerDao;
import cn.accjiyun.entity.sale.Customer;
import cn.accjiyun.service.sale.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /**
     * 创建客户
     *
     * @param customer 客户实体
     * @return 客户ID
     */
    @Override
    public int createCustomer(Customer customer) {
        return customerDao.createCustomer(customer);
    }

    /**
     * 通过客户ID数组删除客户
     *
     * @param customerIds 客户ID数组
     */
    @Override
    public void deleteCustomer(int[] customerIds) {
        customerDao.deleteCustomer(customerIds);
    }

    /**
     * 通过客户ID查询客户信息
     *
     * @param customerId 庭成员ID
     * @return 客户实体
     */
    @Override
    public Customer queryCustomerById(int customerId) {
        return customerDao.queryCustomerById(customerId);
    }

    /**
     * 分页查询客户信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 客户实体列表
     */
    @Override
    public List<Customer> queryCustomerPage(QueryEntity queryEntity, PageModel<Customer> model) {
        return customerDao.queryCustomerPage(queryEntity, model);
    }

    /**
     * 更新客户信息
     *
     * @param customer 客户实体
     */
    @Override
    public void updateCustomer(Customer customer) {
        customerDao.updateCustomer(customer);
    }

    /**
     * 获取客户总数
     *
     * @return 客户总数
     */
    @Override
    public long queryAllCustomerCount() {
        return customerDao.queryAllCustomerCount();
    }

    /**
     * 利用hql语句查找客户信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 客户实体列表
     */
    @Override
    public List<Customer> createQuery(String hql, Object[] queryParams) {
        return customerDao.createQuery(hql, queryParams);
    }
}

package cn.accjiyun.service.impl.production;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.production.CropCareDao;
import cn.accjiyun.entity.production.CropCare;
import cn.accjiyun.service.production.CropCareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Service("cropCareService")
public class CropCareServiceImpl implements CropCareService {

    @Autowired
    private CropCareDao cropCareDao;

    /**
     * 创建作物护理
     *
     * @param cropCare 作物护理实体
     * @return 作物护理ID
     */
    @Override
    public int createCropCare(CropCare cropCare) {
        return cropCareDao.createCropCare(cropCare);
    }

    /**
     * 通过作物护理ID数组删除作物护理
     *
     * @param cropCareIds 作物护理ID数组
     */
    @Override
    public void deleteCropCare(int[] cropCareIds) {
        cropCareDao.deleteCropCare(cropCareIds);
    }

    /**
     * 通过作物护理ID查询作物护理信息
     *
     * @param cropCareId 庭成员ID
     * @return 作物护理实体
     */
    @Override
    public CropCare queryCropCareById(int cropCareId) {
        return cropCareDao.queryCropCareById(cropCareId);
    }

    /**
     * 分页查询作物护理信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 作物护理实体列表
     */
    @Override
    public List<CropCare> queryCropCarePage(QueryEntity queryEntity, PageModel<CropCare> model) {
        return cropCareDao.queryCropCarePage(queryEntity, model);
    }

    /**
     * 更新作物护理信息
     *
     * @param cropCare 作物护理实体
     */
    @Override
    public void updateCropCare(CropCare cropCare) {
        cropCareDao.updateCropCare(cropCare);
    }

    /**
     * 获取作物护理总数
     *
     * @return 作物护理总数
     */
    @Override
    public long queryAllCropCareCount() {
        return cropCareDao.queryAllCropCareCount();
    }

    /**
     * 利用hql语句查找作物护理信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 作物护理实体列表
     */
    @Override
    public List<CropCare> createQuery(String hql, Object[] queryParams) {
        return cropCareDao.createQuery(hql, queryParams);
    }
}

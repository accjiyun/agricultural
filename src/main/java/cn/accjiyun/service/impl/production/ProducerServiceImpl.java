package cn.accjiyun.service.impl.production;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.production.ProducerDao;
import cn.accjiyun.entity.production.Producer;
import cn.accjiyun.service.production.ProducerService;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Service("producerService")
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    private ProducerDao producerDao;

    /**
     * 创建生产
     *
     * @param producer 生产实体
     * @return 生产ID
     */
    @Override
    public int createProducer(Producer producer) {
        return producerDao.createProducer(producer);
    }

    /**
     * 通过生产ID数组删除生产
     *
     * @param producerIds 生产ID数组
     */
    @Override
    public void deleteProducer(int[] producerIds) {
        producerDao.deleteProducer(producerIds);
    }

    /**
     * 通过生产ID查询生产信息
     *
     * @param producerId 庭成员ID
     * @return 生产实体
     */
    @Override
    public Producer queryProducerById(int producerId) {
        return producerDao.queryProducerById(producerId);
    }

    /**
     * 分页查询生产信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 生产实体列表
     */
    @Override
    public List<Producer> queryProducerPage(QueryEntity queryEntity, PageModel<Producer> model) {
        return producerDao.queryProducerPage(queryEntity, model);
    }

    /**
     * 更新生产信息
     *
     * @param producer 生产实体
     */
    @Override
    public void updateProducer(Producer producer) {
        producerDao.updateProducer(producer);
    }

    /**
     * 获取生产总数
     *
     * @return 生产总数
     */
    @Override
    public long queryAllProducerCount() {
        return producerDao.queryAllProducerCount();
    }

    /**
     * 利用hql语句查找生产信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 生产实体列表
     */
    @Override
    public List createQuery(String hql, Object[] queryParams) {
        return producerDao.createQuery(hql, queryParams);
    }

    /**
     * 分组统计查询
     *
     * @param sum     统计字段名
     * @param groupBy 分组字段名
     * @return 查询结果
     */
    @Override
    public List<Object[]> querySumGroupBy(String sum, String groupBy) {
        return producerDao.querySumGroupBy(sum, groupBy);
    }


}

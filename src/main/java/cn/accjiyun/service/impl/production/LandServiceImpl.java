package cn.accjiyun.service.impl.production;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.production.ContractTypeDao;
import cn.accjiyun.dao.production.LandDao;
import cn.accjiyun.dao.production.LandTypeDao;
import cn.accjiyun.entity.production.ContractType;
import cn.accjiyun.entity.production.Land;
import cn.accjiyun.entity.production.LandType;
import cn.accjiyun.service.production.LandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Service("landService")
public class LandServiceImpl implements LandService {

    @Autowired
    private LandDao landDao;
    @Autowired
    private ContractTypeDao contractTypeDao;
    @Autowired
    private LandTypeDao landTypeDao;

    /**
     * 创建土地
     *
     * @param land 土地实体
     * @return 土地ID
     */
    @Override
    public int createLand(Land land) {
        return landDao.createLand(land);
    }

    /**
     * 通过土地ID数组删除土地
     *
     * @param landIds 土地ID数组
     */
    @Override
    public void deleteLand(int[] landIds) {
        landDao.deleteLand(landIds);
    }

    /**
     * 通过土地ID查询土地信息
     *
     * @param landId 庭成员ID
     * @return 土地实体
     */
    @Override
    public Land queryLandById(int landId) {
        return landDao.queryLandById(landId);
    }

    /**
     * 分页查询土地信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 土地实体列表
     */
    @Override
    public List<Land> queryLandPage(QueryEntity queryEntity, PageModel<Land> model) {
        return landDao.queryLandPage(queryEntity, model);
    }

    /**
     * 更新土地信息
     *
     * @param land 土地实体
     */
    @Override
    public void updateLand(Land land) {
        landDao.updateLand(land);
    }

    /**
     * 获取土地总数
     *
     * @return 土地总数
     */
    @Override
    public long queryAllLandCount() {
        return landDao.queryAllLandCount();
    }

    /**
     * 通过名称查询合同类型
     *
     * @param contractTypeId 合同类型ID
     * @return 对应合同类型实体
     */
    @Override
    public ContractType queryContractTypeById(byte contractTypeId) {
        return contractTypeDao.queryContractTypeById(contractTypeId);
    }

    /**
     * 通过ID查询合同类型名
     *
     * @param contractTypeName 合同类型名称
     * @return 对应合同类型实体
     */
    @Override
    public ContractType queryContractTypeIdByName(String contractTypeName) {
        return contractTypeDao.queryContractTypeIdByName(contractTypeName );
    }

    /**
     * 查询所有的合同类型名称
     *
     * @return 合同类型名列表
     */
    @Override
    public List<String> queryAllContractTypeName() {
        List<String> contractTypeNameList = new ArrayList<>();
        QueryEntity queryEntity = new QueryEntity();
        PageModel<ContractType> pageModel = new PageModel<>();
        pageModel.setPageSize((int)contractTypeDao.getCount());
        pageModel.setPageNo(1);
        List<ContractType> contractTypeList = contractTypeDao.queryContractTypePage(queryEntity, pageModel);
        for (ContractType contractType : contractTypeList) {
            contractTypeNameList.add(contractType.getContractTypeName());
        }
        return contractTypeNameList;
    }

    /**
     * 通过名称查询土地类型
     *
     * @param landTypeId 土地类型ID
     * @return 对应土地类型实体
     */
    @Override
    public LandType queryLandTypeById(byte landTypeId) {
        return landTypeDao.queryLandTypeById(landTypeId);
    }

    /**
     * 通过ID查询土地类型名
     *
     * @param landTypeName 土地类型名称
     * @return 对应土地类型实体
     */
    @Override
    public LandType queryLandTypeIdByName(String landTypeName) {
        return landTypeDao.queryLandTypeIdByName(landTypeName );
    }

    /**
     * 查询所有的土地类型名称
     *
     * @return 土地类型名列表
     */
    @Override
    public List<String> queryAllLandTypeName() {
        List<String> landTypeNameList = new ArrayList<>();
        QueryEntity queryEntity = new QueryEntity();
        PageModel<LandType> pageModel = new PageModel<>();
        pageModel.setPageSize((int)landTypeDao.getCount());
        pageModel.setPageNo(1);
        List<LandType> landTypeList = landTypeDao.queryLandTypePage(queryEntity, pageModel);
        for (LandType landType : landTypeList) {
            landTypeNameList.add(landType.getLandTypeName());
        }
        return landTypeNameList;
    }

    /**
     * 利用hql语句查找土地信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 土地实体列表
     */
    @Override
    public List<Land> createQuery(String hql, Object[] queryParams) {
        return landDao.createQuery(hql, queryParams);
    }

    /**
     * 获取所有的土地信息实体
     *
     * @return 土地列表
     */
    @Override
    public List<Land> getLandList() {
        QueryEntity queryEntity = new QueryEntity();
        PageModel<Land> pageModel = new PageModel<>();
        pageModel.setPageNo(1);
        pageModel.setPageSize((int)landDao.queryAllLandCount());
        return landDao.queryLandPage(queryEntity, pageModel);
    }
}

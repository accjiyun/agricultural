package cn.accjiyun.service.sale;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.sale.Customer;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface CustomerService {
    /**
     * 创建客户
     * @param customer 客户实体
     * @return 客户ID
     */
    public int createCustomer(Customer customer);

    /**
     * 通过客户ID数组删除客户
     * @param customerIds 客户ID数组
     */
    public void deleteCustomer(int[] customerIds);

    /**
     * 通过客户ID查询客户信息
     * @param customerId 庭成员ID
     * @return 客户实体
     */
    public Customer queryCustomerById(int customerId);

    /**
     * 分页查询客户信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 客户实体列表
     */
    public List<Customer> queryCustomerPage(QueryEntity queryEntity, PageModel<Customer> model);

    /**
     * 更新客户信息
     * @param customer 客户实体
     */
    public void updateCustomer(Customer customer);

    /**
     * 获取客户总数
     * @return 客户总数
     */
    public long queryAllCustomerCount();

    /**
     * 利用hql语句查找客户信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 客户实体列表
     */
    public List<Customer> createQuery(final String hql, final Object[] queryParams);

}

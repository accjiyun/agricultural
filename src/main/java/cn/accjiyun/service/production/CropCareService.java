package cn.accjiyun.service.production;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.production.CropCare;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface CropCareService {
    /**
     * 创建作物护理
     * @param cropCare 作物护理实体
     * @return 作物护理ID
     */
    public int createCropCare(CropCare cropCare);

    /**
     * 通过作物护理ID数组删除作物护理
     * @param cropCareIds 作物护理ID数组
     */
    public void deleteCropCare(int[] cropCareIds);

    /**
     * 通过作物护理ID查询作物护理信息
     * @param cropCareId 庭成员ID
     * @return 作物护理实体
     */
    public CropCare queryCropCareById(int cropCareId);

    /**
     * 分页查询作物护理信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 作物护理实体列表
     */
    public List<CropCare> queryCropCarePage(QueryEntity queryEntity, PageModel<CropCare> model);

    /**
     * 更新作物护理信息
     * @param cropCare 作物护理实体
     */
    public void updateCropCare(CropCare cropCare);

    /**
     * 获取作物护理总数
     * @return 作物护理总数
     */
    public long queryAllCropCareCount();

    /**
     * 利用hql语句查找作物护理信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 作物护理实体列表
     */
    public List<CropCare> createQuery(final String hql, final Object[] queryParams);
}

package cn.accjiyun.service.production;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.production.ContractType;
import cn.accjiyun.entity.production.Land;
import cn.accjiyun.entity.production.LandType;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface LandService {

    /**
     * 创建土地
     * @param land 土地实体
     * @return 土地ID
     */
    public int createLand(Land land);

    /**
     * 通过土地ID数组删除土地
     * @param landIds 土地ID数组
     */
    public void deleteLand(int[] landIds);

    /**
     * 通过土地ID查询土地信息
     * @param landId 庭成员ID
     * @return 土地实体
     */
    public Land queryLandById(int landId);

    /**
     * 分页查询土地信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 土地实体列表
     */
    public List<Land> queryLandPage(QueryEntity queryEntity, PageModel<Land> model);

    /**
     * 更新土地信息
     * @param land 土地实体
     */
    public void updateLand(Land land);

    /**
     * 获取土地总数
     * @return 土地总数
     */
    public long queryAllLandCount();

    /**
     * 通过名称查询合同类型信息
     * @param contractTypeId 合同类型ID
     * @return 对应合同类型实体
     */
    public ContractType queryContractTypeById(byte contractTypeId);

    /**
     * 通过名称查询合同类型信息
     * @param contractTypeName 合同类型名称
     * @return 对应合同类型实体
     */
    public ContractType queryContractTypeIdByName(String contractTypeName);

    /**
     * 查询所有的合同类型名称
     * @return 合同类型名列表
     */
    public List<String> queryAllContractTypeName();

    /**
     * 通过名称查询土地类型信息
     * @param landTypeId 土地类型ID
     * @return 对应土地类型实体
     */
    public LandType queryLandTypeById(byte landTypeId);

    /**
     * 通过名称查询土地类型信息
     * @param landTypeName 土地类型名称
     * @return 对应土地类型实体
     */
    public LandType queryLandTypeIdByName(String landTypeName);

    /**
     * 查询所有的土地类型名称
     * @return 土地类型名列表
     */
    public List<String> queryAllLandTypeName();

    /**
     * 利用hql语句查找土地信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 土地实体列表
     */
    public List<Land> createQuery(final String hql, final Object[] queryParams);

    /**
     * 获取所有的土地信息实体
     * @return 土地列表
     */
    public List<Land> getLandList();
}

package cn.accjiyun.service.production;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.production.Producer;
import org.hibernate.Criteria;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface ProducerService {
    /**
     * 创建生产
     * @param producer 生产实体
     * @return 生产ID
     */
    public int createProducer(Producer producer);

    /**
     * 通过生产ID数组删除生产
     * @param producerIds 生产ID数组
     */
    public void deleteProducer(int[] producerIds);

    /**
     * 通过生产ID查询生产信息
     * @param producerId 庭成员ID
     * @return 生产实体
     */
    public Producer queryProducerById(int producerId);

    /**
     * 分页查询生产信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 生产实体列表
     */
    public List<Producer> queryProducerPage(QueryEntity queryEntity, PageModel<Producer> model);

    /**
     * 更新生产信息
     * @param producer 生产实体
     */
    public void updateProducer(Producer producer);

    /**
     * 获取生产总数
     * @return 生产总数
     */
    public long queryAllProducerCount();

    /**
     * 利用hql语句查找生产信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 生产实体列表
     */
    public List createQuery(final String hql, final Object[] queryParams);

    /**
     * 分组统计查询
     *
     * @param sum     统计字段名
     * @param groupBy 分组字段名
     * @return 查询结果
     */
    public List<Object[]> querySumGroupBy(String sum, String groupBy);
}

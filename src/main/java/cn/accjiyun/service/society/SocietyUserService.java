package cn.accjiyun.service.society;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.society.SocietyRole;
import cn.accjiyun.entity.society.SocietyUser;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface SocietyUserService {
    /**
     * 创建社员
     * @param user 社员实体
     * @return 社员ID
     */
    public int createSocietyUser(SocietyUser user);

    /**
     * 通过社员ID数组删除社员
     * @param userIds 社员ID数组
     */
    public void deleteSocietyUser(int[] userIds);

    /**
     * 通过社员ID查询社员信息
     * @param userId 庭成员ID
     * @return 社员实体
     */
    public SocietyUser querySocietyUserById(int userId);

    /**
     * 分页查询社员信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 社员实体列表
     */
    public List<SocietyUser> querySocietyUserPage(QueryEntity queryEntity, PageModel<SocietyUser> model);

    /**
     * 更新社员信息
     * @param user 社员实体
     */
    public void updateSocietyUser(SocietyUser user);

    /**
     * 获取社员总数
     * @return 社员总数
     */
    public long queryAllSocietyUserCount();

    /**
     * 通过名称查询社员角色信息
     * @param departmentId 社员角色ID
     * @return 对应社员角色实体
     */
    public SocietyRole querySocietyRoleById(byte departmentId);

    /**
     * 通过名称查询社员角色信息
     * @param roleName 社员角色名称
     * @return 对应社员角色实体
     */
    public SocietyRole querySocietyRoleIdByName(String roleName);

    /**
     * 查询所有的社员角色名称
     * @return 社员角色名列表
     */
    public List<String> queryAllSocietyRoleName();

    /**
     * 利用hql语句查找社员信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 社员实体列表
     */
    public List<SocietyUser> createQuery(final String hql, final Object[] queryParams);

    /**
     * 验证社员身份证是否有效
     *
     * @param identityCard
     * @return 社员身份证不存在则有效
     */
    public boolean validateIDCard(String identityCard);
}

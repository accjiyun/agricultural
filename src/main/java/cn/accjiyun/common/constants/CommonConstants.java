package cn.accjiyun.common.constants;

import cn.accjiyun.common.utils.PropertyUtil;

/**
 * Created by jiyun on 2016/11/24.
 */
public class CommonConstants {

    /**
     * 图片验证码Session的K
     */
    public static final String RAND_CODE = "COMMON_RAND_CODE";
    public static final String propertyFile = "config";
    public static final PropertyUtil propertyUtil = PropertyUtil.getInstance(propertyFile);
    public static final String MYDOMAIN = propertyUtil.getProperty("mydomain");
    public static String contextPath = propertyUtil.getProperty("contextPath");
    public static String staticServer = propertyUtil.getProperty("contextPath");
    public static String uploadImageServer = propertyUtil.getProperty("contextPath");
    public static String staticImage = propertyUtil.getProperty("contextPath");
    public static String projectName = propertyUtil.getProperty("projectName");
    /**
     * 邮箱正则表达式
     */
    public static final String emailRegex = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
    /**
     * 电话号码正则表达式
     */
    public static final String telRegex = "^1[0-9]{10}$";
    /**
     * 后台用户登录名正则表达式
     */
    public static final String loginRegex = "^(?=.*[a-zA-Z])[a-zA-Z0-9]{6,20}$";
}
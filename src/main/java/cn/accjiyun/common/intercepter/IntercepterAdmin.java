package cn.accjiyun.common.intercepter;


import cn.accjiyun.common.utils.SingletonLoginUtils;
import cn.accjiyun.entity.system.SystemUser;
import cn.accjiyun.service.system.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by jiyun on 2017/2/11.
 */
public class IntercepterAdmin extends HandlerInterceptorAdapter {

    @Autowired
    private SystemUserService systemUserService;

    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }


    public void afterConcurrentHandlingStarted(HttpServletRequest request,
                                               HttpServletResponse response, Object handler) throws Exception {
        super.afterConcurrentHandlingStarted(request, response, handler);
    }


    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }

    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        //获取登录的用户
        SystemUser systemUser = systemUserService.querySystemUserById(SingletonLoginUtils.getLoginSystemUserId(request));
        if (systemUser == null) {
            response.sendRedirect("/admin");//跳转登录页面
            return false;
        }
        return true;
    }

}

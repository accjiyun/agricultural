package cn.accjiyun.common.converter;

import cn.accjiyun.common.utils.WebUtils;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;

/**
 * Created by jiyun on 4/16/2017.
 */
public class TimestampConverter implements Converter<String, Timestamp> {

    @Override
    public Timestamp convert(String timeStr) {
        if (WebUtils.isValidateRealString(timeStr)) {
            return WebUtils.stringToTimestamp(timeStr);
        }
        return null;
    }


}
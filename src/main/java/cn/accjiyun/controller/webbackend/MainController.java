package cn.accjiyun.controller.webbackend;

import cn.accjiyun.common.controller.BaseController;
import cn.accjiyun.common.utils.SingletonLoginUtils;
import cn.accjiyun.entity.system.SystemUser;
import cn.accjiyun.service.article.ArticleService;
import cn.accjiyun.service.production.CropCareService;
import cn.accjiyun.service.production.LandService;
import cn.accjiyun.service.production.ProducerService;
import cn.accjiyun.service.society.SocietyUserService;
import cn.accjiyun.service.system.SystemUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jiyun on 2017/2/5.
 */
@Controller
@RequestMapping("/admin/main")
public class MainController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);
    private static String mainPage = getViewPath("/admin//main/main");//后台管理主界面
    private static String mainIndexPage = getViewPath("/admin/main/welcome");//后台操作中心初始化首页
    private static String loginFail = "redirect:/admin";//后台管理主界面

    @Autowired
    private SocietyUserService societyUserService;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private SystemUserService systemUserService;
    @Autowired
    private LandService landService;
    @Autowired
    private ProducerService producerService;
    @Autowired
    private CropCareService cropCareService;

    /**
     * 进入操作中心
     *
     * @param request
     * @return ModelAndView
     */
    @RequestMapping("/main")
    public ModelAndView mainPage(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        try {
            SystemUser systemUser = SingletonLoginUtils.getLoginSystemUser(request);
            if (systemUser != null) {
                model.setViewName(mainPage);
                model.addObject("systemUser", systemUser);
                model.addObject("societyUser",
                        societyUserService.querySocietyUserById(systemUser.getUserId()));
            } else {
                model.setViewName(loginFail);
            }
        } catch (Exception e) {
            model.setViewName(this.setExceptionRequest(request, e));
            logger.error("MainController.mainPage()--error", e);
        }
        return model;
    }

    /**
     * 后台操作中心初始化首页
     *
     * @param request
     * @return ModelAndView
     */
    @RequestMapping("/welcome")
    public ModelAndView mainIndex(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        try {
            SystemUser systemUser = SingletonLoginUtils.getLoginSystemUser(request);
            model.addObject("societyUser", societyUserService.querySocietyUserById(systemUser.getUserId()));
            model.addObject("positionId", systemUser.getPositionByPositionId().getPositionId());

            model.addObject("societyUserCount", societyUserService.queryAllSocietyUserCount());
            model.addObject("systemUserCount", systemUserService.queryAllSystemUserCount());
            model.addObject("articleCount", articleService.queryAllArticleCount());
            model.addObject("landCount", landService.queryAllLandCount());
            model.addObject("producerCount", producerService.queryAllProducerCount());
            model.addObject("cropCareCount", cropCareService.queryAllCropCareCount());
            model.setViewName(mainIndexPage);
        } catch (Exception e) {
            model.setViewName(this.setExceptionRequest(request, e));
            logger.error("MainController.mainIndex()---error", e);
        }
        return model;
    }

    /**
     * 访问权限受限制跳转
     *
     * @return ModelAndView
     */
    @RequestMapping("/loginFail")
    public ModelAndView notFunctionMsg() {
        ModelAndView model = new ModelAndView();
        model.addObject("message", "对不起，您没有操作权限！");
        model.setViewName(loginFail);
        return model;
    }

}

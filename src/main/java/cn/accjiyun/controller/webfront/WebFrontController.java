package cn.accjiyun.controller.webfront;

import cn.accjiyun.common.controller.BaseController;
import cn.accjiyun.service.article.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jiyun on 2017/2/1.
 */
@Controller
public class WebFrontController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(WebFrontController.class);
    private static String indexPage = getViewPath("/web/index");//后台管理主界面

    @Autowired
    private ArticleService articleService;

    /**
     * 首页获取网站首页数据
     */
    @RequestMapping("/index")
    public ModelAndView getIndexpage() {
        return new ModelAndView(indexPage);
    }


}

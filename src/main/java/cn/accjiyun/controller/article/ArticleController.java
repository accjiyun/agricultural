package cn.accjiyun.controller.article;

import cn.accjiyun.common.controller.BaseController;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.common.utils.WebUtils;
import cn.accjiyun.entity.article.Article;
import cn.accjiyun.service.article.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/2/3.
 */
@Controller
public class ArticleController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(ArticleController.class);
    // 文章列表
    private static String newsList = getViewPath("/web/article-list");
    // 文章详情
    private static String articleInfo = getViewPath("/web/article-info");

    @Autowired
    private ArticleService articleService;

    /**
     * 分页查询文章列表
     */
    @RequestMapping("/article/{articleTypeId}")
    public ModelAndView showArticleList(HttpServletRequest request, @ModelAttribute("queryArticle") QueryEntity queryArticle,
                                        @ModelAttribute("page") PageModel<Article> page, @PathVariable("articleTypeId") byte articleTypeId) {
        ModelAndView model = new ModelAndView();
        try {
            // 查询已经发布的文章资讯
            Map eqParams = new HashMap();
            eqParams.put("articleTypeByArticleTypeId.articleTypeId", articleTypeId);
            eqParams.put("status", (byte) 1);
            queryArticle.setEqParams(eqParams);
            Map<String, String> orderBy = new HashMap<>();
            orderBy.put("publicTime", "desc");
            queryArticle.setOrderBy(orderBy);
            page.setPageSize(10);
            List<Article> articleList = articleService.queryArticlePage(queryArticle, page);
            for (Article article : articleList) {
                String summary = WebUtils.html2Text(article.getArticleContentByArticleId().getContent());
                if (summary.length() > 300) {
                    summary = summary.substring(0, 300) + " ...";
                }
                article.setSummary(summary.replaceAll("\\s*", ""));
            }
            model.addObject("articleList", articleList);
            model.addObject("page", page);
            model.addObject("articleType", articleService.queryArticleTypeById(articleTypeId));
            model.setViewName(newsList);
        } catch (Exception e) {
            model.setViewName(this.setExceptionRequest(request, e));
            logger.error("showArticleList()--error", e);
        }
        return model;
    }

    /**
     * 文章详情
     */
    @RequestMapping("/article/articleInfo/{id}.html")
    public String articleInfo(HttpServletRequest request, @PathVariable("id") int id) {
        try {
            // 查询文章详情
            Article article = articleService.queryArticleById(id);
            String content = articleService.queryArticleContentByArticleId(id);
            request.setAttribute("content", content);
            request.setAttribute("article", article);
            Map<String, String> map = new HashMap<>();
            map.put("num", "+1");
            map.put("type", "clickNum");
            map.put("articleId", article.getArticleId() + "");
            articleService.updateArticleNum(map);
        } catch (Exception e) {
            logger.error("articleInfo()--error", e);
            return this.setExceptionRequest(request, e);
        }
        return articleInfo;
    }

    /**
     * 修改文章点击数量
     */
    @RequestMapping("/updateArticleClickNum/{articleId}")
    @ResponseBody
    public Map<String, Object> updateArticleClickNum(HttpServletRequest request, @PathVariable("articleId") int articleId) {
        Map<String, Object> json = new HashMap<>();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("num", "+1");
            map.put("type", "clickNum");
            map.put("articleId", articleId + "");
            articleService.updateArticleNum(map);
            Article article = articleService.queryArticleById(articleId);
            json = this.setJson(true, null, article);
        } catch (Exception e) {
            this.setAjaxException(json);
            logger.error("updateArticleClickNum()--error", e);
        }
        return json;
    }

}

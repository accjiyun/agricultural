package cn.accjiyun.controller.system;

import cn.accjiyun.common.controller.BaseController;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.common.utils.MD5;
import cn.accjiyun.common.utils.SingletonLoginUtils;
import cn.accjiyun.common.utils.WebUtils;
import cn.accjiyun.entity.society.SocietyUser;
import cn.accjiyun.entity.system.SystemUser;
import cn.accjiyun.service.society.SocietyUserService;
import cn.accjiyun.service.system.SystemUserService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/5/2.
 */
@Controller
@RequestMapping("/admin/system")
public class AdminSystemUserController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(AdminSystemUserController.class);

    private static String addSysUserPage = getViewPath("/admin/system/sys-user-add");
    private static String sysUserListPage = getViewPath("/admin/system/sys-user-list");
    private static String userPassPage = getViewPath("/admin/system/sys-user-update");

    @Autowired
    private SystemUserService systemUserService;
    @Autowired
    private SocietyUserService societyUserService;

    @InitBinder({"systemUser"})
    public void initBinderSysUser(WebDataBinder binder) {
        binder.setFieldDefaultPrefix("systemUser.");
    }

    /**
     * 创建用户
     *
     * @param systemUser 用户实体
     * @return Map<String,Object>
     */
    @RequestMapping("/createSystemUser")
    @ResponseBody
    public String createSysUser(HttpServletRequest request,
                                @ModelAttribute("systemUser") SystemUser systemUser) {
        JsonObject resultJson = new JsonObject();
        try {
            boolean isExist = systemUserService.validateLoginName(systemUser.getLoginName());
            if (isExist) {
                resultJson.addProperty("status", 400);
                resultJson.addProperty("msg", "用户名已存在！");
                return gson.toJson(resultJson);
            }
            String departmentName = request.getParameter("departmentName");
            String positionName = request.getParameter("positionName");
            systemUser.setDepartmentByDepartmentId(systemUserService.queryDepartmentIdByName(departmentName));
            systemUser.setPositionByPositionId(systemUserService.queryPositionIdByName(positionName));
            systemUser.setLoginName(systemUser.getLoginName().trim());
            systemUser.setPassword(MD5.getMD5(systemUser.getPassword()));
            systemUser.setSocietyUserByUserId(societyUserService.querySocietyUserById(systemUser.getUserId()));
            systemUserService.createSystemUser(systemUser);
            resultJson.addProperty("status", 200);
            resultJson.addProperty("msg", "添加成功");
            resultJson.addProperty("url", "/admin/system/userList");
        } catch (Exception e) {
            logger.error("AdminSocietyUser.createSysUser()--error", e);
            resultJson.addProperty("status", 400);
            resultJson.addProperty("msg", "添加失败！");
        }
        logger.info(gson.toJson(resultJson));
        return gson.toJson(resultJson);
    }

    @RequestMapping("/validateLoginName")
    @ResponseBody
    public String validateLoginName(HttpServletRequest request) {
        JsonObject resultJson = new JsonObject();
        try {
            boolean isExist = systemUserService.validateLoginName(request.getParameter("loginName"));
            if (isExist) {
                resultJson.addProperty("status", 200);
                resultJson.addProperty("msg", "用户名已存在！");
            }
        } catch (Exception e) {
            logger.error("AdminSocietyUser.validateLoginName()--error", e);
            resultJson.addProperty("status", 400);
        }
        logger.info(gson.toJson(resultJson));
        return gson.toJson(resultJson);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(HttpServletRequest request) {
        JsonObject resultJson = new JsonObject();
        try {
            String[] userIdsString = request.getParameter("userId").split(",");
            int[] userIds = new int[userIdsString.length];
            for (int i = 0; i < userIdsString.length; i++) {
                int userId = Integer.valueOf(userIdsString[i]);
                if (checkAdmin(userId, resultJson)) {
                    return gson.toJson(resultJson);
                }
                userIds[i] = userId;
            }
            if (userIdsString != null && userIdsString.length > 0) {
                systemUserService.deleteSystemUser(userIds);
            }
            resultJson.addProperty("status", 200);
            resultJson.addProperty("msg", "删除成功");
            resultJson.addProperty("url", "reload");
        } catch (Exception e) {
            logger.error("AdminSocietyUser.delete()--error", e);
            resultJson.addProperty("status", 400);
            resultJson.addProperty("msg", "删除失败！");
        }
        logger.info(gson.toJson(resultJson));
        return gson.toJson(resultJson);
    }

    @RequestMapping("/getUserList")
    @ResponseBody
    public String getUserList(HttpServletRequest request) {
        JsonObject resultJson = new JsonObject();
        try {
            QueryEntity queryEntity = new QueryEntity();
            Map eqParams = new HashMap();
            Map likeStrings = new HashMap();
            if (request.getParameter("disabled") != null && !request.getParameter("disabled").equals("-1")) {
                eqParams.put("disabled", Byte.valueOf(request.getParameter("disabled")));
            }
            String departmentName = request.getParameter("departmentName");
            if (WebUtils.isValidateRealString(departmentName)) {
                eqParams.put("departmentByDepartmentId.departmentId", systemUserService.queryDepartmentIdByName(departmentName).getDepartmentId());
            }
            likeStrings.put("loginName", request.getParameter("keyword"));
            queryEntity.setEqParams(eqParams);
            queryEntity.setLikeStrings(likeStrings);
            PageModel<SystemUser> page = new PageModel();
            page.setPageSize(5);
            int pageNo = Integer.valueOf(request.getParameter("pageNo"));
            page.setPageNo(pageNo);
            List<SystemUser> systemUserList = systemUserService.querySystemUserPage(queryEntity, page);
            JsonArray array = new JsonArray();
            for (SystemUser user : systemUserList) {
                JsonObject list = new JsonObject();
                list.addProperty("userId", user.getUserId());
                list.addProperty("loginName", user.getLoginName());
                list.addProperty("disabled", user.getDisabled());
                list.addProperty("departmentName",
                        user.getDepartmentByDepartmentId().getDepartmentName());
                list.addProperty("positionName", user.getPositionByPositionId().getPositionName());
                list.addProperty("username", user.getSocietyUserByUserId().getUsername());
                array.add(list);
            }
            resultJson.addProperty("status", 200);
            resultJson.addProperty("pages", page.getTotalPageSize());
            JsonObject list = new JsonObject();
            list.add("list", array);
            resultJson.add("data", list);
        } catch (Exception e) {
            logger.error("AdminSocietyUser.getUserList()--error", e);
            resultJson.addProperty("status", 400);
            resultJson.addProperty("msg", "获取列表失败！");
        }
        logger.info(gson.toJson(resultJson));
        return gson.toJson(resultJson);
    }

    @RequestMapping("/update")
    @ResponseBody
    public String update(HttpServletRequest request, SystemUser systemUser) {
        JsonObject resultJson = new JsonObject();
        try {
            SystemUser old = systemUserService.querySystemUserById(systemUser.getUserId());
            systemUser.setLevel(old.getLevel());
            systemUser.setPassword(MD5.getMD5(old.getPassword()));
            String departmentName = request.getParameter("departmentName");
            String positionName = request.getParameter("positionName");
            systemUser.setDepartmentByDepartmentId(systemUserService.queryDepartmentIdByName(departmentName));
            systemUser.setPositionByPositionId(systemUserService.queryPositionIdByName(positionName));
            if (!positionName.equals("0") && checkAdmin(systemUser.getUserId(), resultJson)) {
                return gson.toJson(resultJson);
            }
            systemUserService.updateSystemUser(systemUser);
            resultJson.addProperty("status", 200);
            resultJson.addProperty("url", "reload");
            resultJson.addProperty("msg", "更新成功");
        } catch (Exception e) {
            logger.error("AdminSocietyUser.update()--error", e);
            resultJson.addProperty("status", 400);
            resultJson.addProperty("msg", "更新失败！");
        }
        logger.info(gson.toJson(resultJson));
        return gson.toJson(resultJson);
    }

    @RequestMapping("updateStatus")
    @ResponseBody
    public String updateStatus(HttpServletRequest request) {
        JsonObject resultJson = new JsonObject();
        try {
            int userId = Integer.valueOf(request.getParameter("userId"));
            byte disabled = 0;
            if (request.getParameter("disabled").equals("false")) {
                disabled = 1;
            }

            SystemUser systemUser = systemUserService.querySystemUserById(userId);
            systemUser.setDisabled(disabled);
            systemUserService.updateSystemUser(systemUser);
            resultJson.addProperty("status", 200);
            resultJson.addProperty("msg", "更新成功");
        } catch (Exception e) {
            logger.error("AdminSocietyUser.updateStatus()--error", e);
            resultJson.addProperty("status", 400);
            resultJson.addProperty("msg", "更新失败！");
        }
        logger.info(gson.toJson(resultJson));
        return gson.toJson(resultJson);
    }

    /**
     * 修改用户密码
     *
     * @return Map<String,Object>
     */
    @RequestMapping("/updatePWD/{userId}")
    @ResponseBody
    public String updatePWD(HttpServletRequest request, @PathVariable("userId") int userId) {
        JsonObject resultJson = new JsonObject();
        try {
            String password = request.getParameter("password");
            if (userId == SingletonLoginUtils.getLoginSystemUserId(request) &&
                    userId > 0 && password != null && password.trim().length() > 0) {
                SystemUser systemUser = systemUserService.querySystemUserById(userId);
                systemUser.setPassword(MD5.getMD5(password));
                systemUserService.updateSystemUser(systemUser);

                resultJson.addProperty("status", 200);
                resultJson.addProperty("msg", "密码修改成功");
            } else {
                resultJson.addProperty("status", 400);
                resultJson.addProperty("msg", "你无权操作！");
            }
        } catch (Exception e) {
            logger.error("UserController.updatePWD()--error", e);
            resultJson.addProperty("status", 400);
            resultJson.addProperty("msg", "更新失败！");
        }
        logger.info(gson.toJson(resultJson));
        return gson.toJson(resultJson);
    }

    public boolean checkAdmin(int userId, JsonObject resultJson) {
        if (userId == 1) {
            resultJson.addProperty("status", 400);
            resultJson.addProperty("msg", "不能对管理员账号操作！");
            return true;
        } else {
            return false;
        }
    }

    /**
     * 初始化修改页面
     */
    @RequestMapping("/initUserUpdatePage")
    public ModelAndView initUserUpdatePage(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        model.setViewName(userPassPage);
        SystemUser systemUser = systemUserService.querySystemUserById(SingletonLoginUtils.getLoginSystemUserId(request));
        model.addObject("systemUser", systemUser);
        return model;
    }

    @RequestMapping("/userAdd")
    public ModelAndView initUserAddPage() {
        ModelAndView model = new ModelAndView();
        QueryEntity queryEntity = new QueryEntity();
        Map eqParams = new HashMap();
        eqParams.put("societyRoleByRoleTypeId.roleTypeId", (byte) 1);
        queryEntity.setEqParams(eqParams);
        PageModel<SocietyUser> pageModel = new PageModel<>();
        pageModel.setPageNo(1);
        pageModel.setPageSize((int) societyUserService.queryAllSocietyUserCount());
        List<SocietyUser> societyUserList = societyUserService.querySocietyUserPage(queryEntity, pageModel);
        model.addObject("societyUserList", societyUserList);
        model.addObject("departmentNameList", systemUserService.queryAllDepartmentName());
        model.addObject("positionNameList", systemUserService.queryAllPositionName());
        model.setViewName(addSysUserPage);
        return model;
    }

    @RequestMapping("/userList")
    public ModelAndView initUserListPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("departmentNameList", systemUserService.queryAllDepartmentName());
        model.addObject("positionNameList", systemUserService.queryAllPositionName());
        model.setViewName(sysUserListPage);
        return model;
    }

}

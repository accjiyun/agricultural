package cn.accjiyun.dao.impl.article;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.article.ArticleTypeDao;
import cn.accjiyun.entity.article.ArticleType;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("articleTypeDao")
public class ArticleTypeDaoImpl extends DaoSupport<ArticleType> implements ArticleTypeDao {

    /**
     * 创建文章类型
     *
     * @param articleType 文章类型实体
     * @return 文章类型ID
     */
    @Override
    public byte createArticleType(ArticleType articleType) {
        save(articleType);
        return articleType.getArticleTypeId();
    }

    /**
     * 通过文章类型ID数组删除文章类型
     *
     * @param articleTypeIds 文章类型ID数组
     */
    @Override
    public void deleteArticleType(byte[] articleTypeIds) {
        for (int i = 0; i < articleTypeIds.length; i++) {
            delete(articleTypeIds[i]);
        }
    }

    /**
     * 通过文章类型ID查询文章类型信息
     *
     * @param articleTypeId 庭成员ID
     * @return 文章类型实体
     */
    @Override
    public ArticleType queryArticleTypeById(byte articleTypeId) {
        return get(articleTypeId);
    }

    /**
     * 通过文章类型名查询对应ID
     *
     * @param typeName 文章类型名称
     * @return 对应文章类型ID
     */
    @Override
    public ArticleType queryArticleTypeIdByName(String typeName) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("typeName", typeName));
        ArticleType articleType = (ArticleType) criteria.uniqueResult();
        return articleType;
    }

    /**
     * 分页查询文章类型信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 文章类型实体列表
     */
    @Override
    public List<ArticleType> queryArticleTypePage(QueryEntity queryEntity, PageModel<ArticleType> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取文章类型总数
     *
     * @return 文章类型总数
     */
    @Override
    public long queryAllArticleTypeCount() {
        return getCount();
    }

    /**
     * 更新文章类型信息
     *
     * @param articleType 文章类型实体
     */
    @Override
    public void updateArticleType(ArticleType articleType) {
        saveOrUpdate(articleType);
    }
}

package cn.accjiyun.dao.impl.article;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.article.ArticleDao;
import cn.accjiyun.entity.article.Article;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2016/11/13.
 */
@Repository("articleDao")
public class ArticleDaoImpl extends DaoSupport<Article> implements ArticleDao {
    /**
     * 创建文章
     *
     * @param article 文章实体
     * @return 返回文章ID
     */
    @Override
    public int createArticle(Article article) {
        save(article);
        return article.getArticleId();
    }

    /**
     * 修改文章信息
     *
     * @param article 文章实体
     */
    @Override
    public void updateArticle(Article article) {
        saveOrUpdate(article);
    }

    /**
     * 删除文章
     *
     * @param articleIds 文章ID
     */
    @Override
    public void deleteArticleByIds(int[] articleIds) {
        for (int i = 0; i < articleIds.length; i++) {
            delete(articleIds[i]);
        }
    }

    /**
     * 通过文章ID查询文章信息
     *
     * @param articleId 文章ID
     * @return Article文章实体信息
     */
    @Override
    public Article queryArticleById(int articleId) {
        return get(articleId);
    }

    /**
     * 分页查询文章列表
     *
     * @param query     查询条件
     * @param pageModel 分页条件
     * @return List<Article>文章列表
     */
    @Override
    public List<Article> queryArticlePage(QueryEntity query, PageModel<Article> pageModel) {
        return queryByCriteria(query, pageModel);
    }

    /**
     * 获取所有文章总记录数
     *
     * @return 总记录数
     */
    @Override
    public int queryAllArticleCount() {
        return (int) getCount();
    }

    /**
     * 利用hql语句查找信息
     *
     * @param hql
     * @param queryParams
     * @return
     */
    public List<Article> createQuery(final String hql, final Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);//设置查询参数
        return query.list();
    }

    /**
     * 修改累加文章点击量
     *
     * @param map
     */
    @Override
    public void updateArticleNum(Map<String, String> map) {
        Article article = get(Integer.valueOf(map.get("articleId")));
        if (map.get("num").equals("+1")) {
            article.setClickNum(article.getClickNum() + 1);
        } else {
            article.setClickNum(Integer.valueOf(map.get("num")));
        }
        save(article);
    }
}

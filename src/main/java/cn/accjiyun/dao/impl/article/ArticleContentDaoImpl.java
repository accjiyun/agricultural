package cn.accjiyun.dao.impl.article;


import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.dao.article.ArticleContentDao;
import cn.accjiyun.entity.article.ArticleContent;
import org.springframework.stereotype.Repository;

/**
 * Created by jiyun on 2016/11/17.
 */
@Repository("articleContentDao")
public class ArticleContentDaoImpl extends DaoSupport<ArticleContent> implements ArticleContentDao {
    /**
     * 添加文章内容
     *
     * @param content 文章内容实体
     */
    @Override
    public void addArticleContent(ArticleContent content) {
        save(content);
    }

    /**
     * 删除文章内容
     *
     * @param articleIds 文章ID
     */
    @Override
    public void deleteArticleContentByArticleIds(int[] articleIds) {
        for (int i = 0; i < articleIds.length; i++) {
            delete(articleIds[i]);
        }
    }

    /**
     * 通过文章ID查询文章内容
     *
     * @param articleId 文章ID
     * @return String类型文章内容
     */
    @Override
    public String queryArticleContentByArticleId(int articleId) {
        return get(articleId).getContent();
    }

    /**
     * 修改文章内容
     *
     * @param content
     */
    @Override
    public void updateArticleContent(ArticleContent content) {
        saveOrUpdate(content);
    }
}

package cn.accjiyun.dao.impl.society;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.society.SocietyRoleDao;
import cn.accjiyun.entity.society.SocietyRole;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("societyRoleDao")
public class SocietyRoleDaoImpl extends DaoSupport<SocietyRole> implements SocietyRoleDao {

    /**
     * 创建土地类型
     *
     * @param societyRole 土地类型实体
     * @return 土地类型ID
     */
    @Override
    public byte createSocietyRole(SocietyRole societyRole) {
        save(societyRole);
        return societyRole.getRoleTypeId();
    }

    /**
     * 通过土地类型ID数组删除土地类型
     *
     * @param societyRoleIds 土地类型ID数组
     */
    @Override
    public void deleteSocietyRole(byte[] societyRoleIds) {
        for (int i = 0; i < societyRoleIds.length; i++) {
            delete(societyRoleIds[i]);
        }
    }

    /**
     * 通过土地类型ID查询土地类型信息
     *
     * @param societyRoleId 庭成员ID
     * @return 土地类型实体
     */
    @Override
    public SocietyRole querySocietyRoleById(byte societyRoleId) {
        return get(societyRoleId);
    }

    /**
     * 通过名称查询社员角色实体
     *
     * @param roleName 社员角色名
     * @return 对应社员角色实体
     */
    @Override
    public SocietyRole querySocietyRoleIdByName(String roleName) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("roleName", roleName));
        SocietyRole societyRole = (SocietyRole) criteria.uniqueResult();
        return societyRole;
    }

    /**
     * 分页查询土地类型信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 土地类型实体列表
     */
    @Override
    public List<SocietyRole> querySocietyRolePage(QueryEntity queryEntity, PageModel<SocietyRole> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取土地类型总数
     *
     * @return 土地类型总数
     */
    @Override
    public long queryAllSocietyRoleCount() {
        return getCount();
    }

    /**
     * 更新土地类型信息
     *
     * @param societyRole 土地类型实体
     */
    @Override
    public void updateSocietyRole(SocietyRole societyRole) {
        saveOrUpdate(societyRole);
    }
}
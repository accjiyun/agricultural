package cn.accjiyun.dao.impl.society;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.society.SocietyUserDao;
import cn.accjiyun.entity.society.SocietyUser;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("societyUserDao")
public class SocietyUserDaoImpl extends DaoSupport<SocietyUser> implements SocietyUserDao {
    /**
     * 创建社员
     *
     * @param user 社员实体
     * @return 社员ID
     */
    @Override
    public int createSocietyUser(SocietyUser user) {
        save(user);
        return user.getUserId();
    }

    /**
     * 通过社员ID数组删除社员
     *
     * @param userIds 社员ID数组
     */
    @Override
    public void deleteSocietyUser(int[] userIds) {
        for (int i = 0; i < userIds.length; i++) {
            delete(userIds[i]);
        }
    }

    /**
     * 通过社员ID查询社员信息
     *
     * @param userId 庭成员ID
     * @return 社员实体
     */
    @Override
    public SocietyUser querySocietyUserById(int userId) {
        return get(userId);
    }

    /**
     * 分页查询社员信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 社员实体列表
     */
    @Override
    public List<SocietyUser> querySocietyUserPage(QueryEntity queryEntity, PageModel<SocietyUser> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取社员总数
     *
     * @return 社员总数
     */
    @Override
    public long queryAllSocietyUserCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找社员信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 社员实体列表
     */
    @Override
    public List<SocietyUser> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 验证社员身份证是否有效
     *
     * @param identityCard
     * @return 社员身份证不存在则有效
     */
    @Override
    public boolean validateIDCard(String identityCard) {
        String hql = "select count(*) from SocietyUser where identityCard=?";
        Object[] params = {identityCard};
        long count = (Long) uniqueResult(hql, params);
        return count == 0L ? true : false;
    }

    /**
     * 更新社员信息
     *
     * @param user 社员实体
     */
    @Override
    public void updateSocietyUser(SocietyUser user) {
        saveOrUpdate(user);
    }
}

package cn.accjiyun.dao.impl.sale;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.sale.OrdersDao;
import cn.accjiyun.entity.sale.Orders;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("ordersDao")
public class OrdersDaoImpl extends DaoSupport<Orders> implements OrdersDao {
    /**
     * 创建订单
     *
     * @param orders 订单实体
     * @return 订单ID
     */
    @Override
    public int createOrders(Orders orders) {
        save(orders);
        return orders.getOrdersId();
    }

    /**
     * 通过订单ID数组删除订单
     *
     * @param ordersIds 订单ID数组
     */
    @Override
    public void deleteOrders(int[] ordersIds) {
        for (int i = 0; i < ordersIds.length; i++) {
            delete(ordersIds[i]);
        }
    }

    /**
     * 通过订单ID查询订单信息
     *
     * @param ordersId 庭成员ID
     * @return 订单实体
     */
    @Override
    public Orders queryOrdersById(int ordersId) {
        return get(ordersId);
    }

    /**
     * 分页查询订单信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 订单实体列表
     */
    @Override
    public List<Orders> queryOrdersPage(QueryEntity queryEntity, PageModel<Orders> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取订单总数
     *
     * @return 订单总数
     */
    @Override
    public long queryAllOrdersCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找订单信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 订单实体列表
     */
    @Override
    public List<Orders> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 更新订单信息
     *
     * @param orders 订单实体
     */
    @Override
    public void updateOrders(Orders orders) {
        saveOrUpdate(orders);
    }
}

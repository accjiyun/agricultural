package cn.accjiyun.dao.impl.system;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.system.PositionDao;
import cn.accjiyun.entity.system.Position;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("positionDao")
public class PositionDaoImpl extends DaoSupport<Position> implements PositionDao {

    /**
     * 创建职位
     *
     * @param position 职位实体
     * @return 职位ID
     */
    @Override
    public byte createPosition(Position position) {
        save(position);
        return position.getPositionId();
    }

    /**
     * 通过职位ID数组删除职位
     *
     * @param positionIds 职位ID数组
     */
    @Override
    public void deletePosition(byte[] positionIds) {
        for (int i = 0; i < positionIds.length; i++) {
            delete(positionIds[i]);
        }
    }

    /**
     * 通过职位ID查询职位信息
     *
     * @param positionId 庭成员ID
     * @return 职位实体
     */
    @Override
    public Position queryPositionById(byte positionId) {
        return get(positionId);
    }

    /**
     * 通过名称查询职位实体
     *
     * @param positionName 职位名
     * @return 对应职位实体
     */
    @Override
    public Position queryPositionIdByName(String positionName) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("positionName", positionName));
        Position position = (Position) criteria.uniqueResult();
        return position;
    }

    /**
     * 分页查询职位信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 职位实体列表
     */
    @Override
    public List<Position> queryPositionPage(QueryEntity queryEntity, PageModel<Position> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取职位总数
     *
     * @return 职位总数
     */
    @Override
    public long queryAllPositionCount() {
        return getCount();
    }

    /**
     * 更新职位信息
     *
     * @param position 职位实体
     */
    @Override
    public void updatePosition(Position position) {
        saveOrUpdate(position);
    }
}

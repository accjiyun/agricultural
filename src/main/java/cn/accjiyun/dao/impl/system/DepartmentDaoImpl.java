package cn.accjiyun.dao.impl.system;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.system.DepartmentDao;
import cn.accjiyun.entity.system.Department;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("departmentDao")
public class DepartmentDaoImpl extends DaoSupport<Department> implements DepartmentDao {

    /**
     * 创建部门
     *
     * @param department 部门实体
     * @return 部门ID
     */
    @Override
    public byte createDepartment(Department department) {
        save(department);
        return department.getDepartmentId();
    }

    /**
     * 通过部门ID数组删除部门
     *
     * @param departmentIds 部门ID数组
     */
    @Override
    public void deleteDepartment(byte[] departmentIds) {
        for (int i = 0; i < departmentIds.length; i++) {
            delete(departmentIds[i]);
        }
    }

    /**
     * 通过部门ID查询部门信息
     *
     * @param departmentId 庭成员ID
     * @return 部门实体
     */
    @Override
    public Department queryDepartmentById(byte departmentId) {
        return get(departmentId);
    }

    /**
     * 通过名称查询部门实体
     *
     * @param departmentName 部门名
     * @return 对应部门实体
     */
    @Override
    public Department queryDepartmentIdByName(String departmentName) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("departmentName", departmentName));
        Department department = (Department) criteria.uniqueResult();
        return department;
    }

    /**
     * 分页查询部门信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 部门实体列表
     */
    @Override
    public List<Department> queryDepartmentPage(QueryEntity queryEntity, PageModel<Department> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取部门总数
     *
     * @return 部门总数
     */
    @Override
    public long queryAllDepartmentCount() {
        return getCount();
    }

    /**
     * 更新部门信息
     *
     * @param department 部门实体
     */
    @Override
    public void updateDepartment(Department department) {
        saveOrUpdate(department);
    }
}

package cn.accjiyun.dao.impl.system;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.system.SystemUserDao;
import cn.accjiyun.entity.system.SystemUser;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("systemUserDao")
public class SystemUserDaoImpl extends DaoSupport<SystemUser> implements SystemUserDao {
    /**
     * 创建系统用户
     *
     * @param user 系统用户实体
     * @return 系统用户ID
     */
    @Override
    public int createSystemUser(SystemUser user) {
        save(user);
        return user.getUserId();
    }

    /**
     * 通过系统用户ID数组删除系统用户
     *
     * @param userIds 系统用户ID数组
     */
    @Override
    public void deleteSystemUser(int[] userIds) {
        for (int i = 0; i < userIds.length; i++) {
            delete(userIds[i]);
        }
    }

    /**
     * 通过系统用户ID查询系统用户信息
     *
     * @param userId 庭成员ID
     * @return 系统用户实体
     */
    @Override
    public SystemUser querySystemUserById(int userId) {
        return get(userId);
    }

    /**
     * 分页查询系统用户信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 系统用户实体列表
     */
    @Override
    public List<SystemUser> querySystemUserPage(QueryEntity queryEntity, PageModel<SystemUser> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取系统用户总数
     *
     * @return 系统用户总数
     */
    @Override
    public long queryAllSystemUserCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找系统用户信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 系统用户实体列表
     */
    @Override
    public List<SystemUser> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 登录查询系统用户实体
     *
     * @param user 系统用户实例
     * @return 如果存在用户，返回完整实体，否则为空
     */
    @Override
    public SystemUser queryLoginUser(SystemUser user) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("loginName", user.getLoginName()))
                .add(Restrictions.eq("password", user.getPassword()));
        List<SystemUser> systemUserList = criteria.list();
        if (systemUserList.size() > 0) {
            return systemUserList.get(0);
        } else {
            return null;
        }
    }

    /**
     * Validate the LoginName if exist.
     *
     * @param userLoginName
     * @return 1 if the LoginName exists.
     */
    @Override
    public boolean validateLoginName(String userLoginName) {
        String hql = "select count(*) from SystemUser where loginName=?";
        Object[] params = {userLoginName};
        return uniqueResult(hql, params) == null ? true : false;
    }

    /**
     * 更新系统用户信息
     *
     * @param user 系统用户实体
     */
    @Override
    public void updateSystemUser(SystemUser user) {
        saveOrUpdate(user);
    }

    /**
     * 更新用户启用状态
     *
     * @param userId 用户ID
     * @param status 状态0:正常,1:冻结
     */
    @Override
    public void updateDisableOrStartUser(int userId, int status) {
        SystemUser user = querySystemUserById(userId);
        user.setDisabled((byte)(1 -  status));
    }

}

package cn.accjiyun.dao.impl.production;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.production.ProducerDao;
import cn.accjiyun.entity.production.Producer;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("producerDao")
public class ProducerDaoImpl extends DaoSupport<Producer> implements ProducerDao {
    /**
     * 创建生产
     *
     * @param producer 生产实体
     * @return 生产ID
     */
    @Override
    public int createProducer(Producer producer) {
        save(producer);
        return producer.getProducerId();
    }

    /**
     * 通过生产ID数组删除生产
     *
     * @param producerIds 生产ID数组
     */
    @Override
    public void deleteProducer(int[] producerIds) {
        for (int i = 0; i < producerIds.length; i++) {
            delete(producerIds[i]);
        }
    }

    /**
     * 通过生产ID查询生产信息
     *
     * @param producerId 庭成员ID
     * @return 生产实体
     */
    @Override
    public Producer queryProducerById(int producerId) {
        return get(producerId);
    }

    /**
     * 分页查询生产信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 生产实体列表
     */
    @Override
    public List<Producer> queryProducerPage(QueryEntity queryEntity, PageModel<Producer> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取生产总数
     *
     * @return 生产总数
     */
    @Override
    public long queryAllProducerCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找生产信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 生产实体列表
     */
    @Override
    public List createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 分组统计查询
     *
     * @param sum     统计字段名
     * @param groupBy 分组字段名
     * @return 查询结果
     */
    public List<Object[]> querySumGroupBy(String sum, String groupBy) {
        Criteria criteria = getCriteria();

        ProjectionList plist = Projections.projectionList();
        plist.add(Projections.rowCount());
        plist.add(Projections.groupProperty(groupBy));

        plist.add(Projections.sum(sum));
        criteria.setProjection(plist);

        return criteria.list();
    }

    /**
     * 更新生产信息
     *
     * @param producer 生产实体
     */
    @Override
    public void updateProducer(Producer producer) {
        saveOrUpdate(producer);
    }
}
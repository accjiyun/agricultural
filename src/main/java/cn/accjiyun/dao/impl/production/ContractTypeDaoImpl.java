package cn.accjiyun.dao.impl.production;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.production.ContractTypeDao;
import cn.accjiyun.entity.production.ContractType;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("contractTypeDao")
public class ContractTypeDaoImpl extends DaoSupport<ContractType> implements ContractTypeDao {

    /**
     * 创建合同类型
     *
     * @param contractType 合同类型实体
     * @return 合同类型ID
     */
    @Override
    public byte createContractType(ContractType contractType) {
        save(contractType);
        return contractType.getContractTypeId();
    }

    /**
     * 通过合同类型ID数组删除合同类型
     *
     * @param contractTypeIds 合同类型ID数组
     */
    @Override
    public void deleteContractType(byte[] contractTypeIds) {
        for (int i = 0; i < contractTypeIds.length; i++) {
            delete(contractTypeIds[i]);
        }
    }

    /**
     * 通过合同类型ID查询合同类型信息
     *
     * @param contractTypeId 庭成员ID
     * @return 合同类型实体
     */
    @Override
    public ContractType queryContractTypeById(byte contractTypeId) {
        return get(contractTypeId);
    }

    /**
     * 通过名称查询合同类型实体
     *
     * @param contractTypeName 合同类型名
     * @return 对应合同类型实体
     */
    @Override
    public ContractType queryContractTypeIdByName(String contractTypeName) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("contractTypeName", contractTypeName));
        ContractType contractType = (ContractType) criteria.uniqueResult();
        return contractType;
    }

    /**
     * 分页查询合同类型信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 合同类型实体列表
     */
    @Override
    public List<ContractType> queryContractTypePage(QueryEntity queryEntity, PageModel<ContractType> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取合同类型总数
     *
     * @return 合同类型总数
     */
    @Override
    public long queryAllContractTypeCount() {
        return getCount();
    }

    /**
     * 更新合同类型信息
     *
     * @param contractType 合同类型实体
     */
    @Override
    public void updateContractType(ContractType contractType) {
        saveOrUpdate(contractType);
    }
}


package cn.accjiyun.dao.impl.production;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.production.LandDao;
import cn.accjiyun.entity.production.Land;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("landDao")
public class LandDaoImpl extends DaoSupport<Land> implements LandDao {
    /**
     * 创建土地
     *
     * @param land 土地实体
     * @return 土地ID
     */
    @Override
    public int createLand(Land land) {
        save(land);
        return land.getLandId();
    }

    /**
     * 通过土地ID数组删除土地
     *
     * @param landIds 土地ID数组
     */
    @Override
    public void deleteLand(int[] landIds) {
        for (int i = 0; i < landIds.length; i++) {
            delete(landIds[i]);
        }
    }

    /**
     * 通过土地ID查询土地信息
     *
     * @param landId 庭成员ID
     * @return 土地实体
     */
    @Override
    public Land queryLandById(int landId) {
        return get(landId);
    }

    /**
     * 分页查询土地信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 土地实体列表
     */
    @Override
    public List<Land> queryLandPage(QueryEntity queryEntity, PageModel<Land> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取土地总数
     *
     * @return 土地总数
     */
    @Override
    public long queryAllLandCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找土地信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 土地实体列表
     */
    @Override
    public List<Land> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 更新土地信息
     *
     * @param land 土地实体
     */
    @Override
    public void updateLand(Land land) {
        saveOrUpdate(land);
    }
}

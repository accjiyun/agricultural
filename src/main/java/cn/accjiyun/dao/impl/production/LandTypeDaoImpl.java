package cn.accjiyun.dao.impl.production;

import cn.accjiyun.common.dao.DaoSupport;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.dao.production.LandTypeDao;
import cn.accjiyun.entity.production.LandType;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
@Repository("landTypeDao")
public class LandTypeDaoImpl extends DaoSupport<LandType> implements LandTypeDao {

    /**
     * 创建土地类型
     *
     * @param landType 土地类型实体
     * @return 土地类型ID
     */
    @Override
    public byte createLandType(LandType landType) {
        save(landType);
        return landType.getLandTypeId();
    }

    /**
     * 通过土地类型ID数组删除土地类型
     *
     * @param landTypeIds 土地类型ID数组
     */
    @Override
    public void deleteLandType(byte[] landTypeIds) {
        for (int i = 0; i < landTypeIds.length; i++) {
            delete(landTypeIds[i]);
        }
    }

    /**
     * 通过土地类型ID查询土地类型信息
     *
     * @param landTypeId 土地类型ID
     * @return 土地类型实体
     */
    @Override
    public LandType queryLandTypeById(byte landTypeId) {
        return get(landTypeId);
    }

    /**
     * 通过名称查询土地类型实体
     *
     * @param landTypeName 土地类型名
     * @return 对应土地类型实体
     */
    @Override
    public LandType queryLandTypeIdByName(String landTypeName) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("landTypeName", landTypeName));
        LandType contractType = (LandType) criteria.uniqueResult();
        return contractType;
    }

    /**
     * 分页查询土地类型信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 土地类型实体列表
     */
    @Override
    public List<LandType> queryLandTypePage(QueryEntity queryEntity, PageModel<LandType> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取土地类型总数
     *
     * @return 土地类型总数
     */
    @Override
    public long queryAllLandTypeCount() {
        return getCount();
    }

    /**
     * 更新土地类型信息
     *
     * @param landType 土地类型实体
     */
    @Override
    public void updateLandType(LandType landType) {
        saveOrUpdate(landType);
    }
}

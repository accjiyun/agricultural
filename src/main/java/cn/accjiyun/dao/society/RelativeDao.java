package cn.accjiyun.dao.society;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.society.Relative;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface RelativeDao extends BaseDao<Relative> {

    /**
     * 创建家庭成员
     * @param relative 家庭成员实体
     * @return 家庭成员ID
     */
    public int createRelative(Relative relative);

    /**
     * 通过家庭成员ID数组删除家庭成员
     * @param relativeIds 家庭成员ID数组
     */
    public void deleteRelative(int[] relativeIds);

    /**
     * 通过家庭成员ID查询家庭成员信息
     * @param relativeId 庭成员ID
     * @return 家庭成员实体
     */
    public Relative queryRelativeById(int relativeId);

    /**
     * 分页查询家庭成员信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 家庭成员实体列表
     */
    public List<Relative> queryRelativePage(QueryEntity queryEntity, PageModel<Relative> model);

    /**
     * 获取家庭成员总数
     * @return 家庭成员总数
     */
    public long queryAllRelativeCount();

    /**
     * 利用hql语句查找家庭成员信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 家庭成员实体列表
     */
    public List<Relative> createQuery(final String hql, final Object[] queryParams);

    /**
     * 更新家庭成员信息
     * @param relative 家庭成员实体
     */
    public void updateRelative(Relative relative);
}

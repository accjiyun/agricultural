package cn.accjiyun.dao.society;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.society.SocietyRole;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface SocietyRoleDao extends BaseDao<SocietyRole> {

    /**
     * 创建社员角色
     * @param societyRole 社员角色实体
     * @return 社员角色ID
     */
    public byte createSocietyRole(SocietyRole societyRole);

    /**
     * 通过社员角色ID数组删除社员角色
     * @param societyRoleIds 社员角色ID数组
     */
    public void deleteSocietyRole(byte[] societyRoleIds);

    /**
     * 通过社员角色ID查询社员角色信息
     * @param societyRoleId 庭成员ID
     * @return 社员角色实体
     */
    public SocietyRole querySocietyRoleById(byte societyRoleId);

    /**
     * 通过名称查询社员角色实体
     * @param roleName 社员角色名
     * @return 对应社员角色实体
     */
    public SocietyRole querySocietyRoleIdByName(String roleName);

    /**
     * 分页查询社员角色信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 社员角色实体列表
     */
    public List<SocietyRole> querySocietyRolePage(QueryEntity queryEntity, PageModel<SocietyRole> model);

    /**
     * 获取社员角色总数
     * @return 社员角色总数
     */
    public long queryAllSocietyRoleCount();

    /**
     * 更新社员角色信息
     * @param societyRole 社员角色实体
     */
    public void updateSocietyRole(SocietyRole societyRole);
}

package cn.accjiyun.dao.society;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.society.SocietyUser;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface SocietyUserDao extends BaseDao<SocietyUser> {

    /**
     * 创建社员
     * @param user 社员实体
     * @return 社员ID
     */
    public int createSocietyUser(SocietyUser user);

    /**
     * 通过社员ID数组删除社员
     * @param userIds 社员ID数组
     */
    public void deleteSocietyUser(int[] userIds);

    /**
     * 通过社员ID查询社员信息
     * @param userId 庭成员ID
     * @return 社员实体
     */
    public SocietyUser querySocietyUserById(int userId);

    /**
     * 分页查询社员信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 社员实体列表
     */
    public List<SocietyUser> querySocietyUserPage(QueryEntity queryEntity, PageModel<SocietyUser> model);

    /**
     * 更新社员信息
     * @param user 社员实体
     */
    public void updateSocietyUser(SocietyUser user);

    /**
     * 获取社员总数
     * @return 社员总数
     */
    public long queryAllSocietyUserCount();

    /**
     * 利用hql语句查找社员信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 社员实体列表
     */
    public List<SocietyUser> createQuery(final String hql, final Object[] queryParams);

    /**
     * 验证社员身份证是否有效
     *
     * @param identityCard
     * @return 社员身份证不存在则有效
     */
    public boolean validateIDCard(String identityCard);
}
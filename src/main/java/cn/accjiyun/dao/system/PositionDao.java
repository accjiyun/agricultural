package cn.accjiyun.dao.system;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.system.Position;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface PositionDao extends BaseDao<Position> {

    /**
     * 创建职位
     * @param position 职位实体
     * @return 职位ID
     */
    public byte createPosition(Position position);

    /**
     * 通过职位ID数组删除职位
     * @param positionIds 职位ID数组
     */
    public void deletePosition(byte[] positionIds);

    /**
     * 通过职位ID查询职位信息
     * @param positionId 庭成员ID
     * @return 职位实体
     */
    public Position queryPositionById(byte positionId);

    /**
     * 通过名称查询职位实体
     * @param positionName 职位名
     * @return 对应职位实体
     */
    public Position queryPositionIdByName(String positionName);

    /**
     * 分页查询职位信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 职位实体列表
     */
    public List<Position> queryPositionPage(QueryEntity queryEntity, PageModel<Position> model);

    /**
     * 获取职位总数
     * @return 职位总数
     */
    public long queryAllPositionCount();

    /**
     * 更新职位信息
     * @param position 职位实体
     */
    public void updatePosition(Position position);
}

package cn.accjiyun.dao.system;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.system.Department;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface DepartmentDao extends BaseDao<Department> {

    /**
     * 创建部门
     * @param department 部门实体
     * @return 部门ID
     */
    public byte createDepartment(Department department);

    /**
     * 通过部门ID数组删除部门
     * @param departmentIds 部门ID数组
     */
    public void deleteDepartment(byte[] departmentIds);

    /**
     * 通过部门ID查询部门信息
     * @param departmentId 庭成员ID
     * @return 部门实体
     */
    public Department queryDepartmentById(byte departmentId);

    /**
     * 通过名称查询部门实体
     * @param departmentName 部门名
     * @return 对应部门实体
     */
    public Department queryDepartmentIdByName(String departmentName);

    /**
     * 分页查询部门信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 部门实体列表
     */
    public List<Department> queryDepartmentPage(QueryEntity queryEntity, PageModel<Department> model);

    /**
     * 获取部门总数
     * @return 部门总数
     */
    public long queryAllDepartmentCount();

    /**
     * 更新部门信息
     * @param department 部门实体
     */
    public void updateDepartment(Department department);
}

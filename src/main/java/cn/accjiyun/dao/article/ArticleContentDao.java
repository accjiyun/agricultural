package cn.accjiyun.dao.article;


import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.entity.article.ArticleContent;

/**
 * Created by jiyun on 2016/11/17.
 */
public interface ArticleContentDao extends BaseDao<ArticleContent> {
    /**
     * 添加文章内容
     *
     * @param content 文章内容实体
     */
    public void addArticleContent(ArticleContent content);

    /**
     * 删除文章内容
     *
     * @param articleIds 文章ID
     */
    public void deleteArticleContentByArticleIds(int[] articleIds);

    /**
     * 通过文章ID查询文章内容
     *
     * @param articleId 文章ID
     * @return String类型文章内容
     */
    public String queryArticleContentByArticleId(int articleId);

    /**
     * 修改文章内容
     *
     * @param content
     */
    public void updateArticleContent(ArticleContent content);
}

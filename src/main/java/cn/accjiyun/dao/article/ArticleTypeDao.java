package cn.accjiyun.dao.article;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.article.ArticleType;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface ArticleTypeDao extends BaseDao<ArticleType> {

    /**
     * 创建文章类型
     * @param articleType 文章类型实体
     * @return 文章类型ID
     */
    public byte createArticleType(ArticleType articleType);

    /**
     * 通过文章类型ID数组删除文章类型
     * @param articleTypeIds 文章类型ID数组
     */
    public void deleteArticleType(byte[] articleTypeIds);

    /**
     * 通过文章类型ID查询文章类型信息
     * @param articleTypeId 庭成员ID
     * @return 文章类型实体
     */
    public ArticleType queryArticleTypeById(byte articleTypeId);

    /**
     * 通过名称查询文章类型信息
     *
     * @param typeName 文章类型名称
     * @return 文章类型实体
     */
    public ArticleType queryArticleTypeIdByName(String typeName);

    /**
     * 分页查询文章类型信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 文章类型实体列表
     */
    public List<ArticleType> queryArticleTypePage(QueryEntity queryEntity, PageModel<ArticleType> model);

    /**
     * 获取文章类型总数
     * @return 文章类型总数
     */
    public long queryAllArticleTypeCount();

    /**
     * 更新文章类型信息
     * @param articleType 文章类型实体
     */
    public void updateArticleType(ArticleType articleType);
}

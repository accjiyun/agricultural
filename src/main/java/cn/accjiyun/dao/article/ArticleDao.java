package cn.accjiyun.dao.article;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.article.Article;

import java.util.List;
import java.util.Map;

/**
 * 文章DAO层接口
 * Created by jiyun on 2016/11/13.
 */
public interface ArticleDao extends BaseDao<Article> {
    /**
     * 创建文章
     *
     * @param article 文章实体
     * @return 返回文章ID
     */
    public int createArticle(Article article);

    /**
     * 删除文章
     *
     * @param articleIds 文章ID
     */
    public void deleteArticleByIds(int[] articleIds);

    /**
     * 通过文章ID查询文章信息
     *
     * @param articleId 文章ID
     * @return Article文章实体信息
     */
    public Article queryArticleById(int articleId);

    /**
     * 分页查询文章列表
     *
     * @param query     查询条件
     * @param pageModel 分页条件
     * @return List<Article>文章列表
     */
    public List<Article> queryArticlePage(QueryEntity query, PageModel<Article> pageModel);

    /**
     * 获取所有文章总记录数
     *
     * @return 总记录数
     */
    public int queryAllArticleCount();

    /**
     * 利用hql语句查找信息
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return
     */
    public List<Article> createQuery(final String hql, final Object[] queryParams);

    /**
     * 修改文章信息
     *
     * @param article 文章实体
     */
    public void updateArticle(Article article);

    /**
     * 修改累加文章点击量
     *
     * @param map
     */
    public void updateArticleNum(Map<String, String> map);

}

package cn.accjiyun.dao.production;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.production.ContractType;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface ContractTypeDao extends BaseDao<ContractType> {

    /**
     * 创建合同类型
     * @param contractType 合同类型实体
     * @return 合同类型ID
     */
    public byte createContractType(ContractType contractType);

    /**
     * 通过合同类型ID数组删除合同类型
     * @param contractTypeIds 合同类型ID数组
     */
    public void deleteContractType(byte[] contractTypeIds);

    /**
     * 通过合同类型ID查询合同类型信息
     * @param contractTypeId 庭成员ID
     * @return 合同类型实体
     */
    public ContractType queryContractTypeById(byte contractTypeId);

    /**
     * 通过名称查询合同类型实体
     * @param contractTypeName 合同类型名
     * @return 对应合同类型实体
     */
    public ContractType queryContractTypeIdByName(String contractTypeName);

    /**
     * 分页查询合同类型信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 合同类型实体列表
     */
    public List<ContractType> queryContractTypePage(QueryEntity queryEntity, PageModel<ContractType> model);

    /**
     * 获取合同类型总数
     * @return 合同类型总数
     */
    public long queryAllContractTypeCount();

    /**
     * 更新合同类型信息
     * @param contractType 合同类型实体
     */
    public void updateContractType(ContractType contractType);
}


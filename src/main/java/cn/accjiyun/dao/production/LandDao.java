package cn.accjiyun.dao.production;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.production.Land;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface LandDao extends BaseDao<Land> {

    /**
     * 创建土地
     * @param land 土地实体
     * @return 土地ID
     */
    public int createLand(Land land);

    /**
     * 通过土地ID数组删除土地
     * @param landIds 土地ID数组
     */
    public void deleteLand(int[] landIds);

    /**
     * 通过土地ID查询土地信息
     * @param landId 庭成员ID
     * @return 土地实体
     */
    public Land queryLandById(int landId);

    /**
     * 分页查询土地信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 土地实体列表
     */
    public List<Land> queryLandPage(QueryEntity queryEntity, PageModel<Land> model);

    /**
     * 更新土地信息
     * @param land 土地实体
     */
    public void updateLand(Land land);

    /**
     * 获取土地总数
     * @return 土地总数
     */
    public long queryAllLandCount();

    /**
     * 利用hql语句查找土地信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 土地实体列表
     */
    public List<Land> createQuery(final String hql, final Object[] queryParams);
}

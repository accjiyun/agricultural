package cn.accjiyun.dao.production;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.production.LandType;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface LandTypeDao extends BaseDao<LandType> {

    /**
     * 创建土地类型
     * @param landType 土地类型实体
     * @return 土地类型ID
     */
    public byte createLandType(LandType landType);

    /**
     * 通过土地类型ID数组删除土地类型
     * @param landTypeIds 土地类型ID数组
     */
    public void deleteLandType(byte[] landTypeIds);

    /**
     * 通过土地类型ID查询土地类型信息
     * @param landTypeId 土地类型ID
     * @return 土地类型实体
     */
    public LandType queryLandTypeById(byte landTypeId);

    /**
     * 通过名称查询土地类型实体
     * @param landTypeName 土地类型名
     * @return 对应土地类型实体
     */
    public LandType queryLandTypeIdByName(String landTypeName);

    /**
     * 分页查询土地类型信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 土地类型实体列表
     */
    public List<LandType> queryLandTypePage(QueryEntity queryEntity, PageModel<LandType> model);

    /**
     * 获取土地类型总数
     * @return 土地类型总数
     */
    public long queryAllLandTypeCount();

    /**
     * 更新土地类型信息
     * @param landType 土地类型实体
     */
    public void updateLandType(LandType landType);
}

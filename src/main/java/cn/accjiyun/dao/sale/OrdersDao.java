package cn.accjiyun.dao.sale;

import cn.accjiyun.common.dao.BaseDao;
import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.sale.Orders;

import java.util.List;

/**
 * Created by jiyun on 2017/4/30.
 */
public interface OrdersDao extends BaseDao<Orders> {

    /**
     * 创建订单
     * @param orders 订单实体
     * @return 订单ID
     */
    public int createOrders(Orders orders);

    /**
     * 通过订单ID数组删除订单
     * @param ordersIds 订单ID数组
     */
    public void deleteOrders(int[] ordersIds);

    /**
     * 通过订单ID查询订单信息
     * @param ordersId 庭成员ID
     * @return 订单实体
     */
    public Orders queryOrdersById(int ordersId);

    /**
     * 分页查询订单信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 订单实体列表
     */
    public List<Orders> queryOrdersPage(QueryEntity queryEntity, PageModel<Orders> model);

    /**
     * 更新订单信息
     * @param orders 订单实体
     */
    public void updateOrders(Orders orders);

    /**
     * 获取订单总数
     * @return 订单总数
     */
    public long queryAllOrdersCount();

    /**
     * 利用hql语句查找订单信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 订单实体列表
     */
    public List<Orders> createQuery(final String hql, final Object[] queryParams);
}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html"
        data-params='{"url":"/admin/article/getArticleList?pageNo=1","pageid":"#page","dataName":"articleData"}'>
    <table id="example" class="layui-table lay-even" data-name="articleData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="articleId" lay-filter="check" lay-skin="primary"></th>
            <th>缩略图</th>
            <th>文章标题
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="title" data-asc="true"><i
                            class="iconfont">&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="title"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>文章类型</th>
            <th>发布时间
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="publicTime" data-asc="true"><i class="iconfont">&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="publicTime"><i
                            class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>审核</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="articleId" value="{{ item.articleId}}" lay-skin="primary"></td>
            <td>
                <div class="img"><img
                        src='{{#if (item.imageUrl==null||item.imageUrl.length==0){}} /static/web/images/logo.png {{#} }} {{#if (item.imageUrl!=null){}}{{item.imageUrl}} {{#} }}'
                        alt="暂无" style=" max-width: 200px;max-height: 70px;" class="img-thumbnail">
                </div>
            </td>
            <td>{{ item.title}}</td>
            <td>{{ item.articleType }}</td>
            <td>{{ item.publicTime}}</td>
            <td><input type="checkbox" name="status" lay-skin="switch" lay-text="发布|禁用" {{#if
                       (item.status){}}checked="checked" {{# } }} lay-filter="ajax"
                       data-params='{"url":"/admin/article/updateStatus","data":"articleId={{ item.articleId}}"}'></td>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
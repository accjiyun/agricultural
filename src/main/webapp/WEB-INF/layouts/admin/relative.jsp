<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html"
        data-params='{"url":"/admin/society/getRelativeList?pageNo=1","pageid":"#page","dataName":"relativeData"}'>
    <table id="example" class="layui-table lay-even" data-name="relativeData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="relativeId" lay-filter="check" lay-skin="primary"></th>
            <th>姓名</th>
            <th>性别</th>
            <th>户主</th>
            <th>与户主关系</th>
            <th>民族</th>
            <th>手机</th>
            <th>邮箱</th>
            <th>地址</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="relativeId" value="{{ item.relativeId}}" lay-skin="primary"></td>
            <td>{{ item.relativeName}}</td>
            <td>{{ item.sex }}</td>
            <td>{{ item.username }}</td>
            <td>{{ item.relationship }}</td>
            <td>{{ item.nation}}</td>
            <td>{{ item.mobile}}</td>
            <td>{{ item.email}}</td>
            <td>{{ item.address}}</td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.username}}","key":"relativeId={{ item.relativeId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/society/delete?userId={{item.userId}}","loading":"true"}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
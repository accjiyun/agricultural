<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/society/getUserList?pageNo=1","pageid":"#page","dataName":"societyUserData"}'>
    <table id="example" class="layui-table lay-even" data-name="societyUserData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="userId" lay-filter="check" lay-skin="primary"></th>
            <th>姓名</th>
            <th>社员性质</th>
            <th>性别</th>
            <th>入社日期
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="joinDate" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="joinDate"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>截止日期
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="insureEndDate" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="insureEndDate"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="userId" value="{{ item.userId}}" lay-skin="primary"></td>
            <td>{{ item.username}}</td>
            <td>{{ item.roleName}}</td>
            <td>{{ item.sex }}</td>
            <td>{{ item.joinDate}}</td>
            <td>{{ item.insureEndDate}}<td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.username}}","key":"userId={{ item.userId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/society/delete?userId={{item.userId}}","loading":"true"}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
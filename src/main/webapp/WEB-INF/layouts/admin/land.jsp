<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html"
        data-params='{"url":"/admin/production/getLandList?pageNo=1","pageid":"#page","dataName":"landData"}'>
    <table id="example" class="layui-table lay-even" data-name="landData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="landId" lay-filter="check" lay-skin="primary"></th>
            <th>编号</th>
            <th>地点名称</th>
            <th>面积</th>
            <th>土地类型</th>
            <th>总价</th>
            <th>合同编号</th>
            <th>合同类型</th>
            <th>签订日期</th>
            <th>期限</th>
            <th>实际总价</th>
            <th>转让方</th>
            <th>受让方</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="landId" value="{{ item.landId}}" lay-skin="primary"></td>
            <td>{{ item.landId}}</td>
            <td>{{ item.landName}}</td>
            <td>{{ item.area }}</td>
            <td>{{ item.landTypeName}}</td>
            <td>{{ item.totalPrice}}</td>
            <td>{{ item.contractId}}</td>
            <td>{{ item.contractTypeName}}</td>
            <td>{{ item.insureStartDate}}</td>
            <td>{{ item.deadline}}</td>
            <td>{{ item.realTotalPrice}}</td>
            <td>{{ item.transferor}}</td>
            <td>{{ item.assignee}}</td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.landName}}","key":"landId={{ item.landId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/production/deleteLand?landId={{item.landId}}","loading":"true"}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
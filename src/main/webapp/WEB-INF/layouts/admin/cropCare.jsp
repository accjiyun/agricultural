<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/production/getCropCareList?pageNo=1","pageid":"#page","dataName":"cropCareData"}'>
    <table id="example" class="layui-table lay-even" data-name="cropCareData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="cropCareId" lay-filter="check" lay-skin="primary"></th>
            <th>产地</th>
            <th>批次
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="batch" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="batch"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>负责人姓名</th>
            <th>种苗</th>
            <th>数量
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="count" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="count"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>肥（饲）料</th>
            <th>药品</th>
            <th>护理时间
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="cropTime" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="cropTime"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="cropCareId" value="{{ item.cropCareId}}" lay-skin="primary"></td>
            <td>{{ item.landName}}</td>
            <td>{{ item.batch}}</td>
            <td>{{ item.principalName}}</td>
            <td>{{ item.germchit }}</td>
            <td>{{ item.count}}</td>
            <td>{{ item.fertilizer}}</td>
            <td>{{ item.drug}}</td>
            <td>{{ item.cropTime}}</td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.landName}}","key":"cropCareId={{ item.cropCareId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/production/delete?cropCareId={{item.cropCareId}}","loading":"true"}}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/getLoginLogList?pageNo=1","pageid":"#page","dataName":"loginLogData"}'>
    <table id="example" class="layui-table lay-even" data-name="loginLogData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th>编号</th>
            <th>用户名
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="loginName" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="loginName"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>登录时间
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="loginTime" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="loginTime"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>IP地址</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr>
            <td>{{ item.loginId}}</td>
            <td>{{ item.loginName}}</td>
            <td>{{ item.loginTime}}</td>
            <td>{{ item.ipAddress}}</td>
        </tr>
        {{# }); }}
        </tbody>

    </table>
</script>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/sale/getCustomerList?pageNo=1","pageid":"#page","dataName":"customerData"}'>
    <table id="example" class="layui-table lay-even" data-name="customerData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="customerId" lay-filter="check" lay-skin="primary"></th>
            <th>客户姓名</th>
            <th>负责人姓名</th>
            <th>手机</th>
            <th>地址</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="customerId" value="{{ item.customerId}}" lay-skin="primary"></td>
            <td>{{ item.customerName}}</td>
            <td>{{ item.principalName}}</td>
            <td>{{ item.mobile}}</td>
            <td>{{ item.address }}</td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.customerName}}","key":"customerId={{ item.customerId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/sale/deleteCustomer?customerId={{item.customerId}}","loading":"true"}}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
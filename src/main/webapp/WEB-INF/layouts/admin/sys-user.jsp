<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html"
        data-params='{"url":"/admin/system/getUserList?pageNo=1","pageid":"#page","dataName":"systemUserData"}'>
    <table id="example" class="layui-table lay-even" data-name="systemUserData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="userId" lay-filter="check" lay-skin="primary"></th>
            <th>登录账号</th>
            <td>姓名</td>
            <th>部门</th>
            <th>职位</th>
            <th>状态</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="userId" value="{{ item.userId}}" lay-skin="primary"></td>
            <td>{{ item.loginName }}</td>
            <td>{{ item.username }}</td>
            <td>{{ item.departmentName }}</td>
            <td>{{ item.positionName }}</td>
            <td><input type="checkbox" name="disabled" lay-skin="switch" lay-text="启用|禁用" {{#if (item.disabled == 0){
                       }}checked="checked" {{# } }} lay-filter="ajax"
                       data-params='{"url":"/admin/system/updateStatus","data":"userId={{ item.userId}}"}'>
            </td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.username}}","key":"userId={{ item.userId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/system/delete?userId={{item.userId}}","loading":"true"}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/sale/getOrdersList?pageNo=1","pageid":"#page","dataName":"ordersData"}'>
    <table id="example" class="layui-table lay-even" data-name="ordersData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="customerId" lay-filter="check" lay-skin="primary"></th>
            <th>客户姓名</th>
            <th>收购品种</th>
            <th>收购量</th>
            <th>总价</th>
            <th>创建时间</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="ordersId" value="{{ item.ordersId}}" lay-skin="primary"></td>
            <td>{{ item.customerName}}</td>
            <td>{{ item.variety}}</td>
            <td>{{ item.count}}</td>
            <td>{{ item.realPrice }}</td>
            <td>{{ item.createTime }}</td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.customerName}}","key":"ordersId={{ item.ordersId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/sale/deleteOrders?ordersId={{item.ordersId}}","loading":"true"}}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
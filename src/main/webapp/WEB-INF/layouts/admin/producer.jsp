<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/production/getProducerList?pageNo=1","pageid":"#page","dataName":"producerData"}'>
    <table id="example" class="layui-table lay-even" data-name="producerData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="producerId" lay-filter="check" lay-skin="primary"></th>
            <th>产地名称</th>
            <th>使用面积
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="area" data-asc="true"><i class="iconfont" >&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="area"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>生产类型</th>
            <th>品种</th>
            <th>实际单价
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="realPrice" data-asc="true"><i class="iconfont">&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="realPrice"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>实际产量
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="realYield" data-asc="true"><i class="iconfont">&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="realYield"><i class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>总价
                <div class="order-box">
                    <a href="javascript:;" class="sort" data-filed="realTotalPrice" data-asc="true"><i class="iconfont">&#xe615;</i></a>
                    <a href="javascript:;" class="sort down" data-filed="realTotalPrice"><i
                            class="iconfont">&#xe647;</i></a>
                </div>
            </th>
            <th>负责人姓名</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="producerId" value="{{ item.producerId}}" lay-skin="primary"></td>
            <td>{{ item.producerName}}</td>
            <td>{{ item.area}}</td>
            <td>{{ item.producerType }}</td>
            <td>{{ item.varietyName}}</td>
            <td>{{ item.realPrice}}</td>
            <td>{{ item.realYield}}</td>
            <td>{{ item.realTotalPrice}}</td>
            <td>{{ item.principalName}}</td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "编辑 ：{{item.producerName}}","key":"producerId={{ item.producerId }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/production/deleteProducer?producerId={{item.producerId}}","loading":"true"}}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>
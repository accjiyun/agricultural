<!--
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
-->
<!--
<%@ include file="/base.jsp" %>
-->
<html>

<head>
    <!--Basic Page Needs -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="icon" type="image/png" href="/favicon.ico">
    <title>响应式农合社信息管理系统</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Metas -->
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Template CSS Files -->
    <!-- Twitter Bootstrs CSS -->
    <link rel="stylesheet" href="/static/web/css/bootstrap.min.css">
    <!-- Ionicons Fonts Css -->
    <link rel="stylesheet" href="/static/web/css/ionicons.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="/static/web/css/animate.css">
    <!-- Hero area slider css-->
    <link rel="stylesheet" href="/static/web/css/slider.css">
    <!-- owl craousel css -->
    <link rel="stylesheet" href="/static/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/static/web/css/owl.theme.css">
    <link rel="stylesheet" href="/static/web/css/jquery.fancybox.css">
    <!-- template main css file -->
    <link rel="stylesheet" href="/static/web/css/main.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="/static/web/css/responsive.css">

    <!-- Template Javascript Files -->
    <!-- modernizr js -->
    <script src="/static/web/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- jquery -->
    <script src="/static/web/js/jquery.min.js"></script>
    <!-- owl carouserl js -->
    <script src="/static/web/js/owl.carousel.min.js"></script>
    <!-- bootstrap js -->

    <script src="/static/web/js/bootstrap.min.js"></script>
    <!-- wow js -->
    <script src="/static/web/js/wow.min.js"></script>
    <!-- slider js -->
    <script src="/static/web/js/slider.js"></script>
    <script src="/static/web/js/jquery.fancybox.js"></script>
    <!-- template main js -->
    <script src="/static/web/js/main.js"></script>
</head>

<body>

<header id="top-bar" class="navbar-fixed-top animated-header">
    <div class="container">
        <div class="navbar-header">
            <!-- responsive nav button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- /responsive nav button -->

            <!-- logo -->
            <div class="navbar-brand">
                <a href="/index">
                    <img src="/static/web/images/logo.png" alt="" style="height: 58px;">
                </a>
            </div>
            <!-- /logo -->
        </div>
        <!-- main menu -->
        <nav class="collapse navbar-collapse navbar-right" role="navigation">
            <div class="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/index">首页</a>
                    </li>
                    <li>
                        <a href="/article/1">新闻速递</a>
                    </li>
                    <li>
                        <a href="/article/2">通知公告</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- /main nav -->
    </div>
</header>
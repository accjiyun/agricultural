<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/base.jsp" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>响应式农合社信息管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=no">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <!-- load css -->
    <link rel="stylesheet" type="text/css" href="/static/admin/css/bootstrap.min.css?v=v3.3.7" media="all">
    <link rel="stylesheet" type="text/css" href="/static/admin/css/font/iconfont.css?v=1.0.0" media="all">
    <link rel="stylesheet" type="text/css" href="/static/admin/css/layui.css?v=1.0.9" media="all">
    <link rel="stylesheet" type="text/css" href="/static/admin/css/jqadmin.css?v=1.3.4" media="all">
</head>

<body>
<ul class='right-click-menu'>
    <li><a href='javascript:;' data-event='fresh'>刷新</a></li>
    <li><a href='javascript:;' data-event='close'>关闭</a></li>
    <li><a href='javascript:;' data-event='other'>关闭其它</a></li>
    <li><a href='javascript:;' data-event='all'>全部关闭</a></li>
</ul>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <!-- logo区域 -->
        <div class="jqadmin-logo-box">
            <a class="logo" href="/admin/main/main" title="响应式农合社信息管理系统"><img src="/static/admin/images/logo.png"
                                                                               style="width:183px;height:60px;" alt=""></a>
            <div class="menu-type" data-type="2"><i class="iconfont">&#xe61a;</i></div>
        </div>

        <!-- 主菜单区域 -->
        <div class="jqadmin-main-menu">
            <ul class="layui-nav clearfix" id="menu" lay-filter="main-menu">

            </ul>

        </div>
        s
        <!-- 头部右侧导航 -->
        <div class="header-right">
            <ul class="layui-nav jqadmin-header-item right-menu">
                <li class="layui-nav-item first">
                    <a href="javascript:;">
                        <cite>用户： ${societyUser.username}</cite>
                        <!--<span class="layui-nav-more"></span>-->
                    </a>
                    <dl class="layui-nav-child">
                        <dd class="tab-menu">
                            <a href="javascript:;" data-url="/admin/system/initUserUpdatePage"
                               data-title="修改密码"> <i class="iconfont " data-icon='&#xe672;'>
                                &#xe672; </i><span>修改密码</span></a>
                        </dd>
                        <dd>
                            <a href="/admin/outLogin"><i class="iconfont ">&#xe64b; </i>退出</a>
                        </dd>
                    </dl>
                </li>
            </ul>
            <button title="刷新" class="layui-btn layui-btn-normal fresh-btn"><i class="iconfont">&#xe62e; </i></button>
        </div>
    </div>

    <!-- 左侧导航-->
    <div class="layui-side layui-bg-black jqamdin-left-bar">
        <div class="layui-side-scroll">
            <!--子菜单项-->
            <p class="jqadmin-home tab-menu">
                <a href="javascript:;" data-url="/admin/main/welcome" data-title="后台首页">
                    <i class="iconfont" data-icon='&#xe600;'>&#xe600;</i>
                    <span>后台首页</span>
                </a>
            </p>
            <div id="submenu"></div>
        </div>
    </div>

    <!-- 左侧侧边导航结束 -->
    <!-- 右侧主体内容 -->
    <div class="layui-body jqadmin-body" style="left: 200px;margin-bottom: -50px">

        <div class="layui-tab layui-tab-card jqadmin-tab-box" id="jqadmin-tab" lay-filter="tabmenu" lay-notAuto="true">
            <ul class="layui-tab-title">
                <li class="layui-this" id="admin-home" lay-id="0"><i class="iconfont">&#xe600;</i><em>后台首页</em></li>
            </ul>
            <div class="menu-btn" title="显示左则菜单">
                <i class="iconfont">&#xe616;</i>
            </div>

            <div class="tab-move-btn">
                <span>更多<i class="iconfont">&#xe604;</i></span>
            </div>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <iframe class="jqadmin-iframe" data-id='0' src="/admin/main/welcome"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<%@include file="/WEB-INF/layouts/admin/admin-menu.jsp" %>
<script type="text/javascript" src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use('index');
</script>
<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?9e99ae60a18de5e3860d7bfffc86a85d";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

</html>
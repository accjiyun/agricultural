<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/base.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title>响应式农合社信息管理系统</title>
    <link rel="stylesheet" href="/static/admin/css/pintuer.css">
    <link rel="stylesheet" href="/static/admin/css/admin.css">
    <script src="/static/admin/js/jquery.js"></script>
    <script src="/static/admin/js/pintuer.js"></script>
    <script type="text/javascript">
        $(function() {
            if (document.getElementById("msg").innerText == "") {
                document.getElementById("alert").style.visibility="hidden";
            } else {
                document.getElementById("alert").style.visibility="visible";
            }
        });
    </script>
</head>
<body>
<div class="bg"></div>
<div class="container">
    <div class="line bouncein">
        <div class="xs6 xm4 xs3-move xm4-move">
            <div style="height:150px;"></div>
            <div class="media media-y margin-big-bottom"/>
            <form action="/admin/main/login" method="post" id="loginForm">
                <div class="panel loginbox">
                    <div class="text-center margin-big padding-big-top">
                        <h1>响应式农合社信息管理系统</h1>
                    </div>
                    <div class="panel-body" style="padding:30px; padding-bottom:10px; padding-top:10px;">
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="text" class="input input-big" name="systemUser.loginName"
                                       placeholder="登录账号" data-validate="required:请填写账号"/>
                                <span class="icon icon-user margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="password" class="input input-big" name="systemUser.password"
                                       placeholder="登录密码" data-validate="required:请填写密码"/>
                                <span class="icon icon-key margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field">
                                <input type="text" class="input input-big" name="randomCode" placeholder="填写右侧的验证码"
                                       data-validate="required:请填写右侧的验证码"/>
                                <span class="yzm-pic">
                                <img src="/ran/random" alt="验证码，点击图片更换" width="100" height="42" class="passcode"
                                     style="height:43px;cursor:pointer;"
                                     onclick="this.src='/ran/random?random='+Math.random();"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="padding:30px;">
                        <input type="submit" class="button button-block bg-main text-big input-big login" value="登录">
                        <div class="alert alert-red" id="alert">
                            <span class="close rotate-hover"></span><strong>注意：</strong>
                            <span id="msg">${message}</span>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">

    <div class="row">
        <div class="col-xs-12">
            <section class="panel">
                <div class="panel-body">
                    <span style="font-size: larger;color: #FF5722">${societyUser.username}，欢迎您！</span>
                    <hr/>
                    <table class="layui-table" lay-even lay-skin="nob">
                        <tbody>
                        <tr>
                            <td><i class="iconfont">&#xe636;</i>社会性质</td>
                            <td>${societyUser.societyRoleByRoleTypeId.roleName}</td>
                        </tr>
                        <tr>
                            <td><i class="iconfont">&#xe681;</i>性别</td>
                            <td>${societyUser.sex}</td>
                        </tr>
                        <tr>
                            <td><i class="iconfont">&#xe612;</i>民族</td>
                            <td>${societyUser.nation}</td>
                        </tr>
                        <tr>
                            <td><i class="iconfont">&#xe63c;</i> 邮箱</td>
                            <td>${societyUser.email}</td>
                        </tr>
                        <tr>
                            <td><i class="iconfont">&#xe620;</i>入社日期</td>
                            <td><fmt:formatDate value="${societyUser.joinDate}" pattern="yyyy年MM月dd日"/></td>
                        </tr>
                        </tbody>
                    </table>
                    <%--<i class="iconfont">&#xe636;</i>社会性质：${societyUser.societyRoleByRoleTypeId.roleName} <br />--%>
                    <%--<i class="iconfont">&#xe681;</i>性别：${societyUser.sex}<br/>--%>
                    <%--<i class="iconfont">&#xe612;</i>民族：${societyUser.nation}<br/>--%>
                    <%--<i class="iconfont">&#xe63c;</i> 邮箱：${societyUser.email} <br/>--%>
                    <%--<i class="iconfont">&#xe620;</i>入社日期：<fmt:formatDate value="${societyUser.joinDate}" pattern="yyyy年MM月dd日"/><br/>--%>
                </div>
            </section>
        </div>
    </div>

    <div class="row">

        <%--行政部门--%>
        <c:if test="${positionId==0 || positionId==10 || positionId==11}">
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-yellow"><i class="iconfont">&#xe674;</i>
                    </div>
                    <div class="value tab-menu" style="margin-top: 2em">
                        <a href="javascript:;" data-url="/admin/society/userAdd"
                           data-parent="true" data-title="添加社员"> <i class="iconfont " data-icon='&#xe674;'></i>
                            <span style="font-size: 14px;">添加社员</span>
                        </a>
                    </div>
                </section>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-blue"><i class="iconfont">&#xe672;</i>
                    </div>
                    <div class="value tab-menu">
                        <a href="javascript:;" data-url="/admin/society/userList"
                           data-parent="true" data-title="社员列表"><i class="iconfont " data-icon='&#xe672;'></i>
                            <h1>${societyUserCount}</h1>
                        </a>
                        <a href="javascript:;" data-url="/admin/society/userList" data-parent="true" data-title="社员列表">
                            <i class="iconfont" data-icon='&#xe672;'></i><span>社员列表</span>
                        </a>
                    </div>
                </section>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-commred"><i class="iconfont">&#xe608;</i>
                    </div>
                    <div class="value tab-menu" style="margin-top: 2em">
                        <a href="javascript:;" data-url="/admin/system/userAdd"
                           data-parent="true" data-title="添加系统用户"> <i class="iconfont " data-icon='&#xe608;'></i>
                            <span style="font-size: 14px;">添加系统用户</span>
                        </a>
                    </div>
                </section>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-yellow-green"><i class="iconfont">&#xe681;</i>
                    </div>
                    <div class="value tab-menu">
                        <a href="javascript:;" data-url="/admin/system/userList"
                           data-parent="true" data-title="系统用户列表"> <i class="iconfont " data-icon='&#xe681;'></i>
                            <h1>${systemUserCount}</h1>
                        </a>
                        <a href="javascript:;" data-url="/admin/system/userList" data-parent="true" data-title="系统用户列表">
                            <i class="iconfont" data-icon='&#xe672;'></i><span>系统用户列表</span>
                        </a>
                    </div>
                </section>
            </div>
        </c:if>

        <%--行政部门--%>
        <c:if test="${positionId==0 || positionId==10 || positionId==12}">
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-orange"><i class="iconfont">&#xe653;</i>
                    </div>
                    <div class="value tab-menu" style="margin-top: 2em">
                        <a href="javascript:;" data-url="/admin/article/initAddArticle"
                           data-parent="true" data-title="添加文章"> <i class="iconfont" data-icon='&#xe653;'></i>
                            <span style="font-size: 14px;">添加文章</span>
                        </a>
                    </div>
                </section>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe6bc;</i>
                    </div>
                    <div class="value tab-menu">
                        <a href="javascript:;" data-url="/admin/article/initArticleList"
                           data-parent="true" data-title="文章列表"> <i class="iconfont " data-icon='&#xe6bc;'></i>
                            <h1>${articleCount}</h1>
                        </a>
                        <a href="javascript:;" data-url="/admin/article/initArticleList"
                           data-parent="true" data-title="文章列表">
                            <i class="iconfont " data-icon='&#xe6bc;'></i><span>文章列表</span></a>
                    </div>
                </section>
            </div>
        </c:if>

        <%--土地管理--%>
        <c:if test="${positionId==0 || positionId==20 || positionId==21}">
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-orange"><i class="iconfont">&#xe606;</i>
                    </div>
                    <div class="value tab-menu" style="margin-top: 2em">
                        <a href="javascript:;" data-url="/admin/production/landAdd"
                           data-parent="true" data-title="土地信息录入"> <i class="iconfont" data-icon='&#xe606;'></i>
                            <span style="font-size: 14px;">土地信息录入</span>
                        </a>
                    </div>
                </section>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe61a;</i>
                    </div>
                    <div class="value tab-menu">
                        <a href="javascript:;" data-url="/admin/production/landList"
                           data-parent="true" data-title="土地资源列表"> <i class="iconfont " data-icon='&#xe61a;'></i>
                            <h1>${landCount}</h1>
                        </a>
                        <a href="javascript:;" data-url="/admin/production/landList"
                           data-parent="true" data-title="土地资源列表">
                            <i class="iconfont " data-icon='&#xe61a;'></i><span>土地资源列表</span></a>
                    </div>
                </section>
            </div>
        </c:if>

        <%--生产--%>
        <c:if test="${positionId==0 || positionId==20 || positionId==22}">
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-orange"><i class="iconfont">&#xe61c;</i>
                    </div>
                    <div class="value tab-menu" style="margin-top: 2em">
                        <a href="javascript:;" data-url="/admin/production/producerAdd"
                           data-parent="true" data-title="生产信息录入"> <i class="iconfont" data-icon='&#xe61c;'></i>
                            <span style="font-size: 14px;">生产信息录入</span>
                        </a>
                    </div>
                </section>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe622;</i>
                    </div>
                    <div class="value tab-menu">
                        <a href="javascript:;" data-url="/admin/production/producerList"
                           data-parent="true" data-title="生产信息列表"> <i class="iconfont " data-icon='&#xe622;'></i>
                            <h1>${producerCount}</h1>
                        </a>
                        <a href="javascript:;" data-url="/admin/production/producerList"
                           data-parent="true" data-title="生产信息列表">
                            <i class="iconfont " data-icon='&#xe622;'></i><span>生产信息列表</span></a>
                    </div>
                </section>
            </div>
        </c:if>

        <%--护理--%>
        <c:if test="${positionId==0 || positionId==20 || positionId==23}">
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-orange"><i class="iconfont">&#xe628;</i>
                    </div>
                    <div class="value tab-menu" style="margin-top: 2em">
                        <a href="javascript:;" data-url="admin/production/cropCareAdd"
                           data-parent="true" data-title="添加施肥信息"> <i class="iconfont" data-icon='&#xe628;'></i>
                            <span style="font-size: 14px;">添加施肥信息</span>
                        </a>
                    </div>
                </section>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2">
                <section class="panel">
                    <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe634;</i>
                    </div>
                    <div class="value tab-menu">
                        <a href="javascript:;" data-url="/admin/production/cropCareList"
                           data-parent="true" data-title="作物施肥记录"> <i class="iconfont " data-icon='&#xe634;'></i>
                            <h1>${cropCareCount}</h1>
                        </a>
                        <a href="javascript:;" data-url="/admin/production/cropCareList"
                           data-parent="true" data-title="作物施肥记录">
                            <i class="iconfont " data-icon='&#xe634;'></i><span>土地资源列表</span></a>
                    </div>
                </section>
            </div>
        </c:if>

        <%--系统介绍--%>
        <div class="row">
            <div class="col-xs-12">
                <section class="panel">
                    <div class="panel-heading">
                        网站信息
                        <a href="javascript:;" class="pull-right panel-toggle"><i class="iconfont">&#xe604;</i></a>
                    </div>
                    <div class="panel-body">
                        <table class="layui-table" style="text-align: center;">
                            <tbody>
                            <tr>
                                <td>
                                    <strong>软件名称</strong>：
                                </td>
                                <td>响应式农合社信息管理系统</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>前端架构</strong>：
                                </td>
                                <td>LayUI</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>后端架构</strong>：
                                </td>
                                <td>Spring ＋　SpringMVC + Hibernate</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>开发作者</strong>：
                                </td>
                                <td>莫慧华</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>服务器环境</strong>：
                                </td>
                                <td>Tomcat + MYSQL</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>项目管理工具</strong>：
                                </td>
                                <td>Maven</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>

        <%--文章列表--%>
        <c:if test="${positionId==0 || positionId==10 || positionId==11}">
            <div class="row">
                <div class="col-xs-12">
                    <section class="panel">
                        <div class="panel-heading">
                            新闻通知列表
                            <a href="javascript:;" class="pull-right panel-toggle"><i class="iconfont">&#xe604;</i></a>
                        </div>
                        <div class="panel-body" style="text-align: center;">
                            <div id="list" class="layui-form"></div>
                            <div class="text-right" id="page"></div>
                        </div>
                    </section>
                </div>
            </div>
        </c:if>
    </div>

</div>
</body>
<%@include file="/WEB-INF/layouts/admin/article-bief.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use(['main']);
    layui.use('list');
</script>

</html>
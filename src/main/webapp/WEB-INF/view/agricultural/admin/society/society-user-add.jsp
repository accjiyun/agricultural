<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-render="true"
                      action="/admin/society/createSocietyUser" method="post">

                    <div class="layui-form-item">
                        <label class="layui-form-label">姓名</label>
                        <div class="layui-input-block">
                            <input type="text" name="username" required jq-verify="required"
                                   jq-error="请输入正确的姓名" placeholder="姓名" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">社员性质</label>
                        <div class="layui-input-inline">
                            <select name="roleName" jq-verify="required" jq-error="请选择社员性质" lay-filter="verify">
                                <option value="">请选择社员性质</option>
                                <c:if test="${not empty roleNameList }">
                                    <c:forEach var="roleName" items="${roleNameList}" varStatus="status">
                                        <option value="${roleName}">${roleName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">身份证</label>
                        <div class="layui-input-block">
                            <input type="text" name="identityCard" required jq-verify="required|identity"
                                   jq-error="请输入正确的身份证号" placeholder="请输入身份证号" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item" pane>
                        <label class="layui-form-label">性别</label>
                        <div class="layui-input-inline">
                            <input type="radio" name="sex" title="男" value="男" checked/>
                            <input type="radio" name="sex" title="女" value="女"/>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">民族</label>
                        <div class="layui-input-block">
                            <input type="text" name="nation" required jq-verify="required"
                                   jq-error="请输入正确的民族" placeholder="请输入民族" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">手机</label>
                        <div class="layui-input-block">
                            <input type="text" name="mobile" required jq-verify="required|phone"
                                   jq-error="请输入正确的手机号" placeholder="手机号码" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">邮箱</label>
                        <div class="layui-input-block">
                            <input type="text" name="email" required jq-verify="required|email" jq-error="请选择正确的邮箱"
                                   placeholder="邮箱" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">地址</label>
                        <div class="layui-input-block">
                            <input type="text" name="address" required jq-verify="required"
                                   jq-error="请输入正确的地址" placeholder="地址" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">入社日期</label>
                        <div class="layui-input-block">
                            <input class="layui-input birth-date" name="joinDate" placeholder="入社日期">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">截止日期</label>
                        <div class="layui-input-block">
                            <input class="layui-input birth-date" name="insureEndDate" placeholder="合同到期日期">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">备注</label>
                        <div class="layui-input-block">
                            <input type="text" name="comments" placeholder="备注" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>
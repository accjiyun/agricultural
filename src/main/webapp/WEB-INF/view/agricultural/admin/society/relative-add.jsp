<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-render="true"
                      action="/admin/society/createRelative" method="post">
                    <div class="layui-form-item">
                        <label class="layui-form-label">户主姓名</label>
                        <div class="layui-input-inline">
                            <select name="userId" jq-verify="required" jq-error="请选择户主姓名" lay-filter="verify">
                                <option value="">请选择</option>
                                <c:if test="${not empty societyUserList }">
                                    <c:forEach var="societyUser" items="${societyUserList}" varStatus="status">
                                        <option value="${societyUser.userId}">${societyUser.username}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">关系</label>
                        <div class="layui-input-block">
                            <input type="text" name="relationship" required jq-verify="required"
                                   jq-error="请输入正确的关系" placeholder="与户主的关系" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">姓名</label>
                        <div class="layui-input-block">
                            <input type="text" name="relativeName" required jq-verify="required"
                                   jq-error="请输入正确的姓名" placeholder="姓名" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item" pane>
                        <label class="layui-form-label">性别</label>
                        <div class="layui-input-inline">
                            <input type="radio" name="sex" title="男" value="男" checked/>
                            <input type="radio" name="sex" title="女" value="女"/>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">民族</label>
                        <div class="layui-input-block">
                            <input type="text" name="nation" required jq-verify="required"
                                   jq-error="请输入正确的民族" placeholder="请输入民族" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">手机</label>
                        <div class="layui-input-block">
                            <input type="text" name="mobile" required jq-verify="required|phone"
                                   jq-error="请输入正确的手机号" placeholder="手机号码" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">邮箱</label>
                        <div class="layui-input-block">
                            <input type="text" name="email" required jq-verify="required|email" jq-error="请选择正确的邮箱"
                                   placeholder="邮箱" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">地址</label>
                        <div class="layui-input-block">
                            <input type="text" name="address" required jq-verify="required"
                                   jq-error="请输入正确的地址" placeholder="地址" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">备注</label>
                        <div class="layui-input-block">
                            <input type="text" name="comments" placeholder="备注" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>
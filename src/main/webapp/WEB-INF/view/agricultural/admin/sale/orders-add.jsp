<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-render="true"
                      action="/admin/sale/createOrders" method="post">
                    <div class="layui-form-item">
                        <label class="layui-form-label">客户姓名</label>
                        <div class="layui-input-inline">
                            <select name="customerId" jq-verify="required" jq-error="请选择客户姓名" lay-filter="verify">
                                <option value="">请选择</option>
                                <c:if test="${not empty customerList }">
                                    <c:forEach var="customer" items="${customerList}" varStatus="status">
                                        <option value="${customer.customerId}">${customer.customerName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">收购品种</label>
                        <div class="layui-input-inline">
                            <input type="text" name="variety" required jq-verify="required"
                                   jq-error="请输入正确的品种" placeholder="收购品种" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">收购量</label>
                        <div class="layui-input-inline">
                            <input type="text" name="count" jq-verify="required|number" jq-error="请输入正确的数量"  placeholder="数量" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">总价</label>
                        <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
                        <div class="layui-input-inline">
                            <input type="text" name="realPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">创建时间</label>
                        <div class="layui-input-block">
                            <input class="layui-input birth-date" name="createTime" placeholder="创建时间">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>
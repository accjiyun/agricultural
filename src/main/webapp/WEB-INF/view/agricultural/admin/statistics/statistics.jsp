<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<script src="/static/admin/js/lib/echarts.min.js"></script>
<script src="/static/admin/js/jquery.js"></script>
<div class="container-fluid larry-wrapper">
    <!--顶部统计数据预览 -->
    <!--<div class="row">
        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-blue"> <i class="iconfont">&#xe672;</i>
                </div>
                <div class="value">
                    <a href="#">
                        <h1>10</h1>
                    </a>
                    <p>会员总数</p>
                </div>
            </section>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-commred"> <i class="iconfont">&#xe674;</i>
                </div>
                <div class="value">
                    <a href="#">
                        <h1>10</h1>
                    </a>
                    <p>今日注册</p>
                </div>
            </section>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-dark-green"> <i class="iconfont">&#xe6bc;</i>
                </div>
                <div class="value">
                    <a href="#">
                        <h1>10</h1>
                    </a>
                    <p>男性会员</p>
                </div>
            </section>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-yellow-green"> <i class="iconfont">&#xe649;</i>
                </div>
                <div class="value">
                    <a href="#">
                        <h1>10</h1>
                    </a>
                    <p>女性会员</p>
                </div>
            </section>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-2">
            <section class="panel">
                <div class="symbol bgcolor-orange"> <i class="iconfont">&#xe638;</i>
                </div>
                <div class="value">
                    <a href="#">
                        <h1>10</h1>
                    </a>
                    <p>活跃会员</p>
                </div>
            </section>
        </div>

    </div>-->

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <section class="panel">
                <div class="panel-heading">
                    产品产值百分比
                    <a href="javascript:;" class="pull-right panel-toggle"><i class="iconfont">&#xe604;</i></a>
                </div>
                <div class="panel-body">
                    <div class="echarts" id="output"></div>
                </div>
            </section>
        </div>

        <div class="col-xs-12 col-md-6">

            <section class="panel">
                <div class="panel-heading">
                    各产品产量变化趋势
                    <a href="javascript:;" class="pull-right panel-toggle"><i class="iconfont">&#xe604;</i></a>
                </div>
                <div class="panel-body">
                    <div class="echarts" id="yield"></div>
                </div>
            </section>
        </div>

        <div class="col-xs-12 col-md-6">

            <section class="panel">
                <div class="panel-heading">
                    近几年实际产品价格浮动
                    <a href="javascript:;" class="pull-right panel-toggle"><i class="iconfont">&#xe604;</i></a>
                </div>
                <div class="panel-body">
                    <div class="echarts" id="price"></div>
                </div>
            </section>
        </div>

    </div>

</div>

</body>
<script src="/static/admin/js/layui/layui.js"></script>
<script src="/static/admin/js/statistics.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

</html>
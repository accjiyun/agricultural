<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <!--头部搜索-->
            <section class="panel panel-padding">
                <form class="layui-form" action="/admin/article/getArticleList?pageNo=1">
                    <div class="layui-form">
                        <div class="layui-inline">
                            <select name="articleType">
                                <option value="">请选择类型</option>
                                <c:if test="${not empty articleTypeList }">
                                    <c:forEach var="articleTypeName" items="${articleTypeList}" varStatus="status">
                                        <option value="${articleTypeName}">${articleTypeName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input start-date" name="start_date" placeholder="开始日">
                            </div>
                            <div class="layui-input-inline">
                                <input class="layui-input end-date" name="end_date" placeholder="截止日">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input" name="keyword" placeholder="关键字">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button lay-submit class="layui-btn" lay-filter="search">查找</button>
                        </div>
                    </div>
                </form>
            </section>

            <!--列表-->
            <section class="panel panel-padding">
                <div class="group-button">
                    <button class="layui-btn layui-btn-small layui-btn-danger ajax-all" data-name="articleId"
                            data-params='{"url": "/admin/article/delete","loading":"true"}'>
                        <i class="iconfont">&#xe626;</i> 删除
                    </button>
                    <button class="layui-btn layui-btn-small modal-iframe"
                            data-params='{"content": "/admin/article/initAddArticle", "title": "添加文章","full":"true"}'>
                        <i class="iconfont">&#xe649;</i> 添加
                    </button>
                </div>
                <div id="list" class="layui-form"></div>

                <div class="text-right" id="page"></div>
            </section>
        </div>
    </div>
</div>

<div class="add-subcat">
    <form id="form1" class="layui-form layui-form-pane" data-name="articleData" data-tpl="list-tpl"
          data-render="true" action="/admin/article/updateArticle" method="post">
        <div class="layui-form-item">
            <label class="layui-form-label">文章标题</label>
            <div class="layui-input-block">
                <input type="text" name="title" required jq-verify="" jq-error="请输入标题|请输入数字"
                       placeholder="请输入标题" autocomplete="off" class="layui-input ">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">关键字</label>
            <div class="layui-input-block">
                <input type="text" name="keyWord" required jq-verify="required" placeholder="请输入关键字，多个请用英文逗号隔开"
                       autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">上传缩略图</label>
            <div class="layui-input-block">
                <input type="file" name="file" class="layui-upload-file" id="article">
                <input type="hidden" name="imageUrl" error-id="img-error">
                <p id="img-error" class="error"></p>
            </div>
            <div class="layui-input-block">
                <div class="imgbox">
                    <img name="imageUrl" src="/favicon.ico" alt="暂无" class="img-thumbnail">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">文章类型</label>
            <div class="layui-input-inline">
                <select name="articleType" jq-verify="required" jq-error="请输入文章类型" lay-filter="verify">
                    <option value="">请选择类型</option>
                    <c:if test="${not empty articleTypeList }">
                        <c:forEach var="articleTypeName" items="${articleTypeList}" varStatus="status">
                            <option value="${articleTypeName}">${articleTypeName}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
        </div>

        <label class="layui-form-label">内容</label>
        <div class="layui-form-item layui-form-text">
            <div class="layui-input-block">
                <textarea name="content" jq-verify="content" id="content" style="display:none;"></textarea>
            </div>
        </div>

        <div class="layui-form-item" pane>
            <label class="layui-form-label">发布时间</label>
            <div class="layui-input-block">
                <input class="layui-input birth-date" name="publicTime" placeholder="发布时间">
            </div>
        </div>

        <div class="layui-form-item ">
            <label class="layui-form-label">点击数</label>
            <div class="layui-input-inline">
                <input type="text" name="clickNum" value="0" jq-verify="number" jq-error="只能填写数字" autocomplete="off"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item" pane>
            <label class="layui-form-label">发布</label>
            <div class="layui-input-inline">
                <input type="radio" name="status" title="是" value="1" checked/>
                <input type="radio" name="status" title="否" value="0"/>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>

</body>
<%@include file="/WEB-INF/layouts/admin/article.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use('list');
    layui.use('myform');

    function test(ret, options, $) {
        console.log($);
        alert("这是自定义的ajax返回方法，可以用$哦");
    }
</script>

</html>
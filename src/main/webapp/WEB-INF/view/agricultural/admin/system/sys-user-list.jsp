<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <!--头部搜索-->
            <section class="panel panel-padding">
                <form class="layui-form" action="/admin/system/getUserList?pageNo=1">
                    <div class="layui-form">
                        <div class="layui-inline">
                            <select name="departmentName">
                                <option value="">请选择部门</option>
                                <c:if test="${not empty departmentNameList }">
                                    <c:forEach var="departmentName" items="${departmentNameList}" varStatus="status">
                                        <option value="${departmentName}">${departmentName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <select name="disabled" lay-verify="required">
                                <option value="-1">请选择状态</option>
                                <option value="0">启用</option>
                                <option value="1">禁用</option>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input" name="keyword" placeholder="登录名">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button lay-submit class="layui-btn" lay-filter="search">查找</button>
                        </div>
                    </div>
                </form>
            </section>

            <!--列表-->
            <section class="panel panel-padding">
                <div class="group-button">
                    <button class="layui-btn layui-btn-small layui-btn-danger ajax-all" data-name="userId"
                            data-params='{"url": "/admin/system/delete","loading":"true"}'>
                        <i class="iconfont">&#xe626;</i> 删除
                    </button>
                    <button class="layui-btn layui-btn-small modal-iframe"
                            data-params='{"content": "/admin/system/userAdd", "title": "添加系统用户","full":"true"}'>
                        <i class="iconfont">&#xe649;</i> 添加
                    </button>
                </div>
                <div id="list" class="layui-form"></div>

                <div class="text-right" id="page"></div>
            </section>
        </div>
    </div>
</div>

<div class="add-subcat">
    <form id="form1" class="layui-form layui-form-pane" data-name="systemUserData" data-tpl="list-tpl"
          data-render="true" action="/admin/system/update" method="post">

        <div class="layui-form-item">
            <label class="layui-form-label">登录账号</label>
            <div class="layui-input-block">
                <input type="text" name="loginName" required jq-verify="required|validateLoginName"
                       jq-error="请输入帐号|用户已存在"
                       placeholder="请输入登录账号" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">部门</label>
            <div class="layui-input-inline">
                <select name="departmentName" jq-verify="required" jq-error="请选择部门" lay-filter="verify">
                    <option value="">请选择部门</option>
                    <c:if test="${not empty departmentNameList }">
                        <c:forEach var="departmentName" items="${departmentNameList}" varStatus="status">
                            <option value="${departmentName}">${departmentName}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">职位</label>
            <div class="layui-input-inline">
                <select name="positionName" jq-verify="required" jq-error="请选择职位" lay-filter="verify">
                    <option value="">请选择职位</option>
                    <c:if test="${not empty positionNameList }">
                        <c:forEach var="positionName" items="${positionNameList}" varStatus="status">
                            <option value="${positionName}">${positionName}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
        </div>

        <div class="layui-form-item" pane>
            <label class="layui-form-label">状态</label>
            <div class="layui-input-inline">
                <input type="radio" name="disabled" title="启用" value="0" checked/>
                <input type="radio" name="disabled" title="禁用" value="1"/>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>

</body>
<%@include file="/WEB-INF/layouts/admin/sys-user.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use('list');
    layui.use('myform');

    function test(ret, options, $) {
        console.log($);
        alert("这是自定义的ajax返回方法，可以用$哦");
    }
</script>

</html>
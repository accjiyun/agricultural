<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane"
                      action="/admin/system/updatePWD/${systemUser.userId}">
                    <div class="layui-form-item">
                        <label class="layui-form-label">管理员帐号</label>
                        <div class="layui-input-inline">
                            <input type="text" name="loginName" required jq-verify="required" jq-error="请输入帐号"
                                   disabled="disabled" value="${systemUser.loginName}" placeholder="请输入帐号"
                                   autocomplete="off"
                                   class="layui-input layui-disabled">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">新密码</label>
                        <div class="layui-input-inline">
                            <input type="password" name="password" id="password" required jq-verify="required"
                                   jq-error="请输入密码" placeholder="新密码" autocomplete="off" class="layui-input ">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">确认新密码</label>
                        <div class="layui-input-inline">
                            <input type="password" placeholder="确认新密码" autocomplete="off" required
                                   jq-verify="required|confirmPass"
                                   jq-error="请输入密码|密码不一致" class="layui-input ">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit jq-filter="submit">立即提交</button>
                            <button type="reset" id="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-render="true"
                      action="/admin/system/createSystemUser" method="post">
                    <div class="layui-form-item">
                        <label class="layui-form-label">用户姓名</label>
                        <div class="layui-input-inline">
                            <select name="userId" jq-verify="required" jq-error="请选择系统用户" lay-filter="verify">
                                <option value="">请选择</option>
                                <c:if test="${not empty societyUserList }">
                                    <c:forEach var="societyUser" items="${societyUserList}" varStatus="status">
                                        <option value="${societyUser.userId}">${societyUser.username}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">登录账号</label>
                        <div class="layui-input-block">
                            <input type="text" name="loginName" required jq-verify="required|validateLoginName" jq-error="请输入帐号|用户已存在"
                                   placeholder="请输入登录账号" autocomplete="off" class="layui-input ">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">密码</label>
                        <div class="layui-input-block">
                            <input type="password" id="password" name="password" required jq-verify="required"
                                   jq-error="请输入密码" placeholder="密码" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">确认密码</label>
                        <div class="layui-input-block">
                            <input type="password" id="repassword" placeholder="确认密码" required jq-verify="required|confirmPass"
                                   jq-error="请输入密码|密码不一致" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">部门</label>
                        <div class="layui-input-inline">
                            <select name="departmentName" jq-verify="required" jq-error="请选择部门" lay-filter="verify">
                                <option value="">请选择部门</option>
                                <c:if test="${not empty departmentNameList }">
                                    <c:forEach var="departmentName" items="${departmentNameList}" varStatus="status">
                                        <option value="${departmentName}">${departmentName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">职位</label>
                        <div class="layui-input-inline">
                            <select name="positionName" jq-verify="required" jq-error="请选择职位" lay-filter="verify">
                                <option value="">请选择职位</option>
                                <c:if test="${not empty positionNameList }">
                                    <c:forEach var="positionName" items="${positionNameList}" varStatus="status">
                                        <option value="${positionName}">${positionName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item" pane>
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-inline">
                            <input type="radio" name="disabled" title="启用" value="0" checked/>
                            <input type="radio" name="disabled" title="禁用" value="1"/>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>
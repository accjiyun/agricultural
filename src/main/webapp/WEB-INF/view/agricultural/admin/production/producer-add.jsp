<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-render="true"
                      action="/admin/production/createProducer" method="post">

                    <div class="layui-form-item">
                        <label class="layui-form-label">产地名称</label>
                        <div class="layui-input-inline">
                            <select name="producerName" jq-verify="required" jq-error="请选择产地名称" lay-filter="verify">
                                <option value="">请选择产地名称</option>
                                <c:if test="${not empty landList }">
                                    <c:forEach var="land" items="${landList}" varStatus="status">
                                        <option value="${land.landName}">${land.landName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">使用面积</label>
                        <div class="layui-input-inline">
                            <input type="text" name="area" jq-verify="required|number" jq-error="请输入正确的数量"  placeholder="使用面积" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">生产类型</label>
                        <div class="layui-input-inline">
                            <select name="producerType" jq-verify="required" jq-error="请选择生产类型" lay-filter="verify">
                                <option value="">请选择生产类型</option>
                                <option value="种植">种植</option>
                                <option value="畜牧">畜牧</option>
                                <option value="水产">水产</option>
                                <option value="林业">林业</option>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">品种</label>
                        <div class="layui-input-block">
                            <input type="text" name="varietyName" required jq-verify="required"
                                   jq-error="请输入品种" placeholder="品种" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">批次时长</label>
                        <div class="layui-input-inline">
                            <input type="text" name="batchDuration" jq-verify="required|number" jq-error="请输入正确的天数"  placeholder="天数" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">负责人姓名</label>
                        <div class="layui-input-block">
                            <input type="text" name="principalName" required jq-verify="required"
                                   jq-error="请输入正确的负责人姓名" placeholder="负责人姓名" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">批次</label>
                        <div class="layui-input-inline">
                            <input type="text" name="batchId" jq-verify="required|number" jq-error="请输入正确的批次"  placeholder="批次" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">预计年产量</label>
                        <div class="layui-input-inline">
                            <input type="text" name="plannedYield" jq-verify="required|number" jq-error="请输入正确的产量"  placeholder="预计年产量(kg)" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">预估单价</label>
                        <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
                        <div class="layui-input-inline">
                            <input type="text" name="plannedPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">预估总价</label>
                        <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
                        <div class="layui-input-inline">
                            <input type="text" name="plannedTotalPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">开始时间</label>
                        <div class="layui-input-block">
                            <input class="layui-input birth-date" name="startTime" placeholder="开始生产时间">
                        </div>
                    </div>

                    <div class="layui-form-item" pane>
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-inline">
                            <input type="radio" name="isProduced" title="未生产" value="0" checked/>
                            <input type="radio" name="isProduced" title="已生产" value="1"/>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">实际产量</label>
                        <div class="layui-input-inline">
                            <input type="text" name="realYield" jq-verify="required|number" jq-error="请输入正确的产量"  placeholder="实际产量(kg)" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">实际单价</label>
                        <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
                        <div class="layui-input-inline">
                            <input type="text" name="realPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">实际总价</label>
                        <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
                        <div class="layui-input-inline">
                            <input type="text" name="realTotalPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">结束日期</label>
                        <div class="layui-input-block">
                            <input class="layui-input birth-date" name="endTime">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">备注</label>
                        <div class="layui-input-block">
                            <input type="text" name="comments" placeholder="备注" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <!--头部搜索-->
            <section class="panel panel-padding">
                <form class="layui-form" action="/admin/production/getCropCareList?pageNo=1">
                    <div class="layui-form">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input start-date" name="start_date" placeholder="开始日">
                            </div>
                            <div class="layui-input-inline">
                                <input class="layui-input end-date" name="end_date" placeholder="截止日">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input" name="keyword" placeholder="负责人、种苗、药品等">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button lay-submit class="layui-btn" lay-filter="search">查找</button>
                        </div>
                    </div>
                </form>
            </section>

            <!--列表-->
            <section class="panel panel-padding">
                <div class="group-button">
                    <button class="layui-btn layui-btn-small layui-btn-danger ajax-all" data-name="articleId"
                            data-params='{"url": "/admin/production/deleteCropCare","loading":"true"}'>
                        <i class="iconfont">&#xe626;</i> 删除
                    </button>
                    <button class="layui-btn layui-btn-small modal-iframe"
                            data-params='{"content": "/admin/production/cropCareAdd", "title": "添加护理记录","full":"true"}'>
                        <i class="iconfont">&#xe649;</i> 添加
                    </button>
                </div>
                <div id="list" class="layui-form"></div>

                <div class="text-right" id="page"></div>
            </section>
        </div>
    </div>
</div>

<div class="add-subcat">
    <form id="form1" class="layui-form layui-form-pane" data-name="articleData" data-tpl="list-tpl"
          data-render="true" action="/admin/production/updateCropCare" method="post">
        <div class="layui-form-item">
            <label class="layui-form-label">产地</label>
            <div class="layui-input-inline">
                <select name="landId" jq-verify="required" jq-error="请选择产地编号" lay-filter="verify">
                    <option value="">请选择产地</option>
                    <c:if test="${not empty landList }">
                        <c:forEach var="land" items="${landList}" varStatus="status">
                            <option value="${land.landId}">${land.landName}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">批次</label>
            <div class="layui-input-inline">
                <input type="text" name="batch" jq-verify="required|number" jq-error="请输入正确的批次"  placeholder="批次" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">负责人姓名</label>
            <div class="layui-input-block">
                <input type="text" name="principalName" required jq-verify="required"
                       jq-error="请输入正确的负责人姓名" placeholder="负责人姓名" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">种苗</label>
            <div class="layui-input-block">
                <input type="text" name="germchit" required jq-verify="required"
                       jq-error="请输入种苗" placeholder="种苗" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">数量</label>
            <div class="layui-input-inline">
                <input type="text" name="count" jq-verify="required|number" jq-error="请输入正确的数量"  placeholder="数量" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">肥（饲）料</label>
            <div class="layui-input-block">
                <input type="text" name="fertilizer" required jq-verify="required"
                       jq-error="请输入肥（饲）料" placeholder="肥（饲）料" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">药品</label>
            <div class="layui-input-block">
                <input type="text" name="drug" required jq-verify="required"
                       jq-error="请输入药品" placeholder="药品" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">护理时间</label>
            <div class="layui-input-block">
                <input class="layui-input birth-date" name="cropTime" placeholder="护理时间">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>

</body>
<%@include file="/WEB-INF/layouts/admin/cropCare.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use('list');
    layui.use('myform');

    function test(ret, options, $) {
        console.log($);
        alert("这是自定义的ajax返回方法，可以用$哦");
    }
</script>

</html>
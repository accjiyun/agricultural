<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <!--头部搜索-->
            <section class="panel panel-padding">
                <form class="layui-form" action="/admin/production/getProducerList?pageNo=1">
                    <div class="layui-form">
                        <div class="layui-inline">
                            <select name="producerType">
                                <option value="">请选择生产类型</option>
                                <option value="种植">种植</option>
                                <option value="畜牧">畜牧</option>
                                <option value="水产">水产</option>
                                <option value="林业">林业</option>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input start-date" name="start_date" placeholder="开始日">
                            </div>
                            <div class="layui-input-inline">
                                <input class="layui-input end-date" name="end_date" placeholder="截止日">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input" name="keyword" placeholder="产地名称、品种、负责人姓名">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button lay-submit class="layui-btn" lay-filter="search">查找</button>
                        </div>
                    </div>
                </form>
            </section>

            <!--列表-->
            <section class="panel panel-padding">
                <div class="group-button">
                    <button class="layui-btn layui-btn-small layui-btn-danger ajax-all" data-name="articleId"
                            data-params='{"url": "/admin/production/deleteProducer","loading":"true"}'>
                        <i class="iconfont">&#xe626;</i> 删除
                    </button>
                    <button class="layui-btn layui-btn-small modal-iframe"
                            data-params='{"content": "/admin/production/producerAdd", "title": "添加生产信息","full":"true"}'>
                        <i class="iconfont">&#xe649;</i> 添加
                    </button>
                </div>
                <div id="list" class="layui-form"></div>

                <div class="text-right" id="page"></div>
            </section>
        </div>
    </div>
</div>

<div class="add-subcat">
    <form id="form1" class="layui-form layui-form-pane" data-name="producerData" data-tpl="list-tpl"
          data-render="true" action="/admin/production/updateProducer" method="post">
        <div class="layui-form-item">
            <label class="layui-form-label">产地名称</label>
            <div class="layui-input-block">
                <input type="text" name="producerName" required jq-verify="required"
                       jq-error="请输入产地名称" placeholder="产地名称" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">使用面积</label>
            <div class="layui-input-inline">
                <input type="text" name="area" jq-verify="required|number" jq-error="请输入正确的数量"  placeholder="使用面积" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">生产类型</label>
            <div class="layui-input-inline">
                <select name="producerType" jq-verify="required" jq-error="请选择生产类型" lay-filter="verify">
                    <option value="">请选择生产类型</option>
                    <option value="种植">种植</option>
                    <option value="畜牧">畜牧</option>
                    <option value="水产">水产</option>
                    <option value="林业">林业</option>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">品种</label>
            <div class="layui-input-block">
                <input type="text" name="varietyName" required jq-verify="required"
                       jq-error="请输入品种" placeholder="品种" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">批次时长</label>
            <div class="layui-input-inline">
                <input type="text" name="batchDuration" jq-verify="required|number" jq-error="请输入正确的天数"  placeholder="天数" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">负责人姓名</label>
            <div class="layui-input-block">
                <input type="text" name="principalName" required jq-verify="required"
                       jq-error="请输入正确的负责人姓名" placeholder="负责人姓名" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">批次</label>
            <div class="layui-input-inline">
                <input type="text" name="batchId" jq-verify="required|number" jq-error="请输入正确的批次"  placeholder="批次" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">预计年产量</label>
            <div class="layui-input-inline">
                <input type="text" name="plannedYield" jq-verify="required|number" jq-error="请输入正确的产量"  placeholder="预计年产量(kg)" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">预估单价</label>
            <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
            <div class="layui-input-inline">
                <input type="text" name="plannedPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">预估总价</label>
            <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
            <div class="layui-input-inline">
                <input type="text" name="plannedTotalPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">开始时间</label>
            <div class="layui-input-block">
                <input class="layui-input birth-date" name="startTime" placeholder="开始生产时间">
            </div>
        </div>

        <div class="layui-form-item" pane>
            <label class="layui-form-label">状态</label>
            <div class="layui-input-inline">
                <input type="radio" name="isProduced" title="未生产" value="0" checked/>
                <input type="radio" name="isProduced" title="已生产" value="1"/>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">实际产量</label>
            <div class="layui-input-inline">
                <input type="text" name="realYield" jq-verify="required|number" jq-error="请输入正确的产量"  placeholder="实际产量(kg)" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">实际单价</label>
            <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
            <div class="layui-input-inline">
                <input type="text" name="realPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">实际总价</label>
            <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
            <div class="layui-input-inline">
                <input type="text" name="realTotalPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">结束日期</label>
            <div class="layui-input-block">
                <input class="layui-input birth-date" name="endTime" placeholder="结束生产日期">
            </div>
        </div>


        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <input type="text" name="comments" placeholder="备注" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>

</body>
<%@include file="/WEB-INF/layouts/admin/producer.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use('list');
    layui.use('myform');

    function test(ret, options, $) {
        console.log($);
        alert("这是自定义的ajax返回方法，可以用$哦");
    }
</script>

</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-render="true"
                      action="/admin/production/createCropCare" method="post">

                    <div class="layui-form-item">
                        <label class="layui-form-label">产地</label>
                        <div class="layui-input-inline">
                            <select name="landId" jq-verify="required" jq-error="请选择产地编号" lay-filter="verify">
                                <option value="">请选择产地</option>
                                <c:if test="${not empty landList }">
                                    <c:forEach var="land" items="${landList}" varStatus="status">
                                        <option value="${land.landId}">${land.landName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">批次</label>
                        <div class="layui-input-inline">
                            <input type="text" name="batch" jq-verify="required|number" jq-error="请输入正确的批次"  placeholder="批次" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">负责人姓名</label>
                        <div class="layui-input-block">
                            <input type="text" name="principalName" required jq-verify="required"
                                   jq-error="请输入正确的负责人姓名" placeholder="负责人姓名" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">种苗</label>
                        <div class="layui-input-block">
                            <input type="text" name="germchit" required jq-verify="required"
                                   jq-error="请输入种苗" placeholder="种苗" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">数量</label>
                        <div class="layui-input-inline">
                            <input type="text" name="count" jq-verify="required|number" jq-error="请输入正确的数量"  placeholder="数量" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">肥（饲）料</label>
                        <div class="layui-input-block">
                            <input type="text" name="fertilizer" required jq-verify="required"
                                   jq-error="请输入肥（饲）料" placeholder="肥（饲）料" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">药品</label>
                        <div class="layui-input-block">
                            <input type="text" name="drug" required jq-verify="required"
                                   jq-error="请输入药品" placeholder="药品" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">护理时间</label>
                        <div class="layui-input-block">
                            <input class="layui-input birth-date" name="cropTime" placeholder="护理时间">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use('myform');
</script>

</html>
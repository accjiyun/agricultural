<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-render="true"
                      action="/admin/production/createLand" method="post">

                    <div class="layui-form-item">
                        <label class="layui-form-label">地点名称</label>
                        <div class="layui-input-block">
                            <input type="text" name="landName" required jq-verify="required"
                                   jq-error="请输入地点名称" placeholder="地点名称" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">面积</label>
                        <div class="layui-input-inline">
                            <input type="text" name="area" jq-verify="required|number" jq-error="请输入正确的公顷"  placeholder="面积公顷" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">土地类型</label>
                        <div class="layui-input-inline">
                            <select name="landTypeName" jq-verify="required" jq-error="请选择土地类型" lay-filter="verify">
                                <option value="">请选择土地类型</option>
                                <c:if test="${not empty landTypeNameList }">
                                    <c:forEach var="landTypeName" items="${landTypeNameList}" varStatus="status">
                                        <option value="${landTypeName}">${landTypeName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">总价</label>
                        <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
                        <div class="layui-input-inline">
                            <input type="text" name="totalPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">合同编号</label>
                        <div class="layui-input-block">
                            <input type="text" name="contractId" required jq-verify="required"
                                   jq-error="请输入合同编号" placeholder="合同编号" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">合同类型</label>
                        <div class="layui-input-inline">
                            <select name="contractTypeName" jq-verify="required" jq-error="请选择合同类型" lay-filter="verify">
                                <option value="">请选择合同类型</option>
                                <c:if test="${not empty contractTypeNameList }">
                                    <c:forEach var="contractTypeName" items="${contractTypeNameList}" varStatus="status">
                                        <option value="${contractTypeName}">${contractTypeName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">签订日期</label>
                        <div class="layui-input-block">
                            <input class="layui-input birth-date" name="insureStartDate" placeholder="签订日期">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">期限</label>
                        <div class="layui-input-block">
                            <div class="layui-input-inline">
                                <input type="text" name="deadline" jq-verify="required|number" jq-error="请输入正确的期限"  placeholder="期限(月)" autocomplete="off" class="layui-input ">
                            </div>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">实际总价</label>
                        <span class="left-icon"><i class="iconfont">&#xe690;</i></span>
                        <div class="layui-input-inline">
                            <input type="text" name="realTotalPrice" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">转让方</label>
                        <div class="layui-input-block">
                            <input type="text" name="transferor" required jq-verify="required"
                                   jq-error="请输入转让方" placeholder="转让方" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">受让方</label>
                        <div class="layui-input-block">
                            <input type="text" name="assignee" required jq-verify="required"
                                   jq-error="请输入受让方" placeholder="受让方" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">联系电话</label>
                        <div class="layui-input-block">
                            <input type="text" name="mobile" required jq-verify="required|phone"
                                   jq-error="请输入正确的手机号" placeholder="手机号码" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">备注</label>
                        <div class="layui-input-block">
                            <input type="text" name="comments" placeholder="备注" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>
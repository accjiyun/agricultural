<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/web/header.jsp" %>

<section class="global-page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2>${articleType.typeName}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/index">
                                <i class="ion-ios-home"></i> 首页
                            </a>
                        </li>
                        <li class="active">${articleType.typeName}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#Page header-->
<section id="blog-full-width">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <c:if test="${not empty articleList}">
                    <c:forEach var="article" items="${articleList}">
                        <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                            <c:if test="${not empty article.articleTypeByArticleTypeId.articleTypeId}">
                                <div class="blog-post-image">
                                    <a href="/article/articleInfo/${article.articleId}.html">
                                        <img class="img-responsive" src="${article.imageUrl}" alt=""/>
                                    </a>
                                </div>
                            </c:if>
                            <div class="blog-content">
                                <h2 class="blogpost-title">
                                    <a href="/article/articleInfo/${article.articleId}.html">${article.title}</a>
                                </h2>
                                <div class="blog-meta">
                                    <span><fmt:formatDate value="${article.publicTime}" pattern="yyyy年MM月"/></span>
                                </div>
                                <p class="indent">${article.summary}</p>
                                <a href="/article/articleInfo/${article.articleId}.html"
                                   class="btn btn-dafault btn-details">Continue Reading</a>
                            </div>
                        </article>
                    </c:forEach>
                </c:if>
                <c:if test="${empty articleList}">
                    暂无记录
                </c:if>
                <!-- 公共分页 开始 -->
                <form action="/science/${articleType}" method="post" id="searchForm">
                    <input type="hidden" name="page.pageNo" id="pageCurrentPage" value="1">
                </form>
                <div>
                    <jsp:include page="/WEB-INF/view/common/front_page.jsp"></jsp:include>
                    <div class="clear"></div>
                </div>
                <!-- 公共分页 结束 -->
            </div>
        </div>
    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="col-md-8">
            <p class="copyright">Copyright: <span>2017</span> . Powered by
                <a href="http://www.guet.edu.cn/extGuetWeb">MO</a>
            </p>
        </div>
    </div>
</footer>

</body>

</html>
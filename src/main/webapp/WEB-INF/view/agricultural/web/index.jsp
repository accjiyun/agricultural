<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/web/header.jsp" %>

<section id="hero-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="block wow fadeInUp" data-wow-delay=".3s">

                    <!-- Slider -->
                    <section class="cd-intro">
                        <h1 class="wow fadeInUp animated cd-headline slide" data-wow-delay=".4s">
                            <span>响应式农合社信息管理系统</span><br>
                            <span class="cd-words-wrapper">
                                    <b class="is-visible">高效</b>
                                    <b>灵活</b>
                                    <b>方便</b>
                                </span>
                        </h1>
                    </section>

                    <h2 class="wow fadeInUp animated" data-wow-delay=".6s">
                        互联网技术改变传统的农合社经营管理方式，<br>
                        提供人性化办公软件，改善农合社资源、信息管理，提高服务水平。
                    </h2>
                    <a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green"
                       data-wow-delay=".9s" href="#works" data-section="#feature">合作社结构</a>

                </div>
            </div>
        </div>
    </div>
</section>

<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="block wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="500ms">
                    <h2>关于</h2>
                    <p class="indent">
                        农合社信息资源管理平台，实现农合社资源的信息化、规范化、智能化管理。使系统能够通过科学的管理方法、快速的查询、灵活的统计功能，优化农合社资源信息管理工作，让工作人员从繁琐的填表、查表工作中解放出来。降低农合社信息管理的各项开支，提高行政工作效率，改善服务质量，为农合社领导决策提供支持。
                    </p>
                    <p class="indent">
                        利用信息技术推进经济发展，让知识助力农业发展，发挥农合社在推进农业产业化经营的作用。为推进社会主义新农村建设，走中国特色的农业现代化道路，加快形成城乡经济社会发展一体化新格局而努力，促进农业和农村经济的快速发展。
                    </p>
                </div>

            </div>
            <div class="col-md-6 col-sm-6">
                <div class="block wow fadeInRight" data-wow-delay=".3s" data-wow-duration="500ms">
                    <img src="/static/web/images/about/about.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="feature">
    <div class="container">
        <div class="section-heading">
            <h1 class="title wow fadeInDown" data-wow-delay=".3s">社员组成</h1>
            <p class="wow fadeInDown" data-wow-delay=".5s">
                社员是由个人、企业、农户三大部分组成。
            </p>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-lightbulb-outline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">个人</h4>
                        <p>以劳动力招聘入社，对部分社务进行管理。</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="600ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-keypad-outline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">企业</h4>
                        <p>与企业合作有利于促进农合社更好更快的发展。</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="900ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-barcode-outline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">农户</h4>
                        <p>对农户及家庭成员的管理是农合社的发展需求。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">管理系统登录</h2>
                    <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">
                        响应式农合社信息管理系统主要包括办公室、生产部、销售部三大部门，办公室有人事、行政两个职位，人事负责社员信息的管理以及系统用户的管理，行政负责新闻通知的管理。生产部有土地管理、生产、护理三个职位，土地管理负责土地的流转与管理，生产负责合作社社内生产计划及实施管理，护理负责生产过程中对作物所进行的护理操作记录。销售部有业务、销售两个职位，业务负责客户信息管理，销售负责社内销售订单管理。 </p>
                    <a href="/admin" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s"
                       data-wow-duration="500ms">登录</a>
                </div>
            </div>

        </div>
    </div>
</section>
<footer id="footer">
    <div class="container">
        <div class="col-md-8">
            <p class="copyright">Copyright: <span>2017</span> . Powered by <a href="http://www.guet.edu.cn/extGuetWeb">MO</a>
            </p>
        </div>
    </div>
</footer>
</body>
</html>
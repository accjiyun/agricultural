<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/web/header.jsp" %>
<section class="global-page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2>${article.title}</h2>
                    <div class="portfolio-meta">
                        <span><fmt:formatDate value="${article.publicTime }" pattern="yyyy年MM月dd日"/></span>|
                        <span> 分类: ${article.articleTypeByArticleTypeId.typeName }</span>|
                        <span> 点击数: ${article.clickNum }</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#Page header-->

<section class="portfolio-single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <c:if test="${not empty article.articleTypeByArticleTypeId.articleTypeId}">
                    <div class="portfolio-single-img">
                        <img class="img-responsive" alt="" src="${article.imageUrl}">
                    </div>
                </c:if>
                <div class="portfolio-content">
                    <p>${content}</p>
                </div>

            </div>
        </div>
    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="col-md-8">
            <p class="copyright">Copyright: <span>2017</span> . Powered by
                <a href="http://www.guet.edu.cn/extGuetWeb">MO</a>
            </p>
        </div>
    </div>
</footer>

</body>

</html>
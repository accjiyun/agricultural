/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : agricultural_db

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2017-05-31 21:24:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '信息ID',
  `article_type_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '文章类型1:新闻,2:通知公告',
  `title` varchar(100) DEFAULT NULL COMMENT '文章标题',
  `key_word` varchar(50) DEFAULT '' COMMENT '关键字',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '文章创建时间',
  `public_time` timestamp NULL DEFAULT NULL COMMENT '文章发布时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '发表状态：0为未发布，1为发布',
  `image_url` varchar(255) DEFAULT NULL COMMENT '文章缩略图URL',
  `click_num` int(11) DEFAULT '0' COMMENT '文章点击量',
  `sort` int(11) DEFAULT '0' COMMENT '排序值',
  `summary` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`article_id`),
  KEY `ARTICLE_TYPE` (`article_type_id`),
  CONSTRAINT `FKl4lb18ah02nqv88igq5284kvy` FOREIGN KEY (`article_type_id`) REFERENCES `article_type` (`article_type_id`),
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`article_type_id`) REFERENCES `article_type` (`article_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('3', '2', '新东方英语四级学习讲座通知', '新东方', '2017-05-02 20:32:28', '2017-05-01 00:00:00', '1', '', '13', null, null);
INSERT INTO `article` VALUES ('4', '1', '自治区党委书记彭清华考察我校时勉励师生：崇尚创新实干，陶冶高尚情操（图）', '党委书记,彭清华', '2017-05-11 15:03:05', '2017-05-05 00:00:00', '1', '/images/upload/article/20170511/1494486718073.jpg', '16', null, null);
INSERT INTO `article` VALUES ('5', '2', '关于做好2017年征兵报名工作的通知', '征兵', '2017-05-11 15:20:00', '2017-05-02 00:00:00', '1', '/images/upload/article/20170511/1494487182913.jpg', '7', null, null);
INSERT INTO `article` VALUES ('6', '2', '关于桂林电子科技大学金鸡岭校区废旧锅炉进行处置的公告', '废旧锅炉', '2017-05-11 15:24:12', '2017-05-11 00:00:00', '1', '', '5', null, null);

-- ----------------------------
-- Table structure for article_content
-- ----------------------------
DROP TABLE IF EXISTS `article_content`;
CREATE TABLE `article_content` (
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '文章ID',
  `content` text COMMENT '文章内容',
  PRIMARY KEY (`article_id`),
  CONSTRAINT `article_content_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article_content
-- ----------------------------
INSERT INTO `article_content` VALUES ('3', '<p></p><p>　　<span>2017年6月份的全国大学生英语四级考试还有两个多月的时间就要到来了！你是否正为没有好的备战方法而苦恼？是否为怎样正确把握时间而抓狂？</span></p><span>&nbsp;</span><p></p><p></p><p></p><p>　　坚实的语言基础和熟练的解题技巧、应试策略，是顺利通过大学英语四、六级考试和取得高分的不可或缺的条件。</p>&nbsp;<p></p><p>　　为了帮助同学们找到适当且高效率的复习方法，在考试中取得好成绩，<span><strong>我校图书馆</strong></span>特邀请南宁新东方学校主管尹广吉举办英语四级讲座，重点讲解考前复习方法和考试注意事项，还会针对同学们提出的问题进行解答。帮助同学们进行高效率复习，调整好考前心态，合理有效的备战本次四级考试。</p><p>&nbsp;</p><p>主讲人：新东方学校主管 尹广吉</p><p>时 间：4月19日（周三）19:30-21:00</p><p>地 点：花江校区图书馆报告厅</p><p>&nbsp;</p><p>讲师介绍：</p><p>　　主授课程：四六级，考研英语，TOEFL，港大面试</p><p>　　香港城市大学硕士。现任新东方南宁学校 优能中学部主管，四六级、考研英语资深教师，对英语学习考试、考研、出国留学有深入研究，能专业指导学生英语学习，和规划考研生活。课程幽默励志，有效促进学生学习兴趣，为学生提供有效学习规划和职业规划。</p><p>&nbsp;</p><p>　　欢迎广大同学踊跃参加讲座!</p>');
INSERT INTO `article_content` VALUES ('4', '<span style=\"text-align: left;\">　　五月的花江校区，花叶繁茂，生机勃勃。5月3日下午，自治区党委书记、自治区人大常委会主任彭清华到校考察，亲切看望我校师生，勉励青年学子崇尚创新实干，陶冶高尚情操。</span><br style=\"text-align: left;\"><span style=\"text-align: left;\">　　自治区党委常委、宣传部长范晓莉，自治区党委常委、秘书长王可随同考察。桂林市委书记赵乐秦、桂林市市长周家斌，自治区高校工委书记、教育厅厅长莫诗浦陪同考察。</span><br style=\"text-align: left;\"><span style=\"text-align: left;\">　　当天，彭清华书记在我校党委书记周怀营、校长古天龙等的陪同下观看了我校花江校区整体规划，并先后来到我校科技楼和学生活动中心进行实地考察。</span><br style=\"text-align: left;\"><span style=\"text-align: left;\">　　在科技楼，彭清华书记观看了我校科研成果及应用展示，考察了“位置感知与位置服务技术工程实验室”、“省部共建教育部重点实验室——认知无线电与信息处理实验室”，现场了解了我校陈真诚教授团队“无创血糖”检测技术，听取了我校“国家杰青”罗笑南教授等专家的汇报，对我校取得科技与应用成果表示高度赞赏。</span><br style=\"text-align: left;\"><span style=\"text-align: left;\">　　在学生活动中心，彭清华书记参观、考察了我校“校友企业回桂，服务广西经济”成果展示、学生创新创业成果展和广西高校易班发展研究中心。去年在中国“互联网+”大学生创新创业大赛上斩获金奖的许涛同学现场为彭清华书记展示了获金奖的作品。彭书记饶有兴趣地走上前去“操作”起了这项“金奖”作品，并与许涛同学亲切交谈，了解作品的原理、结构和未来的应用前景等，当听说许涛同学已获得10项发明专利时，彭书记高兴地笑了。他勉励同学们要把创新与实干结合起来，掌握科学方法，陶冶高尚情操。努力将青春的梦想融入中国梦之中，脚踏实地，激扬青春，奋发有为。在广西高校易班发展研究中心，彭清华书记听取了我校学工部部长丰硕和政府数字传播与文化软实力研究中心负责人邓国峰教授的工作汇报，对我校创新网络思想政治工作，占领高校网络意识形态主阵地的做法和取得的成绩表示充分肯定。</span><br style=\"text-align: left;\"><span style=\"text-align: left;\">　　彭清华书记在考察中强调，要把总书记对广西教育事业的亲切关怀作为强大动力，把总书记重要讲话精神作为根本遵循，扎实做好高层次人才培养、大学生综合素质提升、科技成果转化、教育扶贫等工作，把更多成果落实到富民兴桂工作中。要进一步加强党对高校工作的领导，加强和改进高校党的建设和思想政治工作，认真落实意识形态工作责任，以优异成绩迎接党的十九大胜利召开。</span><br style=\"text-align: left;\"><span style=\"text-align: left;\">　　当天正值五四青年节即将到来之际，彭清华书记与在场青年学生热情握手，致以节日问候，并合影留念。一时间，广西高校易班发展研究中心大厅内欢声笑语，青春激扬的气息弥漫四周。</span><br style=\"text-align: left;\"><span style=\"text-align: left;\">　　校领导农毅、胡泽民、徐华蕊、钟平等参加了陪同及相关活动。</span><br style=\"text-align: left;\"><span style=\"text-align: left;\">　　（校党委宣传部　贺超/文　　农必东/摄）</span><br style=\"text-align: left;\"><p align=\"center\"><img alt=\"DSC_3802_副本.jpg\" src=\"http://szhxy.guet.edu.cn/qxgl/showimg.aspx?fn=2020015/201755180628847.jpg\"></p><p align=\"center\">自治区党委书记彭清华观看校区建设规划图&nbsp;</p><p align=\"center\"><img alt=\"DSC_3877_副本.jpg\" src=\"http://szhxy.guet.edu.cn/qxgl/showimg.aspx?fn=2020015/201755180729850.jpg\"></p><p align=\"center\">&nbsp;自治区党委书记彭清华考察我校教师科研成果</p><p align=\"center\"><img alt=\"DSC_4088_副本.jpg\" src=\"http://szhxy.guet.edu.cn/qxgl/showimg.aspx?fn=2020015/201755180820165.jpg\"></p><p align=\"center\">&nbsp;自治区党委书记彭清华考察大学生创新创业成果</p><p><br></p>');
INSERT INTO `article_content` VALUES ('5', '<p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>各单位：<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>根据《中华人民共和国国防法》、《中华人民共和国<span lang=\"EN-US\">2017</span>年征兵工作实施方案》的要求，我校征集新兵的工作现已展开。为做好相关报名工作，现将有关事宜通知如下：<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>一、征集对象：<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>我校全日制在校本科生<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>二、报名条件：<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span lang=\"EN-US\">1</span><span>、拥护党的路线方针政策，自愿前往部队工作；<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span lang=\"EN-US\">2</span><span>、年龄要求：男青年：普通高等学校在校生为<span lang=\"EN-US\">2017</span>年年满<span lang=\"EN-US\">17</span>－<span lang=\"EN-US\">22</span>周岁（<span lang=\"EN-US\">1995</span>年<span lang=\"EN-US\">1</span>月<span lang=\"EN-US\">1</span>日—<span lang=\"EN-US\">2000</span>年<span lang=\"EN-US\">12</span>月<span lang=\"EN-US\">31</span>日间出生），大专、本科及以上学历毕业生放宽至<span lang=\"EN-US\">24</span>周岁。女青年：普通高等学校在校生和应届毕业生为<span lang=\"EN-US\">17</span>－<span lang=\"EN-US\">22</span>周岁（<span lang=\"EN-US\">1995</span>年<span lang=\"EN-US\">1</span>月<span lang=\"EN-US\">1</span>日以后出生）。<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span lang=\"EN-US\">3</span><span>、身高：男青年为<span lang=\"EN-US\">160</span>厘米以上，女青年为<span lang=\"EN-US\">158</span>厘米以上。视力：陆勤人员右眼裸眼视力不低于<span lang=\"EN-US\">4.6</span>，左眼裸眼视力不低于<span lang=\"EN-US\">4.5</span>。体重：男青年，不超过标准体重的<span lang=\"EN-US\">30%</span>，不低于标准体重的<span lang=\"EN-US\">15%</span>；女青年，不超过标准体重的<span lang=\"EN-US\">20%</span>，不低于标准体重的<span lang=\"EN-US\">15%</span>（标准体重<span lang=\"EN-US\">kg</span>＝身高<span lang=\"EN-US\">cm</span>－<span lang=\"EN-US\">110</span>）。身体其它条件按照国防部<span lang=\"EN-US\">2016</span>年公布的《应征公民体格检查标准》和有关规定执行。<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span lang=\"EN-US\">4</span><span>、政治条件：应征公民必须热爱中国共产党，热爱社会主义祖国，热爱人民军队，遵纪守法，品德优良，决心为抵抗侵略、保卫祖国、保卫人民的和平劳动而英勇奋斗。其他政治条件，按照公安部、总参谋部、总政治部<span lang=\"EN-US\">2016</span>年公布的《征兵政治考核工作规定》和有关规定执行。<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>三、报名时间：<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>即日起至<span lang=\"EN-US\">6</span>月<span lang=\"EN-US\">10</span>日<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>四、报名方式：<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>在报名方法上，实行网上报名，可通过“全国征兵网”查询，提交报名信息。报名网址《全国征兵网》：<span lang=\"EN-US\">http://gfbzb.gov.cn</span>。<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>同时，直接在线自行打印《应征毕业生预征对象登记表》或《应征男青年网上报名登记表》、《学费代偿表》、《存根》。《应征毕业生预征对象登记表》或《应征男青年网上报名登记表》经所在院系审查并签署意见盖章后，交到学校武装部办公室（花江校区男生宿舍<span lang=\"EN-US\">A</span>区<span lang=\"EN-US\">3</span>栋<span lang=\"EN-US\">2</span>楼<span lang=\"EN-US\">105</span>室。<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>女青年<span lang=\"EN-US\">6</span>月<span lang=\"EN-US\">5</span>日<span lang=\"EN-US\">18</span>时起至<span lang=\"EN-US\">8</span>月<span lang=\"EN-US\">5</span>日前采取网上报名（网址：<span lang=\"EN-US\">http://gfbzb.gov.cn</span>）。登陆网上报名系统，并要通过教育部学生信息数据平台审核。报名结束后，网上系统将自动依据报名人员当年高考相对分数［高考相对分数<span lang=\"EN-US\">=</span>（<span lang=\"EN-US\">1-</span>当年高考成绩在本省的排名次序<span lang=\"EN-US\">/</span>当年本省的高考总人数）<span lang=\"EN-US\">*100</span>］，按照由高到低的顺序，择优选择<span lang=\"EN-US\">6</span>倍征集任务数的女青年作为初选预征对象。<span lang=\"EN-US\">8</span>月<span lang=\"EN-US\">6</span>日起，初选预征对象登录网上报名系统，自行打印《应征女青年网上报名审核表》，等待警备区电话通知。<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>五、入伍优待政策：（见附件<span lang=\"EN-US\">1</span>）<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span></span>&nbsp;</p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>武装部联系人和咨询电话：卢老师，<span lang=\"EN-US\">0773-2305625&nbsp;</span>。<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span lang=\"EN-US\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" align=\"left\" style=\"text-align: left;\"><span>　　　　　　　　　　　　　　　　　　　　　 中共桂林电子科技大学武装部<span lang=\"EN-US\"><o:p></o:p></span></span></p><p><span></span><span></span></p><p class=\"MsoNormal\" align=\"left\"><span lang=\"EN-US\">　　　　　　　　　　　　　　　　　　　　　　　　　2017</span><span>年<span lang=\"EN-US\">5</span>月<span lang=\"EN-US\">2</span>日</span></p>');
INSERT INTO `article_content` VALUES ('6', '<p class=\"MsoNormal\" align=\"left\"><span>桂林电子科技大学</span><span>现对金鸡岭校区锅炉房两台<span lang=\"EN-US\">1996</span>年购置，型号为<span lang=\"EN-US\">WNS1-1.0-Y</span>卧式内燃三回程湿背式燃油锅炉</span><span>进行报废处置</span><span>。</span><span>本着“公平、公正、公开”的原则，本次处置采取现场核看实物和竞价方式进行，欢迎有意向的单位参与竞价。</span><span lang=\"EN-US\"><o:p></o:p></span></p><p class=\"MsoNormal\" align=\"left\"><b><span>一、公告时间：</span></b><st1:chsdate w:st=\"on\" isrocdate=\"False\" islunardate=\"False\" day=\"11\" month=\"5\" year=\"2017\"><span lang=\"EN-US\">2017</span><span>年<span lang=\"EN-US\">5</span>月<span lang=\"EN-US\">11</span>日</span></st1:chsdate><span lang=\"EN-US\">-<st1:chsdate w:st=\"on\" isrocdate=\"False\" islunardate=\"False\" day=\"14\" month=\"5\" year=\"2017\">2017<span lang=\"EN-US\"><span lang=\"EN-US\">年5</span></span><span lang=\"EN-US\"><span lang=\"EN-US\">月14</span></span><span lang=\"EN-US\"><span lang=\"EN-US\">日</span></span></st1:chsdate></span><span lang=\"EN-US\"><o:p></o:p></span></p><p class=\"MsoNormal\" align=\"left\"><b><span>二、报价时间：</span></b><st1:chsdate w:st=\"on\" isrocdate=\"False\" islunardate=\"False\" day=\"15\" month=\"5\" year=\"2017\"><span lang=\"EN-US\">2017</span><span>年<span lang=\"EN-US\">5</span>月<span lang=\"EN-US\">15</span>日上午</span></st1:chsdate><span lang=\"EN-US\">9</span><span>：<span lang=\"EN-US\">00</span></span><span lang=\"EN-US\"><o:p></o:p></span></p><p class=\"MsoNormal\" align=\"left\"><b><span>三、报价地点：</span></b><span>金鸡岭校区</span><span>锅炉房（枫华餐厅旁</span><span>）</span><span lang=\"EN-US\"><o:p></o:p></span></p><p class=\"MsoNormal\" align=\"left\"><b><span>四、联系电话：</span></b><span lang=\"EN-US\">0773-2290676</span><span lang=\"EN-US\"><o:p></o:p></span></p><p class=\"MsoNormal\" align=\"left\"><b><span lang=\"EN-US\">&nbsp;</span></b></p><p class=\"MsoNormal\" align=\"left\"><span>　　　　　　　　　　　　　　　　　　　　　　　　桂林电子科技大学</span><span lang=\"EN-US\"><o:p></o:p></span></p><p class=\"MsoNormal\" align=\"left\"><span lang=\"EN-US\">　　　　　　　　　　　　　　　　　　　　　　　　　2017</span><span>年<span lang=\"EN-US\">5</span>月<span lang=\"EN-US\">11</span>日<span lang=\"EN-US\"><o:p></o:p></span></span></p><p class=\"MsoNormal\" align=\"left\"><span lang=\"EN-US\">&nbsp;</span></p><p class=\"MsoNormal\" align=\"left\"><b><span>附件：</span></b><b><span lang=\"EN-US\"><o:p></o:p></span></b></p><p class=\"MsoNormal\" align=\"center\" style=\"text-align: center;\"><img src=\"http://szhxy.guet.edu.cn/qxgl/showimg.aspx?fn=cj09/2017511145307335.jpg\"></p>');

-- ----------------------------
-- Table structure for article_type
-- ----------------------------
DROP TABLE IF EXISTS `article_type`;
CREATE TABLE `article_type` (
  `article_type_id` tinyint(4) NOT NULL COMMENT '文章类型ID',
  `type_name` varchar(20) DEFAULT '' COMMENT '文章类型名称',
  PRIMARY KEY (`article_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article_type
-- ----------------------------
INSERT INTO `article_type` VALUES ('1', '新闻');
INSERT INTO `article_type` VALUES ('2', '通知公告');

-- ----------------------------
-- Table structure for contract_type
-- ----------------------------
DROP TABLE IF EXISTS `contract_type`;
CREATE TABLE `contract_type` (
  `contract_type_id` tinyint(4) NOT NULL COMMENT '合同类型ID',
  `contract_type_name` varchar(20) DEFAULT '' COMMENT '合同类型名称',
  `comments` varchar(100) DEFAULT '' COMMENT '说明',
  PRIMARY KEY (`contract_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contract_type
-- ----------------------------
INSERT INTO `contract_type` VALUES ('1', '转包', '');
INSERT INTO `contract_type` VALUES ('2', '出租', '');
INSERT INTO `contract_type` VALUES ('3', '借用', '');
INSERT INTO `contract_type` VALUES ('4', '入股', '');
INSERT INTO `contract_type` VALUES ('5', '转让', '');
INSERT INTO `contract_type` VALUES ('6', '互换', '');

-- ----------------------------
-- Table structure for crop_care
-- ----------------------------
DROP TABLE IF EXISTS `crop_care`;
CREATE TABLE `crop_care` (
  `crop_care_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '作物护理编号',
  `land_id` int(11) DEFAULT NULL COMMENT '产地编号',
  `batch` int(11) DEFAULT '0' COMMENT '批次',
  `principal_name` varchar(30) DEFAULT NULL COMMENT '负责人',
  `germchit` varchar(30) DEFAULT '' COMMENT '种苗',
  `count` int(11) DEFAULT '0' COMMENT '数量',
  `fertilizer` varchar(255) DEFAULT '' COMMENT '肥（饲）料',
  `drug` varchar(255) DEFAULT '' COMMENT '药品',
  `crop_time` timestamp NULL DEFAULT NULL COMMENT '护理时间',
  PRIMARY KEY (`crop_care_id`),
  KEY `FK8y07vp8fbj3j3e1vd3ehw71am` (`land_id`),
  CONSTRAINT `FK8y07vp8fbj3j3e1vd3ehw71am` FOREIGN KEY (`land_id`) REFERENCES `land` (`land_id`),
  CONSTRAINT `crop_care_ibfk_1` FOREIGN KEY (`land_id`) REFERENCES `land` (`land_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crop_care
-- ----------------------------
INSERT INTO `crop_care` VALUES ('1', '1', '1', '乔丹', '种苗1号', '12', '肥料1号', '敌敌畏', '2017-05-04 00:00:00');

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '客户编号',
  `customer_name` varchar(40) DEFAULT NULL COMMENT '客户名称',
  `principal_name` varchar(20) DEFAULT NULL COMMENT '负责人姓名',
  `mobile` varchar(15) DEFAULT NULL COMMENT '联系方式',
  `address` varchar(50) DEFAULT '' COMMENT '住址',
  `comments` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES ('1', '马化腾', '展昭', '13847582475', '寒虚宫', '真的');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `department_id` tinyint(4) NOT NULL COMMENT '部门ID',
  `department_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `comments` varchar(50) DEFAULT '' COMMENT 'comments',
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '办公室', '人事、行政');
INSERT INTO `department` VALUES ('2', '生产部', '农资购买、生产');
INSERT INTO `department` VALUES ('3', '销售部', '市场开发、销售、物流');

-- ----------------------------
-- Table structure for land
-- ----------------------------
DROP TABLE IF EXISTS `land`;
CREATE TABLE `land` (
  `land_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '地点编号',
  `land_name` varchar(40) DEFAULT '' COMMENT '地点名称',
  `area` double NOT NULL DEFAULT '0' COMMENT '面积',
  `land_type_id` tinyint(4) DEFAULT '0' COMMENT '土地类型:1种植 2养殖',
  `total_price` double DEFAULT '0' COMMENT '总价',
  `contract_id` int(11) DEFAULT '0' COMMENT '合同编号',
  `contract_type_id` tinyint(4) DEFAULT '0' COMMENT '合同类型',
  `insure_start_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '签订日期',
  `deadline` double DEFAULT '0' COMMENT '期限',
  `real_total_price` double DEFAULT '0' COMMENT '实际总价',
  `transferor` varchar(20) DEFAULT '' COMMENT '转让方',
  `assignee` varchar(20) DEFAULT '' COMMENT '受让方',
  `mobile` varchar(15) DEFAULT '' COMMENT '联系电话',
  `comments` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`land_id`),
  KEY `land_ibfk_1` (`land_type_id`),
  KEY `land_ibfk_2` (`contract_type_id`),
  CONSTRAINT `FK8fxscu8wdn6w6uir0tbrmhp67` FOREIGN KEY (`contract_type_id`) REFERENCES `contract_type` (`contract_type_id`),
  CONSTRAINT `FKdpn5mixiidybp38revqne3ltw` FOREIGN KEY (`land_type_id`) REFERENCES `land_type` (`land_type_id`),
  CONSTRAINT `land_ibfk_1` FOREIGN KEY (`land_type_id`) REFERENCES `land_type` (`land_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `land_ibfk_2` FOREIGN KEY (`contract_type_id`) REFERENCES `contract_type` (`contract_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of land
-- ----------------------------
INSERT INTO `land` VALUES ('1', '陈桥村东二区', '3228', '1', '235425', '2353465', '2', '2017-05-03 00:00:00', '32', '235425', '王宝强', '马蓉', '13838472194', '');
INSERT INTO `land` VALUES ('2', '陈桥村东三区', '5467', '2', '235425', '23534656', '3', '2017-06-02 00:00:00', '24', '455425', '陈羽凡', '白百合', '13538472167', '');

-- ----------------------------
-- Table structure for land_type
-- ----------------------------
DROP TABLE IF EXISTS `land_type`;
CREATE TABLE `land_type` (
  `land_type_id` tinyint(4) NOT NULL COMMENT '土地类型ID',
  `land_type_name` varchar(30) DEFAULT '' COMMENT '土地类型名称',
  `comments` varchar(100) DEFAULT '' COMMENT '说明',
  PRIMARY KEY (`land_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of land_type
-- ----------------------------
INSERT INTO `land_type` VALUES ('1', '种植业', '');
INSERT INTO `land_type` VALUES ('2', '畜牧业', '');
INSERT INTO `land_type` VALUES ('3', '林业', '');
INSERT INTO `land_type` VALUES ('4', '水产业', '');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `customer_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `variety` varchar(20) DEFAULT NULL COMMENT '收购品种',
  `count` int(11) DEFAULT NULL COMMENT '收购量',
  `real_price` int(10) DEFAULT NULL COMMENT '总价',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '订单创建时间',
  PRIMARY KEY (`orders_id`),
  KEY `FKhwn935tudm12n4ihi91mnm0w5` (`customer_id`),
  CONSTRAINT `FKhwn935tudm12n4ihi91mnm0w5` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '1', '亡者农药', '1000', '8999991', '2017-05-11 00:00:00');

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `position_id` tinyint(4) NOT NULL COMMENT '职位ID',
  `position_name` varchar(30) DEFAULT '' COMMENT '职位名称',
  `menu_json` text COMMENT '菜单json数据',
  `comments` varchar(100) DEFAULT '' COMMENT '说明',
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of position
-- ----------------------------
INSERT INTO `position` VALUES ('0', '超级管理员', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 1,\r\n        \"url\": \"\",\r\n        \"name\": \"办公室\",\r\n        \"iconfont\": \"&#xe659;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 2,\r\n            \"url\": \"\",\r\n            \"name\": \"人事\",\r\n            \"iconfont\": \"&#xe630;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 3,\r\n                \"url\": \"/admin/society/userAdd\",\r\n                \"name\": \"社员信息录入\",\r\n                \"iconfont\": \"&#xe674;\"\r\n              },\r\n              {\r\n                \"id\": 4,\r\n                \"url\": \"/admin/society/userList\",\r\n                \"name\": \"社员列表\",\r\n                \"iconfont\": \"&#xe672;\"\r\n              },\r\n              {\r\n                \"id\": 5,\r\n                \"url\": \"/admin/system/userAdd\",\r\n                \"name\": \"系统用户信息录入\",\r\n                \"iconfont\": \"&#xe608;\"\r\n              },\r\n              {\r\n                \"id\": 6,\r\n                \"url\": \"/admin/system/userList\",\r\n                \"name\": \"系统用户列表\",\r\n                \"iconfont\": \"&#xe681;\"\r\n              },\r\n              {\r\n                \"id\": 7,\r\n                \"url\": \"/admin/society/relativeAdd\",\r\n                \"name\": \"农户家庭成员录入\",\r\n                \"iconfont\": \"&#xe623;\"\r\n              },\r\n              {\r\n                \"id\": 8,\r\n                \"url\": \"/admin/society/relativeList\",\r\n                \"name\": \"农户家庭成员列表\",\r\n                \"iconfont\": \"&#xe630;\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"id\": 9,\r\n            \"url\": \"\",\r\n            \"name\": \"行政\",\r\n            \"iconfont\": \"&#xe611;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 10,\r\n                \"url\": \"/admin/article/initAddArticle\",\r\n                \"name\": \"添加文章\",\r\n                \"iconfont\": \"&#xe653;\"\r\n              },\r\n              {\r\n                \"id\": 11,\r\n                \"url\": \"/admin/article/initArticleList\",\r\n                \"name\": \"文章列表\",\r\n                \"iconfont\": \"&#xe6bc;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      },\r\n      {\r\n        \"id\": 12,\r\n        \"url\": \"\",\r\n        \"name\": \"生产部\",\r\n        \"iconfont\": \"&#xe696;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 13,\r\n            \"url\": \"\",\r\n            \"name\": \"土地管理\",\r\n            \"iconfont\": \"&#xe60a;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 14,\r\n                \"url\": \"/admin/production/landAdd\",\r\n                \"name\": \"土地信息录入\",\r\n                \"iconfont\": \"&#xe606;\"\r\n              },\r\n              {\r\n                \"id\": 15,\r\n                \"url\": \"/admin/production/landList\",\r\n                \"name\": \"土地资源列表\",\r\n                \"iconfont\": \"&#xe61a;\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"id\": 16,\r\n            \"url\": \"\",\r\n            \"name\": \"生产\",\r\n            \"iconfont\": \"&#xe612;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 17,\r\n                \"url\": \"/admin/production/producerAdd\",\r\n                \"name\": \"生产信息录入\",\r\n                \"iconfont\": \"&#xe61c;\"\r\n              },\r\n              {\r\n                \"id\": 18,\r\n                \"url\": \"/admin/production/producerList\",\r\n                \"name\": \"生产信息列表\",\r\n                \"iconfont\": \"&#xe622;\"\r\n              },\r\n              {\r\n                \"id\": 24,\r\n                \"url\": \"/admin/production/statisticsPage\",\r\n                \"name\": \"数据统计\",\r\n                \"iconfont\": \"&#xe669;\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"id\": 19,\r\n            \"url\": \"\",\r\n            \"name\": \"护理\",\r\n            \"iconfont\": \"&#xe64c;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 20,\r\n                \"url\": \"/admin/production/cropCareAdd\",\r\n                \"name\": \"添加施肥信息\",\r\n                \"iconfont\": \"&#xe628;\"\r\n              },\r\n              {\r\n                \"id\": 21,\r\n                \"url\": \"/admin/production/cropCareList\",\r\n                \"name\": \"作物施肥记录\",\r\n                \"iconfont\": \"&#xe634;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      },\r\n      {\r\n        \"id\": 22,\r\n        \"url\": \"\",\r\n        \"name\": \"销售部\",\r\n        \"iconfont\": \"&#xe631\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 23,\r\n            \"url\": \"\",\r\n            \"name\": \"业务员\",\r\n            \"iconfont\": \"&#xe612;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 24,\r\n                \"url\": \"/admin/sale/customerAdd\",\r\n                \"name\": \"客户信息录入\",\r\n                \"iconfont\": \"&#xe669;\"\r\n              },\r\n              {\r\n                \"id\": 25,\r\n                \"url\": \"/admin/sale/customerList\",\r\n                \"name\": \"客户列表\",\r\n                \"iconfont\": \"&#xe635;\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"id\": 26,\r\n            \"url\": \"\",\r\n            \"name\": \"销售员\",\r\n            \"iconfont\": \"&#xe74d;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 27,\r\n                \"url\": \"/admin/sale/ordersAdd\",\r\n                \"name\": \"订单信息录入\",\r\n                \"iconfont\": \"&#xe649;\"\r\n              },\r\n              {\r\n                \"id\": 28,\r\n                \"url\": \"/admin/sale/ordersList\",\r\n                \"name\": \"订单列表\",\r\n                \"iconfont\": \"&#xe67a;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '拥有后台管理所有权限');
INSERT INTO `position` VALUES ('10', '办公室经理', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 1,\r\n        \"url\": \"\",\r\n        \"name\": \"办公室\",\r\n        \"iconfont\": \"&#xe659;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 2,\r\n            \"url\": \"\",\r\n            \"name\": \"人事\",\r\n            \"iconfont\": \"&#xe630;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 3,\r\n                \"url\": \"/admin/society/userAdd\",\r\n                \"name\": \"社员信息录入\",\r\n                \"iconfont\": \"&#xe674;\"\r\n              },\r\n              {\r\n                \"id\": 4,\r\n                \"url\": \"/admin/society/userList\",\r\n                \"name\": \"社员列表\",\r\n                \"iconfont\": \"&#xe672;\"\r\n              },\r\n              {\r\n                \"id\": 5,\r\n                \"url\": \"/admin/system/userAdd\",\r\n                \"name\": \"系统用户信息录入\",\r\n                \"iconfont\": \"&#xe608;\"\r\n              },\r\n              {\r\n                \"id\": 6,\r\n                \"url\": \"/admin/system/userList\",\r\n                \"name\": \"系统用户列表\",\r\n                \"iconfont\": \"&#xe681;\"\r\n              },\r\n              {\r\n                \"id\": 7,\r\n                \"url\": \"/admin/society/relativeAdd\",\r\n                \"name\": \"农户家庭成员录入\",\r\n                \"iconfont\": \"&#xe623;\"\r\n              },\r\n              {\r\n                \"id\": 8,\r\n                \"url\": \"/admin/society/relativeList\",\r\n                \"name\": \"农户家庭成员列表\",\r\n                \"iconfont\": \"&#xe630;\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"id\": 9,\r\n            \"url\": \"\",\r\n            \"name\": \"行政\",\r\n            \"iconfont\": \"&#xe611;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 10,\r\n                \"url\": \"/admin/article/initAddArticle\",\r\n                \"name\": \"添加文章\",\r\n                \"iconfont\": \"&#xe653;\"\r\n              },\r\n              {\r\n                \"id\": 11,\r\n                \"url\": \"/admin/article/initArticleList\",\r\n                \"name\": \"文章列表\",\r\n                \"iconfont\": \"&#xe6bc;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '部门内所有权限');
INSERT INTO `position` VALUES ('11', '人事', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 1,\r\n        \"url\": \"\",\r\n        \"name\": \"办公室\",\r\n        \"iconfont\": \"&#xe659;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 2,\r\n            \"url\": \"\",\r\n            \"name\": \"人事\",\r\n            \"iconfont\": \"&#xe630;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 3,\r\n                \"url\": \"/admin/society/userAdd\",\r\n                \"name\": \"社员信息录入\",\r\n                \"iconfont\": \"&#xe674;\"\r\n              },\r\n              {\r\n                \"id\": 4,\r\n                \"url\": \"/admin/society/userList\",\r\n                \"name\": \"社员列表\",\r\n                \"iconfont\": \"&#xe672;\"\r\n              },\r\n              {\r\n                \"id\": 5,\r\n                \"url\": \"/admin/system/userAdd\",\r\n                \"name\": \"系统用户信息录入\",\r\n                \"iconfont\": \"&#xe608;\"\r\n              },\r\n              {\r\n                \"id\": 6,\r\n                \"url\": \"/admin/system/userList\",\r\n                \"name\": \"系统用户列表\",\r\n                \"iconfont\": \"&#xe681;\"\r\n              },\r\n              {\r\n                \"id\": 7,\r\n                \"url\": \"/admin/society/relativeAdd\",\r\n                \"name\": \"农户家庭成员录入\",\r\n                \"iconfont\": \"&#xe623;\"\r\n              },\r\n              {\r\n                \"id\": 8,\r\n                \"url\": \"/admin/society/relativeList\",\r\n                \"name\": \"农户家庭成员列表\",\r\n                \"iconfont\": \"&#xe630;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '社员信息的录入');
INSERT INTO `position` VALUES ('12', '行政', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 1,\r\n        \"url\": \"\",\r\n        \"name\": \"办公室\",\r\n        \"iconfont\": \"&#xe659;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 9,\r\n            \"url\": \"\",\r\n            \"name\": \"行政\",\r\n            \"iconfont\": \"&#xe611;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 10,\r\n                \"url\": \"/admin/article/initAddArticle\",\r\n                \"name\": \"添加文章\",\r\n                \"iconfont\": \"&#xe653;\"\r\n              },\r\n              {\r\n                \"id\": 11,\r\n                \"url\": \"/admin/article/initArticleList\",\r\n                \"name\": \"文章列表\",\r\n                \"iconfont\": \"&#xe6bc;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '新闻通知的发布');
INSERT INTO `position` VALUES ('20', '生产部经理', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 12,\r\n        \"url\": \"\",\r\n        \"name\": \"生产部\",\r\n        \"iconfont\": \"&#xe696;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 13,\r\n            \"url\": \"\",\r\n            \"name\": \"土地管理\",\r\n            \"iconfont\": \"&#xe60a;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 14,\r\n                \"url\": \"/admin/production/landAdd\",\r\n                \"name\": \"土地信息录入\",\r\n                \"iconfont\": \"&#xe606;\"\r\n              },\r\n              {\r\n                \"id\": 15,\r\n                \"url\": \"/admin/production/landList\",\r\n                \"name\": \"土地资源列表\",\r\n                \"iconfont\": \"&#xe61a;\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"id\": 16,\r\n            \"url\": \"\",\r\n            \"name\": \"生产\",\r\n            \"iconfont\": \"&#xe612;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 17,\r\n                \"url\": \"/admin/production/producerAdd\",\r\n                \"name\": \"生产信息录入\",\r\n                \"iconfont\": \"&#xe61c;\"\r\n              },\r\n              {\r\n                \"id\": 18,\r\n                \"url\": \"/admin/production/producerList\",\r\n                \"name\": \"生产信息列表\",\r\n                \"iconfont\": \"&#xe622;\"\r\n              },\r\n              {\r\n                \"id\": 24,\r\n                \"url\": \"/admin/production/statisticsPage\",\r\n                \"name\": \"数据统计\",\r\n                \"iconfont\": \"&#xe669;\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"id\": 19,\r\n            \"url\": \"\",\r\n            \"name\": \"护理\",\r\n            \"iconfont\": \"&#xe64c;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 20,\r\n                \"url\": \"/admin/production/cropCareAdd\",\r\n                \"name\": \"添加施肥信息\",\r\n                \"iconfont\": \"&#xe628;\"\r\n              },\r\n              {\r\n                \"id\": 21,\r\n                \"url\": \"/admin/production/cropCareList\",\r\n                \"name\": \"作物施肥记录\",\r\n                \"iconfont\": \"&#xe634;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '生产部所有权限');
INSERT INTO `position` VALUES ('21', '土地管理', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 12,\r\n        \"url\": \"\",\r\n        \"name\": \"生产部\",\r\n        \"iconfont\": \"&#xe696;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 13,\r\n            \"url\": \"\",\r\n            \"name\": \"土地管理\",\r\n            \"iconfont\": \"&#xe60a;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 14,\r\n                \"url\": \"/admin/production/landAdd\",\r\n                \"name\": \"土地信息录入\",\r\n                \"iconfont\": \"&#xe606;\"\r\n              },\r\n              {\r\n                \"id\": 15,\r\n                \"url\": \"/admin/production/landList\",\r\n                \"name\": \"土地资源列表\",\r\n                \"iconfont\": \"&#xe61a;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '土地信息的录入');
INSERT INTO `position` VALUES ('22', '生产', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 12,\r\n        \"url\": \"\",\r\n        \"name\": \"生产部\",\r\n        \"iconfont\": \"&#xe696;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 16,\r\n            \"url\": \"\",\r\n            \"name\": \"生产\",\r\n            \"iconfont\": \"&#xe612;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 17,\r\n                \"url\": \"/admin/production/producerAdd\",\r\n                \"name\": \"生产信息录入\",\r\n                \"iconfont\": \"&#xe61c;\"\r\n              },\r\n              {\r\n                \"id\": 18,\r\n                \"url\": \"/admin/production/producerList\",\r\n                \"name\": \"生产信息列表\",\r\n                \"iconfont\": \"&#xe622;\"\r\n              },\r\n              {\r\n                \"id\": 24,\r\n                \"url\": \"/admin/production/statisticsPage\",\r\n                \"name\": \"数据统计\",\r\n                \"iconfont\": \"&#xe669;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '计划及实际生产信息的录入');
INSERT INTO `position` VALUES ('23', '护理', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 12,\r\n        \"url\": \"\",\r\n        \"name\": \"生产部\",\r\n        \"iconfont\": \"&#xe696;\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 19,\r\n            \"url\": \"\",\r\n            \"name\": \"护理\",\r\n            \"iconfont\": \"&#xe64c;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 20,\r\n                \"url\": \"/admin/production/cropCareAdd\",\r\n                \"name\": \"添加施肥信息\",\r\n                \"iconfont\": \"&#xe628;\"\r\n              },\r\n              {\r\n                \"id\": 21,\r\n                \"url\": \"/admin/production/cropCareList\",\r\n                \"name\": \"作物施肥记录\",\r\n                \"iconfont\": \"&#xe634;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '作物的施肥');
INSERT INTO `position` VALUES ('30', '销售部经理', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 22,\r\n        \"url\": \"\",\r\n        \"name\": \"销售部\",\r\n        \"iconfont\": \"&#xe631\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 23,\r\n            \"url\": \"\",\r\n            \"name\": \"业务员\",\r\n            \"iconfont\": \"&#xe612;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 24,\r\n                \"url\": \"/admin/sale/customerAdd\",\r\n                \"name\": \"客户信息录入\",\r\n                \"iconfont\": \"&#xe669;\"\r\n              },\r\n              {\r\n                \"id\": 25,\r\n                \"url\": \"/admin/sale/customerList\",\r\n                \"name\": \"客户列表\",\r\n                \"iconfont\": \"&#xe635;\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"id\": 26,\r\n            \"url\": \"\",\r\n            \"name\": \"销售员\",\r\n            \"iconfont\": \"&#xe74d;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 27,\r\n                \"url\": \"/admin/sale/ordersAdd\",\r\n                \"name\": \"订单信息录入\",\r\n                \"iconfont\": \"&#xe649;\"\r\n              },\r\n              {\r\n                \"id\": 28,\r\n                \"url\": \"/admin/sale/ordersList\",\r\n                \"name\": \"订单列表\",\r\n                \"iconfont\": \"&#xe67a;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '销售部所有权限');
INSERT INTO `position` VALUES ('31', '业务员', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 22,\r\n        \"url\": \"\",\r\n        \"name\": \"销售部\",\r\n        \"iconfont\": \"&#xe631\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 23,\r\n            \"url\": \"\",\r\n            \"name\": \"业务员\",\r\n            \"iconfont\": \"&#xe612;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 24,\r\n                \"url\": \"/admin/sale/customerAdd\",\r\n                \"name\": \"客户信息录入\",\r\n                \"iconfont\": \"&#xe669;\"\r\n              },\r\n              {\r\n                \"id\": 25,\r\n                \"url\": \"/admin/sale/customerList\",\r\n                \"name\": \"客户列表\",\r\n                \"iconfont\": \"&#xe635;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '客户信息的录入');
INSERT INTO `position` VALUES ('32', '销售员', '{\r\n  \"data\": {\r\n    \"list\": [\r\n      {\r\n        \"id\": 22,\r\n        \"url\": \"\",\r\n        \"name\": \"销售部\",\r\n        \"iconfont\": \"&#xe631\",\r\n        \"sub\": [\r\n          {\r\n            \"id\": 26,\r\n            \"url\": \"\",\r\n            \"name\": \"销售员\",\r\n            \"iconfont\": \"&#xe74d;\",\r\n            \"sub\": [\r\n              {\r\n                \"id\": 27,\r\n                \"url\": \"/admin/sale/ordersAdd\",\r\n                \"name\": \"订单信息录入\",\r\n                \"iconfont\": \"&#xe649;\"\r\n              },\r\n              {\r\n                \"id\": 28,\r\n                \"url\": \"/admin/sale/ordersList\",\r\n                \"name\": \"订单列表\",\r\n                \"iconfont\": \"&#xe67a;\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ]\r\n  },\r\n  \"status\": 200\r\n}', '订单信息的录入');

-- ----------------------------
-- Table structure for producer
-- ----------------------------
DROP TABLE IF EXISTS `producer`;
CREATE TABLE `producer` (
  `producer_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '产地编号',
  `producer_name` varchar(50) DEFAULT '' COMMENT '产地名称',
  `area` double DEFAULT '0' COMMENT '使用面积',
  `producer_type` varchar(20) DEFAULT '' COMMENT '生产类型:1种植 2养殖 3水产 4林业',
  `variety_name` varchar(20) DEFAULT '' COMMENT '品种',
  `batch_duration` int(11) DEFAULT '0' COMMENT '批次时长',
  `principal_name` varchar(20) NOT NULL DEFAULT '' COMMENT '负责人',
  `batch_id` int(11) DEFAULT '0' COMMENT '批次ID',
  `planned_yield` int(11) DEFAULT '0' COMMENT '预计年产量',
  `planned_price` double DEFAULT '0' COMMENT '预估单价',
  `planned_total_price` double DEFAULT '0' COMMENT '预估总价',
  `start_time` timestamp NULL DEFAULT NULL COMMENT '开始生产时间',
  `is_produced` tinyint(4) DEFAULT '0' COMMENT '是否完成生产',
  `real_yield` int(11) DEFAULT '0' COMMENT '实际产量',
  `real_price` double DEFAULT '0' COMMENT '实际单价',
  `real_total_price` double DEFAULT '0' COMMENT '实际总价',
  `end_time` timestamp NULL DEFAULT NULL COMMENT '结束生产时间',
  `comments` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`producer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of producer
-- ----------------------------
INSERT INTO `producer` VALUES ('1', '陈桥村东二区', '1000', '种植', '土豆', '365', '陈玉', '1', '8999', '45', '34564', '2012-04-06 00:00:00', '1', '8999', '45', '44566', '2012-11-16 00:00:00', '');
INSERT INTO `producer` VALUES ('2', '陈桥村东二区', '1200', '种植', '土豆', '200', '陈玉', '2', '5799', '45', '29564', '2013-05-11 00:00:00', '1', '6899', '51', '14566', '2013-07-11 00:00:00', '');
INSERT INTO `producer` VALUES ('3', '陈桥村东三区', '2000', '畜牧', '鸡', '300', '陈宏', '1', '857', '50', '89546', '2012-04-06 00:00:00', '1', '900', '50', '95476', '2012-11-16 00:00:00', '');

-- ----------------------------
-- Table structure for relative
-- ----------------------------
DROP TABLE IF EXISTS `relative`;
CREATE TABLE `relative` (
  `relative_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '家庭成员ID',
  `user_id` int(11) DEFAULT NULL COMMENT '社员ID',
  `relative_name` varchar(20) DEFAULT '' COMMENT '家庭成员姓名',
  `sex` char(2) DEFAULT '' COMMENT '性别',
  `relationship` varchar(15) DEFAULT '' COMMENT '与户主的关系',
  `nation` varchar(20) DEFAULT '' COMMENT '民族',
  `mobile` varchar(15) DEFAULT '' COMMENT '联系电话',
  `email` varchar(30) DEFAULT '' COMMENT '邮箱',
  `address` varchar(50) DEFAULT '' COMMENT '地址',
  `comments` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`relative_id`),
  KEY `FK8bg5fqr6q01rjbk802qmle9se` (`user_id`),
  CONSTRAINT `FK8bg5fqr6q01rjbk802qmle9se` FOREIGN KEY (`user_id`) REFERENCES `society_user` (`user_id`),
  CONSTRAINT `relative_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `society_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of relative
-- ----------------------------
INSERT INTO `relative` VALUES ('1', '3', '二狗', '男', '父子', '汉', '15834542334', 'ewrgeh@qq.com', '北京', '');
INSERT INTO `relative` VALUES ('2', '4', '傩送', '男', '丈夫', '壮', '13366676788', '2825614566@qq.com', '广西来宾', '');
INSERT INTO `relative` VALUES ('3', '4', '茂银', '男', '儿子', '瑶', '18607737265', '18607737265@qq.com', '湖南凤凰', '');

-- ----------------------------
-- Table structure for society_role
-- ----------------------------
DROP TABLE IF EXISTS `society_role`;
CREATE TABLE `society_role` (
  `role_type_id` tinyint(11) NOT NULL COMMENT '角色类型ID',
  `role_name` varchar(20) DEFAULT '' COMMENT '角色名称',
  PRIMARY KEY (`role_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of society_role
-- ----------------------------
INSERT INTO `society_role` VALUES ('1', '个人');
INSERT INTO `society_role` VALUES ('2', '农户');
INSERT INTO `society_role` VALUES ('3', '企业');

-- ----------------------------
-- Table structure for society_user
-- ----------------------------
DROP TABLE IF EXISTS `society_user`;
CREATE TABLE `society_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '社员ID',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `role_type_id` tinyint(4) DEFAULT '0' COMMENT '社员性质：1:个人 2:农户 3:企业',
  `identity_card` varchar(20) NOT NULL DEFAULT '' COMMENT '身份证',
  `sex` char(2) DEFAULT '' COMMENT '性别',
  `nation` varchar(10) DEFAULT '' COMMENT '民族',
  `mobile` varchar(15) DEFAULT '' COMMENT '手机号',
  `email` varchar(30) DEFAULT '' COMMENT '邮箱',
  `address` varchar(50) DEFAULT '' COMMENT '住址',
  `join_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '入社日期',
  `insure_end_date` timestamp NULL DEFAULT NULL COMMENT '合同到期日期',
  `comments` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`user_id`),
  KEY `FKn8ilirhnw7q4899d6mj6i4lgs` (`role_type_id`),
  CONSTRAINT `FKn8ilirhnw7q4899d6mj6i4lgs` FOREIGN KEY (`role_type_id`) REFERENCES `society_role` (`role_type_id`),
  CONSTRAINT `society_user_ibfk_1` FOREIGN KEY (`role_type_id`) REFERENCES `society_role` (`role_type_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of society_user
-- ----------------------------
INSERT INTO `society_user` VALUES ('1', '超级管理员', '1', '450321199101225317', '男', '汉', '18378398226', 'admin@admin.com', '桂林电子科技大学花江校区', '2017-05-01 00:00:00', '2017-05-01 22:27:53', '超级管理员');
INSERT INTO `society_user` VALUES ('2', '马云', '1', '530623198002164656', '男', '汉', '15295813114', 'mayun@alibaba.com', '浙江杭州', '2017-05-02 00:00:00', '2017-05-18 00:00:00', '');
INSERT INTO `society_user` VALUES ('3', '傻根', '2', '530623198002164656', '男', '汉', '15834542334', 'wregwe@qq.com', '北京保定', '2017-05-02 00:00:00', '2017-05-02 00:00:00', '');
INSERT INTO `society_user` VALUES ('4', '翠翠', '2', '452226199411170000', '女', '壮', '18607737265', '1979859266@qq.com', '湖南凤凰', '2017-05-03 00:00:00', '2020-05-03 00:00:00', '');
INSERT INTO `society_user` VALUES ('5', '京东', '3', '110105197402140000', '男', '汉', '18567213490', 'jdmail@jd.com', '北京朝阳', '2017-05-03 00:00:00', '2019-05-01 00:00:00', 'haoha');
INSERT INTO `society_user` VALUES ('6', '积蕴', '1', '450303198712079278', '男', '瑶', '13683749873', '13683749873@qq.com', '中华大地', '2017-05-04 00:00:00', '2017-05-04 00:00:00', '');
INSERT INTO `society_user` VALUES ('7', '马画藤', '3', '450321199101223317', '男', '汉', '13596828474', 'tenten@ten.com', '深圳', '2017-05-14 00:00:00', '2017-05-13 00:00:00', '');

-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `login_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(64) NOT NULL COMMENT '密码',
  `department_id` tinyint(4) DEFAULT '0' COMMENT '部门 1:办公室 2:生产部 3:销售部',
  `position_id` tinyint(4) DEFAULT '0' COMMENT '职位 11:人事 12:行政 21:土地管理 22生产 23护理 31业务员 32销售原33',
  `disabled` tinyint(4) DEFAULT '0' COMMENT '是否禁用',
  `level` tinyint(4) DEFAULT '0' COMMENT '用户级别',
  PRIMARY KEY (`user_id`),
  KEY `FKexswov2efs6ea88m4khpbn0lx` (`department_id`),
  KEY `FKk1fp8f1k90hq87pf0slvyguv3` (`position_id`),
  CONSTRAINT `FKexswov2efs6ea88m4khpbn0lx` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`),
  CONSTRAINT `FKk1fp8f1k90hq87pf0slvyguv3` FOREIGN KEY (`position_id`) REFERENCES `position` (`position_id`),
  CONSTRAINT `system_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `society_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `system_user_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `system_user_ibfk_3` FOREIGN KEY (`position_id`) REFERENCES `position` (`position_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO `system_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '0', '0', '9');
INSERT INTO `system_user` VALUES ('6', 'jiyun', 'f9e11959013a3fa5692c2549156ee2ea', '1', '0', '0', '9');

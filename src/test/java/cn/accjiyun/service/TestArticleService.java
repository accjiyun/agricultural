package cn.accjiyun.service;

import cn.accjiyun.common.entity.PageModel;
import cn.accjiyun.common.entity.QueryEntity;
import cn.accjiyun.entity.article.Article;
import cn.accjiyun.entity.article.ArticleContent;
import cn.accjiyun.service.article.ArticleService;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/1/28.=
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestArticleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestArticleService.class);

    @Autowired
    private ArticleService articleService;

    @Test
    public void queryById() {
        Article article = articleService.queryArticleById(2);
        LOGGER.info(JSON.toJSONString(article.getTitle()
                + article.getArticleContentByArticleId().getContent()));
    }

    @Test
    public void queryArticlePage(){
        QueryEntity queryArticle = new QueryEntity();

        Map eqParams = new HashMap();
        eqParams.put("articleTypeByArticleTypeId.articleTypeId", (byte)1);
        eqParams.put("status", (byte)1);
        queryArticle.setEqParams(eqParams);

        Map likeStrings = new HashMap();
        likeStrings.put("keyWord", "两会");
        likeStrings.put("title", "记于");
        queryArticle.setLikeStrings(likeStrings);

        Map<String, String> orderBy = new HashMap<>();
        orderBy.put("publicTime", "desc");
        queryArticle.setOrderBy(orderBy);

        PageModel<Article> model = new PageModel<>();
        model.setPageSize(10);
        List<Article> articles = articleService.queryArticlePage(queryArticle, model);
        for (Article article : articles) {
            LOGGER.info(JSON.toJSONString(article.getTitle()
                    + article.getArticleContentByArticleId().getContent()));
        }
    }

    @Test
    public void queryArticleType() {
        String typeName = articleService.queryArticleTypeById((byte)1).getTypeName();
        int articleTypeId = articleService.queryArticleTypeIdByName("新闻").getArticleTypeId();
        LOGGER.info(JSON.toJSONString(articleTypeId + " : " + typeName));
    }

    @Test
    public void saveArticle() {
        Article article = new Article();
        ArticleContent articleContent = new ArticleContent();
        article.setArticleContentByArticleId(articleContent);
        articleContent.setArticleByArticleId(article);
        article.setTitle("Title");
        article.setCreateTime(new Timestamp(new Date().getTime()));
        articleService.createArticle(article);
    }

}

## 响应式农合社信息管理系统

### 技术框架

核心框架：Spring Framework

视图框架：Spring MVC 4.2

持久层框架：Hibernate 5

前端框架：LayUI

### 开发环境

IDE: IntelliJ IDEA

DB :  MySQL Server 5.7

JDK : JAVA 8

Web 服务器 : Tomcat 9

### 数据库

sql文件 : /src/main/resources/agricultural_db.sql

数据库ER图：

![center](demo/3.png) 

页面预览 ：

![login](demo/1.gif)

![center](demo/2.png)

